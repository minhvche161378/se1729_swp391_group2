/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.ad;

import dao.CategotyDao;
import dao.CustomerDAO;
import dao.DateDAO;
import dao.FeedbackDao;
import dao.OrderDao;
import dao.ProductDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Category;
import model.Date;

/**
 *
 * @author hihih
 */
@WebServlet(name="AdminDashboardController1", urlPatterns={"/adminDashboard"})
public class AdminDashboardController1 extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
          response.setContentType("text/html;charset=UTF-8");
        ProductDao pd = new ProductDao();
        CustomerDAO cd = new CustomerDAO();
        FeedbackDao fd = new FeedbackDao();
        CategotyDao ctd = new CategotyDao();
        OrderDao od = new OrderDao();
        DateDAO dd = new DateDAO();

        Date date = dd.get7day();
        String salerId = "!= -1";
        String start = date.getStart().toString();
        String end = date.getEnd().toString();
        String start_raw = request.getParameter("start");
        String end_raw = request.getParameter("end");
        if (start_raw != null) {
            start = start_raw;
            end = end_raw;
        }
        

        int day = dd.CountDayByStartEnd(start, end);

        int totalProduct = pd.getTotalProduct();
        int totalProduct1 = pd.getListCategoryByPid("1").size();
        int totalProduct2 = pd.getListCategoryByPid("2").size();
        int totalProduct3 = pd.getListCategoryByPid("3").size();
        int totalProduct4 = pd.getListCategoryByPid("4").size();


        List<Category> listCategoryProduct = ctd.getListCategory();
        
        // set chart revenue
//        List<Chart> listChartRevenueArea = od.getChartRevenueArea(salerId, start, day);
//        int maxListChartRevenueArea = -1;
//        for (Chart o : listChartRevenueArea) {
//            if (o.getValue() > maxListChartRevenueArea) {
//                maxListChartRevenueArea = o.getValue();
//            }
//        }
//        maxListChartRevenueArea = (maxListChartRevenueArea / 1000000 + 1) * 1000000;
//        
//        // set chart customer
//        List<Chart> listChartCustomer = cd.getChartCustomerArea(start, day);
//        int maxListChartCustomerArea = -1;
//        for (Chart o : listChartCustomer) {
//            if (o.getValue() > maxListChartCustomerArea) {
//                maxListChartCustomerArea = o.getValue();
//            }
//        }
//        maxListChartCustomerArea = (maxListChartCustomerArea / 10 + 1) * 10;
//        
//        // set chart avg rated
//        List<ChartStar> listChartAvgStar = fd.getChartAvgStar(start, day);

        request.setAttribute("totalProduct", totalProduct);
        request.setAttribute("totalProduct1", totalProduct1);
        request.setAttribute("totalProduct2", totalProduct2);
        request.setAttribute("totalProduct3", totalProduct3);
        request.setAttribute("totalProduct4", totalProduct4);
         List<Integer> a=  pd.getProductCountByCaid(); 
         request.setAttribute("totalProductAll", a);
        
        
        request.setAttribute("listCategoryProduct", listCategoryProduct);
        
//        request.setAttribute("listChartRevenueArea", listChartRevenueArea);
//        request.setAttribute("maxListChartRevenueArea", maxListChartRevenueArea);
//        
//        request.setAttribute("listChartCustomer", listChartCustomer);
//        request.setAttribute("maxListChartCustomerArea", maxListChartCustomerArea);
//        
//        request.setAttribute("listChartAvgStar", listChartAvgStar);
        request.setAttribute("start", start);
        request.setAttribute("end", end);
        request.getRequestDispatcher("newjsp.jsp").forward(request, response);
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
