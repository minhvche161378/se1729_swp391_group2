/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.common;

import dao.AccountDao;
import dao.NewsDao;
import dao.OrderDao;
import dao.ProductDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.LinkedHashMap;
import java.util.List;
import model.Account;
import model.CategoriesNew;
import model.Category;
import model.Order;
import model.OrderDetails;
import model.ShipmentDetails;

/**
 *
 * @author HP
 */
@WebServlet(name="OrderHistoryController", urlPatterns={"/orderHistory"})
public class OrderHistoryController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OrderListController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OrderListController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String status = request.getParameter("status");
        HttpSession session = request.getSession();
        AccountDao ad = new AccountDao();
        ProductDao pd = new ProductDao();
        NewsDao nd = new NewsDao();
        OrderDao od = new OrderDao();
        Account ac = (Account) session.getAttribute("account");

        //Thông tin tài khoản
        session.setAttribute("account", ac);
     
        
         //List category header
        List<Category> caList = pd.getListCate();
        List<CategoriesNew> canewList = nd.getListCategoriesNew();
        request.setAttribute("caList", caList);
        request.setAttribute("canewList", canewList);
        
        //Order list
        List<Order> orderList = od.getListorder(ac.getAcid(),status);
        
        //Số lượng tình trạng các order
        int all = od.getListorder(ac.getAcid(),null).size();
        int wait = od.getListorder(ac.getAcid(),"0").size();
         int shipping = od.getListorder(ac.getAcid(),"1").size();
          int receive = od.getListorder(ac.getAcid(),"2").size();
          int cancel = od.getListorder(ac.getAcid(),"3").size();
           request.setAttribute("all",all);
          request.setAttribute("wait",wait);
          request.setAttribute("shipping",shipping);
          request.setAttribute("receive",receive);
          request.setAttribute("cancel",cancel);
         
        
        
        request.setAttribute("orderList", orderList);
        request.setAttribute("status",status);
        request.setAttribute("od",od);
        request.getRequestDispatcher("OrderHistory.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
