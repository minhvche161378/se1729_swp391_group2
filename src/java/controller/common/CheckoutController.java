/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.common;

import Validation.Cart;
import Validation.ValidationDate;
import Validation.ValidationString;
import dao.AccountDao;
import dao.CodeSaleDao;
import dao.NewsDao;
import dao.OrderDao;
import dao.ProductDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import model.Account;
import model.CategoriesNew;
import model.Category;
import model.CodeSale;
import model.Order;
import model.OrderDetails;
import model.Product;
import model.ShipmentDetails;

/**
 *
 * @author HP
 */
@WebServlet(name = "CheckoutController", urlPatterns = {"/checkout"})
public class CheckoutController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CheckoutController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CheckoutController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Cart t = new Cart();
        ProductDao pd = new ProductDao();
        NewsDao nd = new NewsDao();
        OrderDao od = new OrderDao();
        CodeSaleDao cd = new CodeSaleDao();
        HttpSession session = request.getSession();
        AccountDao ad = new AccountDao();
        String oid = request.getParameter("oid");
        String[] selectedItems = request.getParameterValues("selectedItems");
        String pid = request.getParameter("pid");
        
       

        
      
       
        Account ac = (Account) session.getAttribute("account");

        //Thông tin tài khoản
        session.setAttribute("account", ac);
        List<ShipmentDetails> listsh = ad.getListShipmentOfAcc(ac.getEmail());

        //List category header
        List<Category> caList = pd.getListCate();
        List<CategoriesNew> canewList = nd.getListCategoriesNew();
        request.setAttribute("caList", caList);
        request.setAttribute("canewList", canewList);

        //Lay sản phẩm sẽ mua  
        String txt = "";
        LinkedHashMap<Product, Integer> pList = new LinkedHashMap<>();
        if (pid == null) {

            Cookie[] arr = request.getCookies();
            // Lấy các sản phẩm trong cart
            if (arr != null) {
                for (Cookie o : arr) {
                    if (o.getName().equals("cart")) {
                        txt += o.getValue();

                    }
                    // Xóa cookie check out
                    if (o.getName().equals("checkout")) {
                        o.setMaxAge(0);
                        response.addCookie(o);
                    }
                }
            }
            // Trường hợp chọn các ô checkbox
            if (selectedItems != null && selectedItems.length > 0) {
                txt = t.getQuantityBypid(txt, selectedItems);
            }

            pList = t.getCart(txt);

            //San phẩm mua ngay
        }
        if (pid != null) {
            Product p = pd.getProductbyID(pid);
            pList.put(p, 1);
            txt = p.getPid() + ":1";

        }
        if (oid != null) {
            // mua lại hàng trong order

            List<OrderDetails> ord = od.getOrderDetails(oid);
            for (OrderDetails orderD : ord) {
                Product p = od.getProductByIdOrderDetails(orderD.getOdid());
                pList.put(p, orderD.getQuantity());
                txt = p.getPid() + ":" + orderD.getQuantity();
            }

        }
        //Tạo cookie checkout
        Cookie c = new Cookie("checkout", txt);
        c.setMaxAge(2 * 24 * 60 * 60);

        response.addCookie(c);

        request.setAttribute("pList", pList);
        request.setAttribute("oid", oid);
        request.setAttribute("listsh", listsh);
        request.getRequestDispatcher("Checkout.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

      
        String receiver = request.getParameter("receiver");
        String address = request.getParameter("address");
        String phonenumber = request.getParameter("phonenumber");
        String totalPrice_raw = request.getParameter("totalPrice");
        String codeSale_raw = request.getParameter("codeSale");
        
        String note = request.getParameter("note");
        String city = request.getParameter("city");
        String district = request.getParameter("district");
        String ward = request.getParameter("ward");

        ValidationString valtS = new ValidationString();
        address = valtS.trimAndCheckBlank(address);
        phonenumber = valtS.trimAndCheckBlank(phonenumber);
      
        HttpSession session = request.getSession();
        AccountDao ad = new AccountDao();
        Account ac = (Account) session.getAttribute("account");
        ShipmentDetails sh = ad.getMarkSipment(ac.getEmail());
        String alertMessage = "Mua hàng thành công. Vui lòng chờ xác nhận đơn hàng";
        String  redirectUrl = "useCodeSale?codeSale=" + codeSale_raw + 
                "&address=" + address + "&phonenumber=" + phonenumber + "&receiver=" + receiver+
         "&city=" + city + "&district=" + district + "&ward=" + ward;
        
        if (address == null || address.length() < 4) {
            alertMessage = "Địa chỉ không hợp lệ. Vui lòng nhập lại thông tin địa chỉ !";
        
        } else if (!valtS.validatePhoneNumber(phonenumber)) {
            alertMessage = "Số điện thoại phải có 10 số! Vui lòng nhập lại.";

        
        } else if (!valtS.isValidName(receiver)) {
            alertMessage = "Tên người nhận không hợp lệ! Vui lòng nhập lại.";

       
        } else if (sh == null && (city.isEmpty() || district.isEmpty() || ward.isEmpty())) {

            alertMessage = "Vui lòng nhập thông tin địa chỉ tỉnh,thành phố,xã phường !";
          

        } else {
               redirectUrl = "orderHistory";
            if (sh == null) {
                address = city + ", " + district + ", " + ward + ", " + address;
                ad.createShipment(address, phonenumber, ac.getAcid(), receiver);
            }

           try {
            Cart t = new Cart();
            OrderDao od = new OrderDao();
            ProductDao pd = new ProductDao();
            CodeSaleDao cd = new CodeSaleDao();
            ValidationDate valdate = new ValidationDate();
            Cookie[] arr = request.getCookies();

            LinkedHashMap<Product, Integer> pList = new LinkedHashMap<>();

            // Xóa các product đã mua trong cart
            if (arr != null) {
                String cart = "";
                String checkout = "";
                for (Cookie o : arr) {
                    if (o.getName().equals("cart")) {
                        cart += o.getValue();

                        o.setMaxAge(0);
                        response.addCookie(o);

                    }
                    if (o.getName().equals("checkout")) {
                        checkout += o.getValue();
                        o.setMaxAge(0);
                        response.addCookie(o);
                    }
                    pList = t.getCart(checkout);
                }

                cart = t.removeDuplicateElements(cart, checkout);

                if (!cart.isEmpty()) {
                    Cookie c = new Cookie("cart", cart);
                    c.setMaxAge(2 * 24 * 60 * 60);

                    response.addCookie(c);
                }

            }

            String currentdate = valdate.getCurrentTime();
            // tao order
          

                int totalPrice = Integer.parseInt(totalPrice_raw);
                int discount =0;
                //Lấy mã giảm giá
                if(codeSale_raw!=null){
                CodeSale code = cd.getCodeSalebyCode(currentdate, codeSale_raw);
                
                cd.updateQuantityCodeSale(codeSale_raw);
                discount = code.getDiscount();
                }
               
                od.createOrder(ac.getAcid(), currentdate, totalPrice, address, phonenumber, note, receiver,discount);
                
                Order o = od.getOrderByAcid(ac.getAcid(), totalPrice, address);

                for (Map.Entry<Product, Integer> entry : pList.entrySet()) {
                    Product p = entry.getKey();
                    int quantity = entry.getValue();
                    int price = 0;
                    if (p.isIsDiscount()) {
                        price = p.getPriceSale();
                    } else {
                        price = p.getPrice();
                    }
                    if (p != null) {
                    
                        od.createOrderDetails(p.getPid(), o.getOid(), price, quantity);
                    }

                }

            } catch (Exception e) {

            }
        }

        String script1 = "<script type='text/javascript'>alert('" + alertMessage + "');window.location.href='" + redirectUrl + "';</script>";
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        response.getWriter().println(script1);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
