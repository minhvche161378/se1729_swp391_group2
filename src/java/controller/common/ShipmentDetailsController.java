/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.common;

import Validation.ValidationString;
import dao.AccountDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Account;
import model.ShipmentDetails;

/**
 *
 * @author HP
 */
@WebServlet(name = "ShipmentDetailsController", urlPatterns = {"/shipmentDetails"})
public class ShipmentDetailsController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ShipmentDetailsController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ShipmentDetailsController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String receiver = request.getParameter("receiver");
        String address = request.getParameter("address");
        String phonenumber = request.getParameter("phonenumber");

        String city = request.getParameter("city");
        String district = request.getParameter("district");
        String ward = request.getParameter("ward");
        
        String displayForm = request.getParameter("displayForm");
        
        
        
        //Khai bao classes
        HttpSession session = request.getSession();
        AccountDao ad = new AccountDao();
        ValidationString vas = new ValidationString();
        Account ac = (Account) session.getAttribute("account");

        ShipmentDetails sh = ad.getMarkSipment(ac.getEmail());
        List<ShipmentDetails> listsh = ad.getListShipmentOfAcc(ac.getEmail());

        request.setAttribute("sh", sh);
        request.setAttribute("listsh", listsh);
        
         request.setAttribute("receiver", receiver);
        request.setAttribute("address", address);
        request.setAttribute("phonenumber", phonenumber);

        request.setAttribute("city", city);
        request.setAttribute("district", district);
        request.setAttribute("ward", ward);
        request.setAttribute("displayForm", displayForm);
        
        request.getRequestDispatcher("ShipmentDetails.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String changeMarkShip_raw = request.getParameter("changeMarkShip");
        String removeInfo_raw = request.getParameter("removeInfo");

        String newPhonenumber = request.getParameter("newPhonenumber");
        String newAddress = request.getParameter("newAddress");
        String newReceiver = request.getParameter("newReceiver");
        String city = request.getParameter("city");
        String district = request.getParameter("district");
        String ward = request.getParameter("ward");
        String type = request.getParameter("type");

        HttpSession session = request.getSession();
        AccountDao ad = new AccountDao();
        ValidationString vas = new ValidationString();
        String err = "";
        Account ac = (Account) session.getAttribute("account");
        ShipmentDetails sh = ad.getMarkSipment(ac.getEmail());

        newPhonenumber = vas.trimAndCheckBlank(newPhonenumber);
        newAddress = vas.trimAndCheckBlank(newAddress);
        newReceiver = vas.trimAndCheckBlank(newReceiver);

        String resend = "shipmentDetails?address=" + newAddress + "&phonenumber=" + newPhonenumber + "&receiver=" + newReceiver
                    + "&city=" + city + "&district=" + district + "&ward=" + ward + "&displayForm=block";

        // Change Mark shipmentDetails
        if (type.equals("changeInfo")) {
            if (changeMarkShip_raw != null) {

                int changeShipid = Integer.parseInt(changeMarkShip_raw);
                ad.changeInforShip(changeShipid, ac.getAcid());

                resend = "shipmentDetails";
            }
        }
        if (type.equals("removeInfo")) {

            ad.RemoveInfoShip(removeInfo_raw);
           resend = "shipmentDetails";

        }

        //Add new shipmentDetails
        if (type.equals("addNew")) {
            String displayForm = "";

            if (!city.isEmpty() && !district.isEmpty() && !ward.isEmpty()) {
                String address = city + ", " + district + ", " + ward + ", " + newAddress;

                if (!vas.isValidName(newReceiver)) {
                    err = "Tên người nhận không hợp lệ ! Vui lòng nhập lại.";

                } else if (ad.isAcchaveShipment(ac.getAcid(), address) != null) {
                    err = "Địa chỉ đã tồn tại ! Vui lòng nhập lại.";
                } else if (!vas.validatePhoneNumber(newPhonenumber)) {
                    err = "Số điện thoại phải có 10 số! Vui lòng nhập lại.";

                } else {
                    ad.createShipment(address, newPhonenumber, ac.getAcid(), newReceiver);
                    err = "Thêm thành công";
                    resend = "shipmentDetails";
                }

            } else {

                if (city.isEmpty()) {
                    err = " Vui lòng nhập đầy đủ thông tin tỉnh thành!";
                } else if (district.isEmpty()) {
                    err = " Vui lòng nhập đầy đủ thông tin thành phố!";
                } else if (ward.isEmpty()) {
                    err = " Vui lòng nhập đầy đủ thông tin xã phường!";
                }
            }

        }

        response.sendRedirect(resend);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
