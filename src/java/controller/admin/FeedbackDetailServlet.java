/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.admin;

import dao.CommentDao;
import dao.FeedbackDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Comment;
import model.Feedback;

/**
 *
 * @author hihih
 */
@WebServlet(name="FeedbackDetailServlet", urlPatterns={"/FeedbackDetailServlet"})
public class FeedbackDetailServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FeedbackDetailServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FeedbackDetailServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
         int commentId =Integer.parseInt(request.getParameter("commentId")); 
         request.setAttribute("commentId", commentId);
         CommentDao c= new CommentDao();
        FeedbackDao d= new FeedbackDao();
        // Lấy thông tin chi tiết của comment
        Comment comment = c.getCommentById(commentId);
        
        // Lấy danh sách các feedback của comment
        List<Feedback> feedbackList = d.getFeedbackByCommentId(commentId);
        
        request.setAttribute("comment", comment);
        request.setAttribute("feedbackList", feedbackList);
        request.getRequestDispatcher("FeedbackDetail1.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        CommentDao commentDao = new CommentDao();
        FeedbackDao feedbackDao  = new FeedbackDao();
        String action = request.getParameter("action");

        if ("search".equals(action)) {
            String searchKeyword = request.getParameter("searchKeyword");

            List<Feedback> searchResults = feedbackDao.search(searchKeyword);

            request.setAttribute("feedbackList", searchResults);
            // Điều hướng đến trang kết quả tìm kiếm FeedbackListController

            request.getRequestDispatcher("FeedbackDetail1.jsp").forward(request, response);
        } else if ("edit".equals(action)) {
//            // Xử lý chỉnh sửa phản hồi
            int commentId = Integer.parseInt(request.getParameter("commentId"));
            int feedbackId = Integer.parseInt(request.getParameter("feedbackId"));
            String newContent = request.getParameter("newContent");
            int newStatus = Integer.parseInt(request.getParameter("newStatus"));
//            // Thực hiện cập nhật phản hồi trong cơ sở dữ liệu

            // Gọi phương thức để chỉnh sửa Comment trong lớp CommentDAO
//           feedbackDao.editComment(feedbackId, newContent, newStatus);

            // Nếu chỉnh sửa thành công, bạn có thể điều hướng hoặc hiển thị thông báo
            response.sendRedirect("FeedbackDetailServlet?commentId="+commentId);

        } else if ("reply".equals(action)) {
//            // Xử lý phản hồi
            int commentId = Integer.parseInt(request.getParameter("commentId"));
            String feedbackBySellerName = request.getParameter("feedbackBySellerName");
            String feedbackContent = request.getParameter("feedbackContent");

            // Gọi phương thức để thêm phản hồi (reply) trong lớp FeedbackDAO
            FeedbackDao feedbackDAO = new FeedbackDao();
            Date currentDate = new Date();
            // Định dạng ngày/tháng/năm (dd/MM/yyyy)
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
            // Chuyển đổi thời gian hiện tại sang chuỗi theo định dạng
            String formattedDate = dateFormat.format(currentDate);
            Feedback feedback = Feedback.builder()
                    .CommentId(commentId)
                    .Create_atFeedback(formattedDate)
                    .FeedbackBySellerName(feedbackBySellerName)
                    .FeedbackContent(feedbackContent)
                    .build();
            feedbackDAO.addFeedback(feedback);
            response.sendRedirect("FeedbackDetailServlet?commentId="+commentId);
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
