/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import dao.CommentDao;
import dao.FeedbackDao;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Comment;
import model.Feedback;

/**
 *
 * @author hihih
 */
@WebServlet(name = "FeedbackListController", urlPatterns = {"/FeedbackListController"})
public class FeedbackListController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FeedbackListController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FeedbackListController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CommentDao commentDao = new CommentDao();
        FeedbackDao feedbackDao = new FeedbackDao();
        List<Comment> comments = commentDao.getAllCommentsSortedByFormattedCreateAt();
        List<Feedback> feedbacks = feedbackDao.getAllFeedbacks();
        Map<Integer, List<Feedback>> feedbacksByComment = new HashMap<>();
        for (Feedback feedback : feedbacks) {
            int commentId = feedback.getCommentId();
            feedbacksByComment.computeIfAbsent(commentId, k -> new ArrayList<>()).add(feedback);
        }

        // Lưu thông tin vào request để hiển thị trên trang JSP
        request.setAttribute("feedbacksByComment", feedbacksByComment);

        double averageRate = commentDao.calculateAverageRate();

        // Đưa số rate trung bình vào request attribute
        request.setAttribute("averageRate", averageRate);

        request.setAttribute("feedbacks", feedbacks);
        // Lấy thông tin rate và phần trăm
        Map<Integer, Map<String, Double>> rateStatistics = commentDao.getRateStatistics();

// Đưa thông tin vào request attribute
        request.setAttribute("rateStatistics", rateStatistics);

        // Phân trang:
// Số bình luận trên mỗi trang
        int commentsPerPage = 5;

// Tính tổng số trang
        int totalComments = comments.size();
        int totalPages = (int) Math.ceil((double) totalComments / commentsPerPage);

// Lưu danh sách bình luận và số trang vào request
        request.setAttribute("totalPages", totalPages);
        int currentPage = 1; // Trang mặc định là trang 1
        String pageParam = request.getParameter("page");
        if (pageParam != null && !pageParam.isEmpty()) {
            currentPage = Integer.parseInt(pageParam);
        }

        int startIndex = (currentPage - 1) * commentsPerPage;
        int endIndex = Math.min(startIndex + commentsPerPage, totalComments);
// xử lý next trang
        int previousPage = (currentPage > 1) ? (currentPage - 1) : 1;
        int nextPage = (currentPage < totalPages) ? currentPage + 1 : totalPages;

        request.setAttribute("previousPage", previousPage);
        request.setAttribute("nextPage", nextPage);

        List<Comment> commentsForCurrentPage = comments.subList(startIndex, endIndex);
        request.setAttribute("comments", commentsForCurrentPage);
        request.setAttribute("commentsA", comments);

        String sortColumn = request.getParameter("sort");
        String reverseColumn = request.getParameter("reverse");
        if (sortColumn != null) {
            // Sort based on the selected column
            Collections.sort(commentsForCurrentPage, (c1, c2) -> {
                switch (sortColumn) {
                    case "CommentId":
                        return Integer.compare(c1.getCommentId(), c2.getCommentId());
                    case "CommentContent":
                        return c1.getCommentContent().compareTo(c2.getCommentContent());
                    case "Create_at":
                        return c1.getCreate_at().compareTo(c2.getCreate_at());
                    case "Status":
                        return Integer.compare(c1.getStatus(), c2.getStatus());
                    case "ProductId":
                        return Integer.compare(c1.getProductId(), c2.getProductId());
                    case "AccountId":
                        return Integer.compare(c1.getAccountId(), c2.getAccountId());
                    case "Rate":
                        return Integer.compare(c1.getRate(), c2.getRate());
                    case "CustomerName":
                        return c1.getCustomerName().compareTo(c2.getCustomerName());
                    case "CustomerEmail":
                        return c1.getCustomerEmail().compareTo(c2.getCustomerEmail());
                    default:
                        return 0;
                }
            });
        } else if (reverseColumn != null) {
            // Reverse the order based on the selected column
            Collections.sort(commentsForCurrentPage, (c1, c2) -> {
                switch (reverseColumn) {
                    case "CommentId":
                        return Integer.compare(c2.getCommentId(), c1.getCommentId());
                    case "CommentContent":
                        return c2.getCommentContent().compareTo(c1.getCommentContent());
                    case "Create_at":
                        return c2.getCreate_at().compareTo(c1.getCreate_at());
                    case "Status":
                        return Integer.compare(c2.getStatus(), c1.getStatus());
                    case "ProductId":
                        return Integer.compare(c2.getProductId(), c1.getProductId());
                    case "AccountId":
                        return Integer.compare(c2.getAccountId(), c1.getAccountId());
                    case "Rate":
                        return Integer.compare(c2.getRate(), c1.getRate());
                    case "CustomerName":
                        return c2.getCustomerName().compareTo(c1.getCustomerName());
                    case "CustomerEmail":
                        return c2.getCustomerEmail().compareTo(c1.getCustomerEmail());
                    default:
                        return 0;
                }
            });
        }

        // Forward request đến trang JSP để hiển thị
        RequestDispatcher dispatcher = request.getRequestDispatcher("FeedbackList.jsp");
        dispatcher.forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CommentDao commentDao = new CommentDao();
        String action = request.getParameter("action");

        if ("search".equals(action)) {
            String searchKeyword = request.getParameter("searchKeyword");

            List<Comment> searchResults = new ArrayList<>();
            searchResults = commentDao.search1(searchKeyword);

            request.setAttribute("comments", searchResults);
            // Điều hướng đến trang kết quả tìm kiếm FeedbackListController

            request.getRequestDispatcher("FeedbackList.jsp").forward(request, response);
        } else if ("edit".equals(action)) {
//            // Xử lý chỉnh sửa phản hồi
            int commentId = Integer.parseInt(request.getParameter("commentId"));
            String newContent = request.getParameter("newContent");
            int newStatus = Integer.parseInt(request.getParameter("newStatus"));
//            // Thực hiện cập nhật phản hồi trong cơ sở dữ liệu

            // Gọi phương thức để chỉnh sửa Comment trong lớp CommentDAO
            boolean editSuccess = commentDao.editComment(commentId, newContent, newStatus);

            // Nếu chỉnh sửa thành công, bạn có thể điều hướng hoặc hiển thị thông báo
            response.sendRedirect("FeedbackListController");

        } else if ("reply".equals(action)) {
//            // Xử lý phản hồi
            int commentId = Integer.parseInt(request.getParameter("commentId"));
            String feedbackBySellerName = request.getParameter("feedbackBySellerName");
            String feedbackContent = request.getParameter("feedbackContent");

            // Gọi phương thức để thêm phản hồi (reply) trong lớp FeedbackDAO
            FeedbackDao feedbackDAO = new FeedbackDao();
            Date currentDate = new Date();
            // Định dạng ngày/tháng/năm (dd/MM/yyyy)
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
            // Chuyển đổi thời gian hiện tại sang chuỗi theo định dạng
            String formattedDate = dateFormat.format(currentDate);
            Feedback feedback = Feedback.builder()
                    .CommentId(commentId)
                    .Create_atFeedback(formattedDate)
                    .FeedbackBySellerName(feedbackBySellerName)
                    .FeedbackContent(feedbackContent)
                    .build();
            feedbackDAO.addFeedback(feedback);
            response.sendRedirect("FeedbackListController");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
