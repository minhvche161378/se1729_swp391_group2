/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.maketing;

import dao.AccountDao;
import dao.CodeSaleDao;
import dao.CommentDao;
import dao.NewsDao;
import dao.OrderDao;
import dao.ProductDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import model.Account;
import model.CategoriesNew;
import model.CodeSale;
import model.Comment;
import model.News;
import model.NewsDetails;
import model.Order;
import model.Product;

/**
 *
 * @author Thanh
 */
@WebServlet(name="MKTDashBoardController", urlPatterns={"/mktdashboard"})
public class MKTDashBoardController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MKTDashBoardController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MKTDashBoardController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        NewsDao ndao = new NewsDao();
        AccountDao acdao = new AccountDao();
        CommentDao comdao = new CommentDao();
        CodeSaleDao csdao = new CodeSaleDao();
        int totalNews = ndao.getTotalNewsCount();
        int totalView = ndao.getTotalView();
        int totalcode = csdao.getTotalCodeSaleCount();
        int totalComment = comdao.getTotalAllComment();
        int totalCustomer = acdao.getTotalCustomerCount();
        List<CodeSale> codList = csdao.getAllCodeSale();
        List<News> newsList = ndao.getTop5MostView();
        List<News> newsleastList = ndao.getTop5LeastView();
        
        //get Canew
        LinkedHashMap<News, String> list = new LinkedHashMap<>();
        for (News news1 : newsList) {
            CategoriesNew canew = ndao.getCateNewbyId(news1.getCanewId() + "");
            list.put(news1, canew.getName());
        }

        //get user name
        LinkedHashMap<News, String> list2 = new LinkedHashMap<>();
        for (News news2 : newsList) {
            Account username = ndao.getUserNameById(news2.getAcid() + "");
            list2.put(news2, username.getUsername());
        }
        
        //get View 
        LinkedHashMap<News, Integer> list3 = new LinkedHashMap<>();
        for (News news3 : newsList) {
            NewsDetails views = ndao.getViewbyId(news3.getNid()+ "");
            list3.put(news3, views.getView());
        }
        
        //get Canew
        LinkedHashMap<News, String> list4 = new LinkedHashMap<>();
        for (News news4 : newsleastList) {
            CategoriesNew canew = ndao.getCateNewbyId(news4.getCanewId() + "");
            list4.put(news4, canew.getName());
        }

        //get user name
        LinkedHashMap<News, String> list5 = new LinkedHashMap<>();
        for (News news5 : newsleastList) {
            Account username = ndao.getUserNameById(news5.getAcid() + "");
            list5.put(news5, username.getUsername());
        }
        
        //get View 
        LinkedHashMap<News, Integer> list6 = new LinkedHashMap<>();
        for (News news6 : newsleastList) {
            NewsDetails views = ndao.getViewbyId(news6.getNid()+ "");
            list6.put(news6, views.getView());
        }
        
        Map<Integer, Integer> ratePercentage = comdao.getRatePercentage();
        Map<Integer, Integer> rateCounts = comdao.getRatebyRateType();
        Map<Integer, Integer> CustomerCount = acdao.getListNewCustomerPerWeek();
        
        
        
        request.setAttribute("ratelist", rateCounts);
        request.setAttribute("totalCus", totalCustomer);
        request.setAttribute("CustomerCount", CustomerCount);
        request.setAttribute("ratepercent", ratePercentage);
        request.setAttribute("canew", list);
        request.setAttribute("username", list2);
        request.setAttribute("views", list3);
        request.setAttribute("canewleast", list4);
        request.setAttribute("usernameleast", list5);
        request.setAttribute("viewsleast", list6);
        request.setAttribute("totalcom", totalComment);
        request.setAttribute("codlist", codList);
        request.setAttribute("leastView", newsleastList);
        request.setAttribute("mostView", newsList);
        request.setAttribute("totalCode", totalcode);
        request.setAttribute("totalNews", totalNews);
        request.setAttribute("totalView", totalView);
        
        request.getRequestDispatcher("MKTDashBoard.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
