/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.maketing;

import dao.AccountDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import model.Account;

/**
 *
 * @author HP
 */
@WebServlet(name="CustomerListController", urlPatterns={"/customerList"})
public class CustomerListController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CustomerListController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CustomerListController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        AccountDao d = new AccountDao();
        List<Account> acc = d.getAllUsers("4");

        PrintWriter out = response.getWriter();
        String search = request.getParameter("search");
        if (search != null) {
            acc = d.getAllAccountbyName(search);
        } else if (request.getParameter("acid") != null) {
            int acid = 0;
            try {
                acid = Integer.parseInt(request.getParameter("acid"));
            } catch (Exception e) {
            }
            Account a = d.getAccountFromAid(acid);
            acc = d.getAllAccountbyName(a.getUsername());
        }
        // Sort theo create_at
        String sort = request.getParameter("sort");
        if ("newest".equals(sort)) {
            Collections.sort(acc, Comparator.comparing(Account::getCreated_at).reversed());
        } else if ("oldest".equals(sort)) {
            Collections.sort(acc, Comparator.comparing(Account::getCreated_at));
        }
        if ("role".equals(sort)) {
            // Use a custom comparator to sort by role
            Collections.sort(acc, Comparator.comparing(Account::getRole));
        }

        request.setAttribute("acc", acc);
        out.println(acc.get(0).getUsername());
        if (request.getParameter("page") == null) {
            request.getRequestDispatcher("CustomerList.jsp?page=1").forward(request, response);
        } else {
            request.getRequestDispatcher("CustomerList.jsp?page=" + request.getParameter("page")).forward(request, response);
        }

//        request.getRequestDispatcher("UserList.jsp").forward(request, response);
    }
    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
