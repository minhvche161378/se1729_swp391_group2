/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.maketing;

import Validation.ValidationDate;
import dao.NewsDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import model.Account;
import model.CategoriesNew;
import model.News;
import model.NewsDetails;

/**
 *
 * @author Thanh
 */

@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, //50MB
        maxFileSize = 1024 * 1024 * 50, // 50MB
        maxRequestSize = 1024 * 1024 * 50) // 50MB

@WebServlet(name="UpdatePostController", urlPatterns={"/updatepost"})
public class UpdatePostController extends HttpServlet {
   
     private static final long serialVersionUID = 1L;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdatePostController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdatePostController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        NewsDao ndao = new NewsDao();
        
        String nid = request.getParameter("nid");

        News n = ndao.getNewsbyID(nid);
        NewsDetails nd = ndao.getNewsDetailsById(nid);


        request.setAttribute("news", n);
        request.setAttribute("newdetail", nd);
        
        List<CategoriesNew> cn = ndao.getListCategoriesNew();
        request.setAttribute("catnews", cn);
        request.setAttribute("thisc", ndao.getCategoryNewbyId(cn, n.getCanewId()).getCanewId());
        request.getRequestDispatcher("PostUpdate.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }

    private File getFolderUpload() {        
        String basePath = getServletContext().getRealPath(File.separator) + "\\..\\..\\web\\images\\UploadImgs\\";
//        System.out.println(basePath);
        File folderUpload = new File(basePath);
        if (!folderUpload.exists()) {
            folderUpload.mkdirs();
        }
        return folderUpload;
    }
    
    private File getFolderUpload2() {        
        String basePath = getServletContext().getRealPath(File.separator) + "\\images\\UploadImgs\\";
//        System.out.println(basePath);
        File folderUpload = new File(basePath);
        if (!folderUpload.exists()) {
            folderUpload.mkdirs();
        }
        return folderUpload;
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account acc = (Account)session.getAttribute("account");        
//        if (acc == null) {
//            req.setAttribute("mess", "Please <a href='login.jsp'>login</a> to access!!");
//            req.getRequestDispatcher("showMessage.jsp").forward(req, resp);
//        }
        // upload file to server
        String files = "";
        for (Part part : request.getParts()) {
            if (part.getName().equals("file")) {
                String fileName = extractFileName(part);
                fileName = new File(fileName).getName();                                            
                files += (files.length()>0?";":"") + fileName;
                System.out.println(this.getFolderUpload().getAbsolutePath() + File.separator + fileName);
                System.out.println(this.getFolderUpload2().getAbsolutePath() + File.separator + fileName);
                part.write(this.getFolderUpload().getAbsolutePath() + File.separator + fileName);                
                part.write(this.getFolderUpload2().getAbsolutePath() + File.separator + fileName);                
            }
        }
        
        String files2 = "";
        for (Part part2 : request.getParts()) {
            if (part2.getName().equals("file2")) {
                String fileName = extractFileName(part2);
                fileName = new File(fileName).getName();                                            
                files2 += (files2.length()>0?";":"") + fileName;
                System.out.println(this.getFolderUpload().getAbsolutePath() + File.separator + fileName);
                System.out.println(this.getFolderUpload2().getAbsolutePath() + File.separator + fileName);
                part2.write(this.getFolderUpload().getAbsolutePath() + File.separator + fileName);                
                part2.write(this.getFolderUpload2().getAbsolutePath() + File.separator + fileName);                
            }
        }
        
        
        ValidationDate vd = new ValidationDate();
        String nid = request.getParameter("nid");
        String title = request.getParameter("title");
        String canewId = request.getParameter("cate");
        String description = request.getParameter("desc");
        String alertMessage = "update News Successfully";
        String redirectUrl = "postlist";
        
        if (files.isEmpty() && files2.isEmpty()) {
            files = request.getParameter("img");
            files2 = request.getParameter("ndimg");
        }
        NewsDao ndao = new NewsDao();
        if (title != null && !title.isEmpty() && canewId != null && !canewId.isEmpty() && description != null && !description.isEmpty() && files2 != null && !files2.isEmpty() && files2 != null && !files2.isEmpty()) {
            int canewid = Integer.parseInt(canewId);
            if (canewid > 0) {
                News n = News.builder()
                .nid(Integer.parseInt(nid))
                .title(title)
                .img(files)
                .createat(vd.getCurrentTime())
                .acid(6)
                .canewId(canewid)
                .build();
        ndao.updateNews(n);

        NewsDetails nd = NewsDetails.builder()
                .nid(Integer.parseInt(nid))
                .description(description)
                .img(files2)
                .build();

        ndao.updateNewsDetails(nd);
            }
        }
        
        String script = "<script type='text/javascript'>alert('" + alertMessage + "');window.location.href='" + redirectUrl + "';</script>";
            response.getWriter().println(script);

        
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
