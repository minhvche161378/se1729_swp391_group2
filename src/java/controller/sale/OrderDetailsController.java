/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.sale;

import Validation.ValidationDate;
import dao.AccountDao;
import dao.CodeSaleDao;
import dao.NewsDao;
import dao.OrderDao;
import dao.ProductDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.LinkedHashMap;
import java.util.List;
import model.Account;
import model.CategoriesNew;
import model.Category;
import model.Order;
import model.OrderDetails;
import model.Product;

/**
 *
 * @author HP
 */
@WebServlet(name = "OrderDetails", urlPatterns = {"/orderDetails"})
public class OrderDetailsController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OrderDetails</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OrderDetails at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String oid = request.getParameter("oid");

        AccountDao ad = new AccountDao();
        ProductDao pd = new ProductDao();
        NewsDao nd = new NewsDao();
        OrderDao od = new OrderDao();
        CodeSaleDao cd = new CodeSaleDao();

        //List category header
        List<Category> caList = pd.getListCate();
        List<CategoriesNew> canewList = nd.getListCategoriesNew();
        request.setAttribute("caList", caList);
        request.setAttribute("canewList", canewList);

        //Order 
        Order order = od.getOrderById(oid);
        Account accCustomer = ad.getAccountByoid(oid);

        LinkedHashMap<model.OrderDetails, Product> list = new LinkedHashMap<>();
        List<model.OrderDetails> orderDetailsList = od.getOrderDetails(oid);
        for (model.OrderDetails ord : orderDetailsList) {
            Product p = od.getProductByIdOrderDetails(ord.getOdid());
            list.put(ord, p);

        }

        request.setAttribute("order", order);
        request.setAttribute("accCustomer", accCustomer);
        request.setAttribute("list", list);
        request.getRequestDispatcher("OrderDetails.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String oid = request.getParameter("oid");
        String type = request.getParameter("type");
           ValidationDate valdate = new ValidationDate();

      
        String currenDate = valdate.getCurrentTime();
        OrderDao od = new OrderDao();
        ProductDao pd = new ProductDao();
        try {
            if (type.equals("cancel")) {
                  
            // Huy don hang
                od.cancelOrder(oid,currenDate);
               
               
            } else if(type.equals("confirm")) {
                od.confirmOrder(oid);
               
            }else if(type.equals("complete")) {
               
                  List<OrderDetails> orderDetailsList = od.getOrderDetails(oid);
                    for (OrderDetails orderDetails : orderDetailsList) {
                    int productquantity = pd.getQuantityProductByPid(orderDetails.getPid());
                    int saled = pd.getQuantitSaledyByPid(orderDetails.getPid());
                    pd.UpdateQuantityProductWhenOrder(orderDetails.getPid(),
                    productquantity - orderDetails.getQuantity(),saled + orderDetails.getQuantity());
                    }
                  od.completeOrder(oid,currenDate);
            }

          response.sendRedirect("orderDetails?oid=" + oid);

        } catch (Exception e) {

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
