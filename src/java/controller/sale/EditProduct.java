/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.sale;

import dao.ProductCategoryDao;
import dao.ProductDao;
import dao.ProductDetailDao;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import model.Category;
import model.Product;
import model.ProductDetail;

/**
 *
 * @author tuanm
 */
@WebServlet(name = "EditProduct", urlPatterns = {"/edit"})
public class EditProduct extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditProduct</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditProduct at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          ProductDetailDao pd = new ProductDetailDao();
        ProductDao p = new ProductDao();

        String Pid = request.getParameter("Pid");

        ProductDetail listpd = pd.getProductDetailByPid(Pid);
        Product listp = p.getProductByID(Pid);
        List<Category> calist = p.getListCate();

        if (listp != null && listpd != null && calist != null) {
            request.setAttribute("listp", listp);
            request.setAttribute("listpd", listpd);
            request.setAttribute("calist", calist);
        }

        request.getRequestDispatcher("EditProduct.jsp").forward(request, response);
    }


/**
 * Handles the HTTP <code>POST</code> method.
 *
 * @param request servlet request
 * @param response servlet response
 * @throws ServletException if a servlet-specific error occurs
 * @throws IOException if an I/O error occurs
 */
@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pid_raw = request.getParameter("id");
        String status = request.getParameter("status");
        String pname = request.getParameter("pname");
        String image = request.getParameter("image");
        String price_raw = request.getParameter("price");
        String quantity_raw = request.getParameter("quantity");
        String description = request.getParameter("description");
        String rules = request.getParameter("rules");
        String numplayer_raw = request.getParameter("numplayer");
        String timeplay = request.getParameter("timeplay");
        String requiredAge_raw = request.getParameter("requiredAge");
        String caid_raw = request.getParameter("category");

        // Chuyển đổi các dữ liệu cần thiết sang kiểu dữ liệu mong muốn
        int price = Integer.parseInt(price_raw);
        int quantity = Integer.parseInt(quantity_raw);
        int requiredAge = Integer.parseInt(requiredAge_raw);
        int numplayer = Integer.parseInt(numplayer_raw);
        int caid = Integer.parseInt(caid_raw);
        int pid = Integer.parseInt(pid_raw);

        // Kiểm tra lỗi
        String error = "";
        if (pname.trim().isEmpty()) {
            error = "Không được để trống tên của sản phẩm !";
        } else if (price < 0 || price > 999999999) {
            error = "Vui lòng nhập giá sản phẩm hợp lệ !";
        } else if (quantity < 0 || quantity > 999999999) {
            error = "Vui lòng nhập số lượng sản phẩm hợp lệ !";
        } else if (requiredAge < 0 || requiredAge > 110) {
            error = " Vui lòng nhập độ tuổi hợp lệ !";
        } else if (numplayer < 0 || numplayer > 100) {
            error = " Vui lòng nhập số người chơi hợp lệ !";
        } else if (description.trim().isEmpty() || rules.trim().isEmpty()) {
            error = "Không được để trống mô tả sản phẩm !";
         } else if (rules.trim().isEmpty()) {
            error = "Không được để trống luật chơi sản phẩm !";
        }

        // Nếu có lỗi, đặt thuộc tính 'error' và forward về trang Edit.jsp
        if (!error.isEmpty()) {
            ProductDetailDao pd = new ProductDetailDao();
            ProductDao p = new ProductDao();

            // Lấy lại dữ liệu sản phẩm và danh sách danh mục
            ProductDetail listpd = pd.getProductDetailByPid(pid_raw);
            Product listp = p.getProductByID(pid_raw);
            List<Category> calist = p.getListCate();

            if (listp != null && listpd != null && calist != null) {
                request.setAttribute("listp", listp);
                request.setAttribute("listpd", listpd);
                request.setAttribute("calist", calist);
            }

            request.setAttribute("error", error);
            request.getRequestDispatcher("EditProduct.jsp").forward(request, response);
        } else {
            // Nếu không có lỗi, cập nhật thông tin sản phẩm và chuyển hướng về trang quản lý sản phẩm
            ProductDao pdao = new ProductDao();
            ProductDetailDao pddao = new ProductDetailDao();
            ProductCategoryDao pcd = new ProductCategoryDao();

            pdao.updateProduct(pname, price, image, quantity, status, pid);
            pddao.updateProductDetail(description, rules, timeplay, requiredAge, numplayer, pid);
            pcd.updateProductCategory(caid, pid);
            response.sendRedirect("manageproduct");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
