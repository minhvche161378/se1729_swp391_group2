package model;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
/**
 *
 * @author FPT
 */
@Builder
@Getter
@Setter
@ToString
public class Product {
    private int pid;
    private String pname;
    private float rate;
    private String img;
    private int price;
    private int priceSale;
    private int quantity;
    private int pubid;
    private int saled;
    private boolean isDiscount;
    private boolean isSoldout;
    private String created_at;
    private int status;

    public boolean isIsDiscount() {
        return isDiscount;
    }

    public void setIsDiscount(boolean isDiscount) {
        this.isDiscount = isDiscount;
    }

}
