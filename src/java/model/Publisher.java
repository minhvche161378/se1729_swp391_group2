/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author tuanm
 */
@Builder
@Getter
@Setter
@ToString
public class Publisher {
    private int pubid;
    private String pubname;
    private String country;
    private String material;
    private String size;
    private String weight;
    private int pid;
}
