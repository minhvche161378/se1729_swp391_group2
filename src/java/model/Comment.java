/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author hihih
 */
@Builder
@Getter
@Setter
@ToString
public class Comment {

    private int CommentId;
    private String CommentContent;
    private String Create_at;
    private int Status;
    private int ProductId;
    private int AccountId;
    private int Rate;
    private String CustomerName;
    private String CustomerEmail;

}
