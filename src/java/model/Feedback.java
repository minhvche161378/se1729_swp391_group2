/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author hihih
 */
@Builder
@Getter
@Setter
@ToString

public class Feedback {

    private int FeedbackID;
    private String FeedbackBySellerName;
    private String Create_atFeedback;
    private String FeedbackContent;
    private int AcountId;
    private int CommentId;
    private int StatusFeedback;
    

}
