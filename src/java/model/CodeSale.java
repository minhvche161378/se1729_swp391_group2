/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author tuanm
 */
@Builder
@Getter
@Setter
@ToString
public class CodeSale {
    private int csid;
    private String codeSale;
    private int discount;
    private int csStatus;
    private String dateStart;
    private String dateEnd;
    private int limitedQuantity;
     private int discountConditions;
     private String titile;
     private int  quantityUsed;
}
