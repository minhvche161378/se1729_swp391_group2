/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Validation;

import dao.ProductDao;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import model.Product;

/**
 *
 * @author HP
 */
public class Cart {
    
   
   
    
    // xử lí id trùng lặp
    public  String processString(String input) {
ProductDao pd = new ProductDao();
  Map<String, Integer> map = new HashMap<>();
  
  String[] parts = input.split("/");
  for(String part : parts) {
    String[] s = part.split(":");
    String id = s[0];
    int qty = Integer.parseInt(s[1]);
    
    if(map.containsKey(id)) {
      Product p = pd.getProductByID(id);
      if (p.getQuantity() >= qty + map.get(id)) {
                qty += map.get(id);
                map.put(id, qty);
            }
      
    }else {
            Product p = pd.getProductByID(id);
         
                map.put(id, qty);
            
    
   }
  }

  String result = "";
  for(Map.Entry<String, Integer> entry : map.entrySet()) {
        result += entry.getKey() + ":" + entry.getValue() + "/";
    }

    // Kiểm tra nếu chuỗi kết quả không rỗng thì loại bỏ ký tự '/' cuối cùng
    if (!result.isEmpty()) {
        result = result.substring(0, result.length() - 1);
    }

    return result;
}
    
    public  String updateCartQuantity (String input, String pid, int newQty) {

  Map<String, Integer> map = new HashMap<>();

  String[] parts = input.split("/");
  for(String part : parts) {
    String[] s = part.split(":");
    String id = s[0];
    int qty = Integer.parseInt(s[1]);

    map.put(id, qty);
  }

  // Cập nhật lại số lượng của key cần thay đổi
  map.put(pid, newQty);

  // Chuyển map về chuỗi
  String result = "";
  for(Map.Entry<String, Integer> entry : map.entrySet()) {
        result += entry.getKey() + ":" + entry.getValue() + "/";
    }

    // Kiểm tra nếu chuỗi kết quả không rỗng thì loại bỏ ký tự '/' cuối cùng
    if (!result.isEmpty()) {
        result = result.substring(0, result.length() - 1);
    }

    return result;
  }

    public  String getQuantityBypid(String input, String[] ids) {

  Map<String, Integer> map = new HashMap<>();

  // Chuyển chuỗi sang map
  String[] parts = input.split("/");
  for (String part : parts) {
    String[] split = part.split(":");
    String id = split[0];
    int qty = Integer.parseInt(split[1]);
    map.put(id, qty);
  }

  // Lấy ra các phần tử có id trùng với ids
   String result = "";
    for (String id : ids) {
        Integer qty = map.get(id);
        if (qty != null) {
            result += id + ":" + qty + "/";
        }
    }

  
    if (!result.isEmpty()) {
        result = result.substring(0, result.length() - 1);
    }

    return result;

}
    
    public  String removeProductinCart(String input, String pid) {
if(input.equals("") || input==null){
    return "";
}
  Map<String, Integer> map = new HashMap<>();
  
  String[] parts = input.split("/");
  for(String part : parts) {
    String[] s = part.split(":");
    String id = s[0];
    int qty = Integer.parseInt(s[1]);
    
    if(!id.equals(pid)) {
      map.put(id, qty); 
    }
  }

  // Chuyển map về chuỗi
   String result = "";
  if(map.isEmpty()){
      return "";
  }else{
       for(Map.Entry<String, Integer> entry : map.entrySet()) {
   
       result += entry.getKey() + ":" + entry.getValue() + "/";
    }

    // Kiểm tra nếu chuỗi kết quả không rỗng thì loại bỏ ký tự '/' cuối cùng
    if (!result.isEmpty()) {
        result = result.substring(0, result.length() - 1);
    }

    return result;
  }
 

}
    
public String removeDuplicateElements(String s, String s1) {
    Map<String, String> mapS = new HashMap<>();

    // Chuyển chuỗi 1 về map
    String[] parts = s.split("/");
    for (String part : parts) {
        String[] a = part.split(":");
        String id = a[0];
        String qty = a[1];
        mapS.put(id, qty);
    }

    // Chuyển chuỗi 2 về map
    String[] parts1 = s1.split("/");
    for (String part1 : parts1) {
        String[] a1 = part1.split(":");
        String id = a1[0];
        if (mapS.containsKey(id)) {
            mapS.remove(id);
        }
    }

    String result = "";
    // Chuyển mapS về chuỗi
    for (Map.Entry<String, String> entry : mapS.entrySet()) {
        result += entry.getKey() + ":" + entry.getValue() + "/";
    }

    // Kiểm tra nếu chuỗi kết quả không rỗng thì loại bỏ ký tự '/' cuối cùng
    if (!result.isEmpty()) {
        result = result.substring(0, result.length() - 1);
    }

    return result;
}

    
    public LinkedHashMap<Product, Integer> getCart(String txt) {
    ProductDao pd = new ProductDao();
    LinkedHashMap<Product, Integer> historyMap = new LinkedHashMap<>();
   
    try {
        if (txt != null && txt.length() != 0) {
            String[] s = txt.split("/");
            
            for (String i : s) {
                String[] n = i.split(":");
                
                if (n.length == 2) {
                    String pid = n[0];
                    int quantity = Integer.parseInt(n[1]);
                    
                    Product p = pd.getProductbyID(pid);
                    
                    if (p != null) {
                        historyMap.put(p, quantity);
                    }
                }
            }
        }
    } catch (NumberFormatException e) {
        // Handle the NumberFormatException here if needed.
        e.printStackTrace();
    }
    
    return historyMap;
}


public static void main(String[] args) {

       
 
   
}

     }

