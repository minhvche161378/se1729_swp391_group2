/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import context.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.ProductDetail;

/**
 *
 * @author tuanm
 */
public class ProductDetailDao extends DBContext{
       public ProductDetail getProductDetailByPid(String Pid) {
        try {
            String sql = "  select* from [ProductDetail] where pid = " + Pid;
            PreparedStatement st = connection.prepareStatement(sql);
         
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ProductDetail pd = ProductDetail.builder()
                        .pdid(rs.getInt("pdid"))
                        .numPlayer(rs.getInt("numPlayer"))
                        .requiredAge(rs.getInt("requiredAge"))
                        .description(rs.getString("description"))
                        .rules(rs.getString("rules"))
                        .timeplay(rs.getString("timeplay"))
                        .pid(rs.getInt("pid"))
                        .build();
               return pd;
            }
        } catch (Exception e) {
            System.out.println("get Product Detail By Pid " + e.getMessage());
        }
        return null;
    }
       public void updateProductDetail(String description, String rules, String timeplay, int requiredAge, int numplayer, int pid) {

        try {
            String sql = "UPDATE [WebBoardGame4].[dbo].[ProductDetail]\n"
                    + "SET \n"
                    + "  numplayer = ?,\n"
                    + "  requiredAge = ?,\n"
                    + "  description = ?,\n"
                    + "  rules = ?,\n"
                    + "  timeplay = ?\n"
                    + "WHERE pid = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, numplayer);
            st.setInt(2, requiredAge);
            st.setString(3, description);
            st.setString(4, rules);
            st.setString(5, timeplay);
            st.setInt(6, pid);

            st.execute();

        } catch (Exception e) {
        }
    }
        public void insertProductDetail(int numplayer, int requiredAge, String description, String rules, String timeplay) {

        int pid = 0;
        try {
            String sql = " Select MAX(pid) AS Maxpid from Products";

            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                pid = rs.getInt("Maxpid");

            }

        } catch (Exception e) {
            System.out.println("getpid " + e.getMessage());
        }

        try {
            String sql = " INSERT INTO [WebBoardGame].[dbo].[ProductDetail] \n"
                    + "(numplayer, requiredAge, description, rules, timeplay, pid)\n"
                    + "VALUES\n"
                    + "(?, ?, ?, ?, ?,? )";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, numplayer);
            st.setInt(2, requiredAge);
            st.setString(3, description);
            st.setString(4, rules);
            st.setString(5, timeplay);
            st.setInt(6, pid);
            st.execute();

        } catch (Exception e) {
        }
    }
             public void deleteProductDetails(int pid) {

        try {
            String sql = "delete from ProductDetail where pid = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, pid);
            st.executeUpdate();

        } catch (Exception e) {
        }
    }
      public static void main(String[] args) {
        ProductDetailDao pd = new ProductDetailDao();
          System.out.println(pd.getProductDetailByPid("9"));
      
                 
    }
}
