/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;


/**
 *
 * @author hihih
 */
import context.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Feedback;

public class FeedbackDao extends DBContext {
    public boolean editComment(int commentId, String newContent, int newStatus) {
        String updateQuery = "UPDATE Feedback SET FeedbackContent=?, Status=? WHERE FeedbackID=?";
        try (
                 PreparedStatement stmt = connection.prepareStatement(updateQuery)) {
            stmt.setString(1, newContent);
            stmt.setInt(2, newStatus);
            stmt.setInt(3, commentId);

            int rowsAffected = stmt.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<Feedback> getAllFeedbacks() {
        List<Feedback> feedbacks = new ArrayList<>();
        String sql = "SELECT * FROM Feedback";
        try ( PreparedStatement statement = connection.prepareStatement(sql);  ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Feedback feedback = Feedback.builder()
                        .FeedbackID(resultSet.getInt("FeedbackID"))
                        .FeedbackBySellerName(resultSet.getString("FeedbackBySellerName"))
                        .Create_atFeedback(resultSet.getString("Create_atFeedback"))
                        .FeedbackContent(resultSet.getString("FeedbackContent"))
                        .AcountId(resultSet.getInt("AcountId"))
                        .CommentId(resultSet.getInt("CommentId"))
                        .StatusFeedback(resultSet.getInt("StatusFeedback"))
                        .build();
                feedbacks.add(feedback);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return feedbacks;
    }

    // Thêm một phản hồi mới vào cơ sở dữ liệu
    public void addFeedback(Feedback feedback) {
        String sql = "INSERT INTO Feedback (FeedbackBySellerName, Create_atFeedback, FeedbackContent, AcountId, CommentId, StatusFeedback) "
                + "VALUES (?, ?, ?, ?, ?,?)";
        try ( PreparedStatement statement = connection.prepareStatement(sql)) {
            Date currentDate = new Date();
            // Định dạng ngày/tháng/năm (dd/MM/yyyy)
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
            // Chuyển đổi thời gian hiện tại sang chuỗi theo định dạng
            String formattedDate = dateFormat.format(currentDate);
            statement.setString(1, feedback.getFeedbackBySellerName());
            statement.setString(2, formattedDate);
            statement.setString(3, feedback.getFeedbackContent());
            statement.setInt(4, feedback.getAcountId());
            statement.setInt(5, feedback.getCommentId());
            statement.setInt(6, feedback.getStatusFeedback());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean addFeedback(String feedbackBySellerName, String feedbackContent, int accountId, int commentId) {
        String insertQuery = "INSERT INTO feedbacks (FeedbackBySellerName, Create_atFeedback, FeedbackContent, AcountId, CommentId) VALUES (?, ?, ?, ?, ?)";
        try (
                 PreparedStatement stmt = connection.prepareStatement(insertQuery)) {
            stmt.setString(1, feedbackBySellerName);
            stmt.setString(2, "");
            stmt.setString(3, feedbackContent);
            stmt.setInt(4, accountId);
            stmt.setInt(5, commentId);
            stmt.setInt(5, 1);

            int rowsAffected = stmt.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
        public List<Feedback> getFeedbackByCommentId(int commentId) {
        List<Feedback> feedbackList = new ArrayList<>();
        String query = "SELECT * FROM Feedback WHERE CommentId = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, commentId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Feedback feedback =  Feedback.builder()
                .FeedbackID(resultSet.getInt("FeedbackID"))
                .FeedbackBySellerName(resultSet.getString("FeedbackBySellerName"))
                .Create_atFeedback(resultSet.getString("Create_atFeedback"))
                .FeedbackContent(resultSet.getString("FeedbackContent"))
                .AcountId(resultSet.getInt("AcountId"))
                .CommentId(resultSet.getInt("CommentId"))
                .StatusFeedback(resultSet.getInt("StatusFeedback"))
                        .build();
                feedbackList.add(feedback);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return feedbackList;
    }

   

   public List<Feedback> search(String searchKeyword) {
        List<Feedback> results = new ArrayList<>();
        String sql = "SELECT * FROM feedback WHERE FeedbackContent LIKE ?";

        try (
             PreparedStatement stmt = connection.prepareStatement(sql)) {
            // Thiết lập tham số và thực hiện truy vấn
            stmt.setString(1, "%" + searchKeyword + "%");
            ResultSet rs = stmt.executeQuery();

            // Lặp qua kết quả và tạo đối tượng Feedback cho mỗi bản ghi
            while (rs.next()) {
                Feedback feedback = Feedback.builder()
                .FeedbackID(rs.getInt("FeedbackID"))
                .FeedbackBySellerName(rs.getString("FeedbackBySellerName"))
                .Create_atFeedback(rs.getString("Create_atFeedback"))
                .FeedbackContent(rs.getString("FeedbackContent"))
                .AcountId(rs.getInt("AcountId"))
                .CommentId(rs.getInt("CommentId"))
                .StatusFeedback(rs.getInt("StatusFeedback"))
.build();
                results.add(feedback);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return results;
    }
    public static void main(String[] args) {
       FeedbackDao feedbackDao = new FeedbackDao();
        System.out.println(feedbackDao.search("a"));
    }
}

