/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import context.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Publisher;

/**
 *
 * @author tuanm
 */
public class PublisherDao extends DBContext {

    public Publisher getPublisherByPid(String Pid) {

        try {
            String sql = "   select*from Publishers where pid =   " + Pid;
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Publisher pub = Publisher.builder()
                        .pubid(rs.getInt("pubid"))
                        .pubname(rs.getString("pubname"))
                        .country(rs.getString("country"))
                        .material(rs.getString("material"))
                        .size(rs.getString("size"))
                        .weight(rs.getString("weight"))
                        .pid(rs.getInt("pid"))
                        .build();
                return pub;
            }
        } catch (Exception e) {
            System.out.println("get Publisher By Pid " + e.getMessage());
        }
        return null;
    }
     public void insertPublisher(String pubname, String country, String material, String size, String weight) {

        int pid = 0;
        try {
            String sql = " Select MAX(pid) AS Maxpid from Products";

            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                pid = rs.getInt("Maxpid");

            }

        } catch (Exception e) {
            System.out.println("getpid " + e.getMessage());
        }

        try {
            String sql = "   INSERT INTO Publishers (pubname, country, material, size, weight, pid)\n"
                    + "VALUES (?, ?, ?, ?, ?, ?);";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, pubname);
            st.setString(2, country);
            st.setString(3, material);
            st.setString(4, size);
            st.setString(5, weight);
            st.setInt(6, pid);
            st.execute();

        } catch (Exception e) {
        }
    }

    public static void main(String[] args) {
        PublisherDao pubd = new PublisherDao();
        System.out.println(pubd.getPublisherByPid("1"));
    }
}
