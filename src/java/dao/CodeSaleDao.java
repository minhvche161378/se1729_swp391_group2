/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import context.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import model.CodeSale;
import model.Order;

/**
 *
 * @author HP
 */
public class CodeSaleDao extends DBContext {

    public List<CodeSale> getCodeSaleActive(String currentDate) {
        List<CodeSale> list = new ArrayList<>();
        String sql = "select * from [dbo].[CodeSale] where dateEnd > ? and csStatus=1 "
                + "Order by dateStart DESC";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, currentDate);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                CodeSale cs = CodeSale.builder()
                        .csid(rs.getInt("csid"))
                        .codeSale(rs.getString("codeSale"))
                        .csStatus(rs.getInt("csStatus"))
                        .dateStart(rs.getString("dateStart"))
                        .dateEnd(rs.getString("dateEnd"))
                        .discountConditions(rs.getInt("discountConditions"))
                        .discount(rs.getInt("discount"))
                        .titile(rs.getString("titile"))
                        .limitedQuantity(rs.getInt("limitedQuantity"))
                        .quantityUsed(rs.getInt("quantityUsed"))
                        .build();

                list.add(cs);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;

    }

    public CodeSale getCodeSalebyCode(String currentDate, String codeSale) {

        String sql = "select * from [dbo].[CodeSale] where limitedQuantity> quantityUsed and dateEnd > '" + currentDate + "' and csStatus=1 \n"
                + "                  and codeSale = '" + codeSale + "'";

        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                CodeSale cs = CodeSale.builder()
                        .csid(rs.getInt("csid"))
                        .codeSale(rs.getString("codeSale"))
                        .csStatus(rs.getInt("csStatus"))
                        .dateStart(rs.getString("dateStart"))
                        .dateEnd(rs.getString("dateEnd"))
                        .discountConditions(rs.getInt("discountConditions"))
                        .discount(rs.getInt("discount"))
                        .titile(rs.getString("titile"))
                        .limitedQuantity(rs.getInt("limitedQuantity"))
                        .quantityUsed(rs.getInt("quantityUsed"))
                        .build();
                return cs;

            }

        } catch (SQLException e) {
            System.out.println(e);
        }

        return null;

    }

    public CodeSale getCodeSalebyId(String csid) {

        String sql = "select * from [dbo].[CodeSale] where csid = " + csid;

        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                CodeSale cs = CodeSale.builder()
                        .csid(rs.getInt("csid"))
                        .codeSale(rs.getString("codeSale"))
                        .csStatus(rs.getInt("csStatus"))
                        .dateStart(rs.getString("dateStart"))
                        .dateEnd(rs.getString("dateEnd"))
                        .discountConditions(rs.getInt("discountConditions"))
                        .discount(rs.getInt("discount"))
                        .titile(rs.getString("titile"))
                        .limitedQuantity(rs.getInt("limitedQuantity"))
                        .quantityUsed(rs.getInt("quantityUsed"))
                        .build();
                return cs;

            }

        } catch (SQLException e) {
            System.out.println(e);
        }

        return null;

    }

    public void updateQuantityCodeSale(String csid) {

        try {
            String sql = " UPDATE [WebBoardGame].[dbo].[CodeSale] \n"
                    + "  set quantityUsed = quantityUsed+1\n"
                    + " where csid = " + csid;
            PreparedStatement st = connection.prepareStatement(sql);

            st.execute();

        } catch (Exception e) {
        }
    }

    public List<CodeSale> getAllCodeSale() {
        String sql = "SELECT * FROM [dbo].[CodeSale]";
        List<CodeSale> codeSale = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                CodeSale cs = CodeSale.builder()
                        .csid(rs.getInt("csid"))
                        .codeSale(rs.getString("codeSale"))
                        .csStatus(rs.getInt("csStatus"))
                        .dateStart(rs.getString("dateStart"))
                        .dateEnd(rs.getString("dateEnd"))
                        .discountConditions(rs.getInt("discountConditions"))
                        .discount(rs.getInt("discount"))
                        .titile(rs.getString("titile"))
                        .limitedQuantity(rs.getInt("limitedQuantity"))
                        .quantityUsed(rs.getInt("quantityUsed"))
                        .build();
                codeSale.add(cs);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return codeSale;
    }

    public CodeSale getCodeSaleById(String csid) {
        String sql = "Select * from CodeSale where csid = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, csid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                CodeSale cs = CodeSale.builder()
                        .csid(rs.getInt("csid"))
                        .codeSale(rs.getString("codeSale"))
                        .csStatus(rs.getInt("csStatus"))
                        .dateStart(rs.getString("dateStart"))
                        .dateEnd(rs.getString("dateEnd"))
                        .discountConditions(rs.getInt("discountConditions"))
                        .discount(rs.getInt("discount"))
                        .titile(rs.getString("titile"))
                        .limitedQuantity(rs.getInt("limitedQuantity"))
                        .quantityUsed(rs.getInt("quantityUsed"))
                        .build();
                return cs; // Trả về đối tượng Account đã tạo
            }
        } catch (SQLException e) {
            e.printStackTrace(); // In thông báo lỗi hoặc ghi log
        }

        return null;
    }

    public void insertCodeSale(String codeSale, int discount, int csStatus,
            String dateStart, String dateEnd, int limitedQuantity,
            String discountConditions, String titile, int quantityUsed) {
        String sql = "INSERT INTO [dbo].[CodeSale]\n"
                + "           ([codeSale]\n"
                + "           ,[discount]\n"
                + "           ,[csStatus]\n"
                + "           ,[dateStart]\n"
                + "           ,[dateEnd]\n"
                + "           ,[limitedQuantity]\n"
                + "           ,[discountConditions]\n"
                + "           ,[titile]\n"
                + "           ,[quantityUsed])\n"
                + "     VALUES (?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, codeSale);
            st.setInt(2, discount);
            st.setInt(3, csStatus);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date startDate = sdf.parse(dateStart);
            java.util.Date endDate = sdf.parse(dateEnd);

            st.setDate(4, new java.sql.Date(startDate.getTime()));
            st.setDate(5, new java.sql.Date(endDate.getTime()));

            st.setInt(6, limitedQuantity);
            st.setString(7, discountConditions);
            st.setString(8, titile);
            st.setInt(9, quantityUsed);

            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        } catch (ParseException ex) {
            System.out.println(ex);
        }
    }

    public void updateCodeSale(int csid, String codeSale, int discount, int csStatus,
            String dateStart, String dateEnd, int limitedQuantity,
            String discountConditions, String titile) {
        String sql = "UPDATE [dbo].[CodeSale]\n"
                + "   SET [codeSale] = ?\n"
                + "      ,[discount] = ?\n"
                + "      ,[csStatus] = ?\n"
                + "      ,[dateStart] = ?\n"
                + "      ,[dateEnd] = ?\n"
                + "      ,[limitedQuantity] = ?\n"
                + "      ,[discountConditions] = ?\n"
                + "      ,[titile] = ?\n"
               
                + " WHERE csid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, codeSale);
            st.setInt(2, discount);
            st.setInt(3, csStatus);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date startDate = sdf.parse(dateStart);
            java.util.Date endDate = sdf.parse(dateEnd);

            st.setDate(4, new java.sql.Date(startDate.getTime()));
            st.setDate(5, new java.sql.Date(endDate.getTime()));
            st.setInt(6, limitedQuantity);
            st.setString(7, discountConditions);
            st.setString(8, titile);
          
            st.setInt(9, csid);

            st.executeUpdate();
        } catch (Exception e) {
        }

    }

    public CodeSale getCodeSaleById(int csid) {
        String sql = "SELECT * FROM codesale WHERE csid=?";
        List<CodeSale> codeSale = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, csid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                CodeSale cs = CodeSale.builder()
                        .csid(rs.getInt("csid"))
                        .codeSale(rs.getString("codeSale"))
                        .discount(rs.getInt("discount"))
                        .csStatus(rs.getInt("csStatus"))
                        .dateStart(rs.getString("dateStart"))
                        .dateEnd(rs.getString("dateEnd"))
                        .limitedQuantity(rs.getInt("limitedQuantity"))
                        .discountConditions(rs.getInt("discountConditions"))
                        .titile(rs.getString("title"))
                        .quantityUsed(rs.getInt("quantityUsed"))
                        .build();
            }
        } catch (Exception e) {
        }
        return null;

    }

    public boolean checkEmailExist(String email) {
        try {
            String str = "select * from Accounts\n"
                    + "where email=?";
            PreparedStatement pstm = connection.prepareStatement(str);
            pstm.setString(1, email);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public void deleteCodeSale(String csid) {
        String sql = "DELETE FROM [dbo].[CodeSale]\n"
                + "      WHERE csid = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, csid);
            st.executeUpdate();
        } catch (Exception e) {
        }

    }
    
    public int getTotalCodeSaleCount() {
    int totalCodeSaleCount = 0;

    try {
        String sql = "SELECT COUNT(*) AS TotalCount FROM CodeSale";
        PreparedStatement st = connection.prepareStatement(sql);
        ResultSet resultSet = st.executeQuery();

        if (resultSet.next()) {
            totalCodeSaleCount = resultSet.getInt("TotalCount");
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }

    return totalCodeSaleCount;
}
     public CodeSale getCodeSaleFromId(int csid) {
        String sql = "Select * from CodeSale where csid = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, csid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                CodeSale cs = CodeSale.builder()
                        .csid(rs.getInt("csid"))
                        .codeSale(rs.getString("codeSale"))
                        .csStatus(rs.getInt("csStatus"))
                        .dateStart(rs.getString("dateStart"))
                        .dateEnd(rs.getString("dateEnd"))
                        .discountConditions(rs.getInt("discountConditions"))
                        .discount(rs.getInt("discount"))
                        .titile(rs.getString("titile"))
                        .limitedQuantity(rs.getInt("limitedQuantity"))
                        .quantityUsed(rs.getInt("quantityUsed"))
                        .build();
                return cs; 
            }
        } catch (SQLException e) {
            e.printStackTrace(); // In thông báo lỗi hoặc ghi log
        }

        return null;

    }
       public int updateCodeSaleStatus(CodeSale a, int status) {
        String sql = "update CodeSale set status=? where csid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, status);
            st.setInt(2, a.getCsid());
            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;

    }
        
       

    public static void main(String[] args) {
        CodeSaleDao codeSaleDao = new CodeSaleDao();

        codeSaleDao.deleteCodeSale("16");
                }

}
