/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import context.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import model.Account;
import model.Category;
import model.CategoriesNew;
import model.News;
import model.NewsDetails;

/**
 *
 * @author hihih
 * @author tuanm
 */
public class NewsDao extends DBContext {

    public List<News> getListNew() {
        List<News> data = new ArrayList<>();
        try {
            String sql = "select*from News where status=1"
                    + " Order by [createat] DESC";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {

                News news = News.builder()
                        .nid(rs.getInt("nid"))
                        .title(rs.getString("title"))
                        .img(rs.getString("img"))
                        .createat(rs.getString("createat"))
                        .acid(rs.getInt("acid"))
                        .canewId(rs.getInt("canewId"))
                        .build();
                data.add(news);

            }
            rs.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public int getNewById(int nid) {
        try {
            String sql = "select*from News where nid = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, nid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println("getNewById" + e.getMessage());
        }
        return 0;
    }

    public List<CategoriesNew> getListCategoriesNew() {
        String sql = "select * from [dbo].[CategoriesNew] where status=1 ";
        List<CategoriesNew> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                CategoriesNew c = CategoriesNew.builder()
                        .canewId(rs.getInt(1))
                        .name(rs.getString(2))
                        .build();
                list.add(c);

            }
        } catch (SQLException e) {
        }
        return list;
    }

    public List<News> getNewsBycanewId(String canewId) {
        ArrayList<News> data = new ArrayList<>();
        try {
            String sql = "    SELECT TOP (1000)\n"
                    + "    N.nid,\n"
                    + "    N.title,\n"
                    + "    N.img,\n"
                    + "    N.createat,\n"
                    + "    N.acid,\n"
                    + "    C.name,\n"
                    + "	C.canewId\n"
                    + "FROM\n"
                    + "    [dbo].[News] N\n"
                    + "INNER JOIN\n"
                    + "   [dbo].[CategoriesNew] C ON N.canewId = C.canewId\n"
                    + "	where C.canewId =" + canewId;

            sql += "  Order by [createat] DESC";
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                News news = News.builder()
                        .nid(rs.getInt("nid"))
                        .title(rs.getString("title"))
                        .img(rs.getString("img"))
                        .createat(rs.getString("createat"))
                        .acid(rs.getInt("acid"))
                        .canewId(rs.getInt("canewId"))
                        .build();
                data.add(news);
            }
        } catch (Exception e) {
            System.out.println("get News by canewId " + e.getMessage());
        }
        return data;
    }

    public News getNewsbyID(String nid) {

        try {
            String sql = "select * "
                    + "from News "
                    + "where nid = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, nid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                News news = News.builder()
                        .nid(rs.getInt("nid"))
                        .title(rs.getString("title"))
                        .img(rs.getString("img"))
                        .createat(rs.getString("createat"))
                        .acid(rs.getInt("acid"))
                        .canewId(rs.getInt("canewId"))
                        .build();
                return news;
            }
        } catch (Exception e) {
            System.out.println("getNewById" + e.getMessage());
        }
        return null;
    }

    public List<NewsDetails> getListNewsDetails() {
        List<NewsDetails> data = new ArrayList<>();
        try {
            String sql = "select * "
                    + "from NewsDetails";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {

                NewsDetails nd = NewsDetails.builder()
                        .ndid(rs.getInt("ndid"))
                        .description(rs.getString("description"))
                        .img(rs.getString("img"))
                        .nid(rs.getInt("nid"))
                        .build();
                data.add(nd);

            }
            rs.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public NewsDetails getNewsDetailsById(String ndid) {
        try {
            String sql = "select * "
                    + "from NewsDetails "
                    + "where nid = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, ndid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                NewsDetails nd = NewsDetails.builder()
                        .ndid(rs.getInt("ndid"))
                        .description(rs.getString("description"))
                        .img(rs.getString("img"))
                        .nid(rs.getInt("nid"))
                        .view(rs.getInt("view"))
                        .build();
                return nd;
            }
        } catch (Exception e) {
            System.out.println("getNewsDetailsById" + e.getMessage());
        }
        return null;
    }

    public List<News> listPost(String search, String sort) {
        List<News> data = new ArrayList<>();
        try {
            String sql = "select "
                    + "*"
                    + "from News ";

            if (search != null) {
                sql += " where title like '%" + search + "%'";
            }
            if (sort != null) {
                sql += "Order by " + sort + " ASC";
            } else {
                sql += "Order by nid DESC";
            }

            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                News n = News.builder()
                        .nid(rs.getInt("nid"))
                        .title(rs.getString("title"))
                        .img(rs.getString("img"))
                        .createat(rs.getString("createat"))
                        .canewId(rs.getInt("canewId"))
                        .acid(rs.getInt("acid"))
                        .build();
                data.add(n);
            }
            rs.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public void updateNews(News n) {
        try {
            String sqlNews = "UPDATE News\n"
                    + "SET "
                    + "title = ?,"
                    + "createat=?, "
                    + "img= ?, "
                    + "canewId = ?,"
                    + "acid=?\n"
                    + "WHERE nid = ?";

            // Update the "News" table
            PreparedStatement stn = connection.prepareStatement(sqlNews);
            stn.setString(1, n.getTitle());
            stn.setString(2, n.getCreateat());
            stn.setString(3, n.getImg());
            stn.setInt(4, n.getCanewId());
            stn.setInt(5, n.getAcid());
            stn.setInt(6, n.getNid());
            stn.executeUpdate();

        } catch (Exception e) {
            System.out.println("updateNews " + e.getMessage());
        }
    }

    public void updateNewsDetails(NewsDetails nd) {
        try {
            // Update the "NewsDetails" table
            String sqlNewsDetails = "UPDATE NewsDetails "
                    + "SET "
                    + "description = ?,"
                    + "img= ? \n"
                    + "WHERE nid = ?";

            PreparedStatement stnd = connection.prepareStatement(sqlNewsDetails);
            stnd.setString(1, nd.getDescription());
            stnd.setString(2, nd.getImg());
            stnd.setInt(3, nd.getNid());
            int updateNewsDetails = stnd.executeUpdate();

            if (updateNewsDetails > 0) {

                System.out.println("update successfull");
            } else {

                System.out.println("update failed. Transaction rolled back.");
            }
        } catch (Exception e) {
            System.out.println("update NewsDetails " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void insertNews(News n) {
        try {
            // Insert the "News" table
            String sqlNews = "INSERT INTO News "
                    + "([title], "
                    + "[img], "
                    + "[createat],"
                    + "[acid],"
                    + "[canewId])"
                    + "VALUES (?, ?, ?, ?, ?)";

            PreparedStatement stn = connection.prepareStatement(sqlNews);
            stn.setString(1, n.getTitle());
            stn.setString(2, n.getImg());
            stn.setString(3, n.getCreateat());
            stn.setInt(4, n.getAcid());
            stn.setInt(5, n.getCanewId());
            int rowsinsertNews = stn.executeUpdate();

            if (rowsinsertNews > 0) {
                connection.commit();
                System.out.println("Insert successfull");
            } else {
                // Rollback the transaction if any of the updates failed
                connection.rollback();
                System.out.println("Insert failed. Transaction rolled back.");
            }

        } catch (Exception e) {
            System.out.println("Insert News " + e.getMessage());
        }
    }

    public void insertNewsDetails(NewsDetails nd) {
        int nid = 0;
        try {
            String sqlGetNews = " Select MAX(nid) AS MaxNid from News";

            PreparedStatement st = connection.prepareStatement(sqlGetNews);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                nid = rs.getInt("MaxNid");

            }

        } catch (Exception e) {
            System.out.println("getnid " + e.getMessage());
        }

        try {

            String sqlNewsDetails = "INSERT INTO NewsDetails "
                    + "([description], "
                    + "[img],"
                    + "[nid])"
                    + "VALUES (?, ?, ?)";

            PreparedStatement stnd = connection.prepareStatement(sqlNewsDetails);
            stnd.setString(1, nd.getDescription());
            stnd.setString(2, nd.getImg());
            stnd.setInt(3, nid);
            int rowsInsertNewsDetails = stnd.executeUpdate();

            if (rowsInsertNewsDetails > 0) {
                connection.commit();
                System.out.println("Insert successfull");
            } else {
                // Rollback the transaction if any of the updates failed
                connection.rollback();
                System.out.println("Insert failed. Transaction rolled back.");
            }
        } catch (Exception e) {
            System.out.println("Insert NewsDetails " + e.getMessage());
        }

    }

    public void deletePostByID(String nid) {
        try {
            String sql = "delete "
                    + "from NewsDetails\n"
                    + "where nid=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, nid);
            st.executeUpdate();

            String str = "delete "
                    + " from News\n"
                    + "where nid=?";
            PreparedStatement pstm = connection.prepareStatement(str);
            pstm.setString(1, nid);
            pstm.executeUpdate();

        } catch (Exception e) {
            System.out.println("Delete Post: " + e.getMessage());
        }
    }

    public CategoriesNew getCateNewbyId(String canewId) {
        try {
            String sql = "select name "
                    + "from CategoriesNew\n"
                    + "where canewId = " + canewId;
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                CategoriesNew ca = CategoriesNew.builder()
                        .name(rs.getString("name"))
                        .build();
                return ca;
            }
        } catch (Exception e) {
            System.out.println("getCategoriesNewById " + e.getMessage());
        }
        return null;
    }

    public Account getUserNameById(String acid) {
        try {
            String sql = "select username "
                    + "from Accounts\n"
                    + "where acid = " + acid;
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Account ac = Account.builder()
                        .username(rs.getString("username"))
                        .build();
                return ac;
            }
        } catch (Exception e) {
            System.out.println("getUserNameById " + e.getMessage());
        }
        return null;
    }

    public List<News> getListByPage(List<News> list, int begin, int end) {
        List<News> myList = new ArrayList<>();
        int myEnd = Math.min(end, list.size());
        for (int i = begin; i < myEnd; i++) {
            myList.add(list.get(i));
        }
        return myList;
    }

    public int getTotalNewsCount() {
        int totalNewsCount = 0;

        try {
            String sql = "SELECT COUNT(*) AS TotalCount FROM News";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet resultSet = st.executeQuery();

            if (resultSet.next()) {
                totalNewsCount = resultSet.getInt("TotalCount");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return totalNewsCount;
    }

    public List<News> getPostSorted(String atribute) {
        List<News> list = new ArrayList<>();
        String sql = "select "
                + "*"
                + "from News "
                + "\n"
                + "Order by ";
        if (atribute.trim().equals("nid")) {
            sql += " nid desc";
        }
        if (atribute.trim().equals("createat")) {
            sql += " createat asc";
        }
        if (atribute.trim().equals("title")) {
            sql += " title asc";
        }
        if (atribute.trim().equals("canewId")) {
            sql += " canewId asc";
        }
        if (atribute.trim().equals("acid")) {
            sql += " acid asc";
        }

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                News n = News.builder()
                        .nid(rs.getInt("nid"))
                        .title(rs.getString("title"))
                        .img(rs.getString("img"))
                        .createat(rs.getString("createat"))
                        .canewId(rs.getInt("canewId"))
                        .acid(rs.getInt("acid"))
                        .build();
                list.add(n);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public int getTotalView() {
        int toltalView = 0;
        try {
            String sql = "SELECT SUM([view]) AS total FROM NewsDetails";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                toltalView = rs.getInt("total");
            }
        } catch (Exception e) {
            System.out.println("total View " + e.getMessage());
        }
        return toltalView;
    }

    public CategoriesNew getCategoryNewbyId(List<CategoriesNew> list, int canewId) {
        for (CategoriesNew cate : list) {
            if (cate.getCanewId() == canewId) {
                return cate;
            }
        }
        return null;
    }

    public NewsDetails getViewbyId(String nid) {
        try {
            String sql = "select [view] "
                    + "from NewsDetails\n"
                    + "where nid = " + nid;
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                NewsDetails nd = NewsDetails.builder()
                        .view(rs.getInt("view"))
                        .build();
                return nd;
            }
        } catch (Exception e) {
            System.out.println("getViewById " + e.getMessage());
        }
        return null;
    }

    public List<News> getTop5MostView() {
        List<News> list = new ArrayList<>();
        try {
            String sql = "SELECT TOP (5) n.nid, n.title, n.canewId, n.acid, nd.[view] FROM News n "
                    + "inner join NewsDetails nd on n.nid= nd.nid\n"
                    + "ORDER BY nd.[view]  DESC ;";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                News p = News.builder()
                        .nid(rs.getInt("nid"))
                        .title(rs.getString("title"))
                        .canewId(rs.getInt("canewId"))
                        .acid(rs.getInt("acid"))
                        .build();
                list.add(p);
            }
        } catch (Exception e) {
            System.out.println("top 10 " + e.getMessage());
        }
        return list;
    }

    public List<News> getTop5LeastView() {
        List<News> list = new ArrayList<>();
        try {
            String sql = "SELECT TOP (5) n.nid, n.title, n.canewId, n.acid, nd.[view] FROM News n "
                    + "inner join NewsDetails nd on n.nid= nd.nid\n"
                    + "ORDER BY nd.[view]  ASC ;";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                News p = News.builder()
                        .nid(rs.getInt("nid"))
                        .title(rs.getString("title"))
                        .canewId(rs.getInt("canewId"))
                        .acid(rs.getInt("acid"))
                        .build();
                list.add(p);
            }
        } catch (Exception e) {
            System.out.println("top 10 " + e.getMessage());
        }
        return list;
    }

    public static void main(String[] args) {
        NewsDao ndao = new NewsDao();
        System.out.println(ndao.getListNew());

    }

}
