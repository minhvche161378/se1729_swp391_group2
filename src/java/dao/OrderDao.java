/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import Validation.ValidationDate;
import context.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import model.Account;
import model.Order;
import model.OrderDetails;
import model.Product;

/**
 *
 * @author HP
 */
public class OrderDao extends DBContext {

    public List<Order> getListorder(int acid, String status) {

        List<Order> list = new ArrayList<>();
        String sql = "select * from [dbo].[Orders] o\n"
                + "               inner join Accounts a on a.acid = o.acid"
                + " where a.acid = " + acid;
        if(status!=null){
            sql+=" and o.status = "+status;
        }
        sql+=" Order by o.oid DESC";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Order o = Order.builder()
                        .oid(rs.getInt(1))
                        .acid(rs.getInt(2))
                        .ordered_at(rs.getString(3))
                        .totalAmount(rs.getInt(4))
                        .created_by(rs.getString(5))
                        .address(rs.getString(6))
                        .phonenumber(rs.getString(7))
                        .status(rs.getInt(8))
                        .note(rs.getString(9))
                        .receiver(rs.getString("receiver"))
                        .discount(rs.getInt("discount"))
                        .enddate("enddate")
                        .build();
                list.add(o);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    } 
     public List<Order> getListOrder(int acid, String status,String page_raw) {

        List<Order> list = new ArrayList<>();
        String sql = "select * from [dbo].[Orders] o\n"
                + "               inner join Accounts a on a.acid = o.acid"
                + " where a.acid = " + acid;
        if(status!=null){
            sql+=" and o.status = "+status;
        }
        sql+=" Order by o.oid DESC";
        
         try {
            int page = 1;
            if (page_raw != null) {
                page = Integer.parseInt(page_raw);
            }            
            
            sql += "   "
                    + "OFFSET " + (page - 1) * 10 + " ROWS\n"
                    + "FETCH FIRST 10 ROWS Only";
            
        } catch (Exception e) {
            
        }
        
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Order o = Order.builder()
                        .oid(rs.getInt(1))
                        .acid(rs.getInt(2))
                        .ordered_at(rs.getString(3))
                        .totalAmount(rs.getInt(4))
                        .created_by(rs.getString(5))
                        .address(rs.getString(6))
                        .phonenumber(rs.getString(7))
                        .status(rs.getInt(8))
                        .note(rs.getString(9))
                        .receiver(rs.getString("receiver"))
                        .discount(rs.getInt("discount"))
                        .enddate("enddate")
                        .build();
                list.add(o);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    } 
   

    public Order getOrderByAcid(int acid,int totalPrice ,String address) {
       
        String sql = "select Top 1 * from orders where acid = ? and "
                + "TotalAmount = ? and address = ?  "
                + "Order by ordered_at DESC";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
             st.setInt(1, acid);
             st.setInt(2, totalPrice);
             st.setString(3, address);
             
             
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
              Order o = Order.builder()
                        .oid(rs.getInt(1))
                        .acid(rs.getInt(2))
                        .ordered_at(rs.getString(3))
                        .totalAmount(rs.getInt(4))
                        .created_by(rs.getString(5))
                        .address(rs.getString(6))
                        .phonenumber(rs.getString(7))
                        .status(rs.getInt(8))
                       .note(rs.getString(9))
                      .receiver(rs.getString("receiver"))
                       .discount(rs.getInt("discount"))
                       .enddate("enddate")
                        .build();
               return o;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
    public Order getOrderById(String oid) {
       
        String sql = "select  * from orders where oid = "+oid;
             
        try {
            PreparedStatement st = connection.prepareStatement(sql);
        
             
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
              Order o = Order.builder()
                        .oid(rs.getInt(1))
                        .acid(rs.getInt(2))
                        .ordered_at(rs.getString(3))
                        .totalAmount(rs.getInt(4))
                        .created_by(rs.getString(5))
                        .address(rs.getString(6))
                        .phonenumber(rs.getString(7))
                        .status(rs.getInt(8))
                       .note(rs.getString(9))
                      .receiver(rs.getString("receiver"))
                       .discount(rs.getInt("discount"))
                       .enddate(rs.getString("enddate"))
                        .build();
               return o;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
      public List<OrderDetails> getOrderDetails(String oid) {
        List<OrderDetails> list = new ArrayList<>();
        String sql = "select * from [dbo].[orderDetails] od\n"
                + "               inner join orders o on o.oid = od.oid"
                + " where od.oid = " + oid;
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                OrderDetails od = OrderDetails.builder()
                        .odid(rs.getInt("odid"))
                        .oid(rs.getInt("oid"))
                        .pid(rs.getInt("pid"))
                        .price(rs.getInt("price"))
                        .quantity(rs.getInt("quantity"))
                        .build();
                list.add(od);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public void createOrder(int acid, String ordered_at, int totalPrice, String address, String phonenumber,String note,String receiver,int discount) {
        String sql = " INSERT INTO [dbo].[orders]\n"
                + "           ([acid]\n"
                + "           ,[ordered_at]\n"
                + "           ,[TotalAmount]          \n"
                + "           ,[address]\n"
                + "           ,[phonenumber]\n"
                + "           ,[status]\n"
                  + "           ,[note]\n"
                       + "           ,[receiver]\n"
                     + "           ,[discount])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?,0,?,?,?)\n"
                + "";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, acid);
            st.setString(2, ordered_at);
            st.setInt(3, totalPrice);
            st.setString(4, address);
            st.setString(5, phonenumber);
            st.setString(6, note);
             st.setString(7, receiver);
             st.setInt(8, discount);

            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);

        }
    }

    public void createOrderDetails(int pid,int oid ,int price,int quantity ) {
        String sql = " INSERT INTO [dbo].[orderDetails]\n"
                + "           ([pid]\n"
                + "           ,[oid]\n"
                + "           ,[price]\n"
                + "           ,[quantity])\n"
                + "     VALUES\n"
                + "           (?,?,?,?)";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, pid);
            st.setInt(2, oid);
            st.setInt(3, price);
            st.setInt(4, quantity);

            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);

        }
    }
    public Product getProductByIdOrderDetails(int odid){
        

        try {
            String sql = "select * from [dbo].orderDetails od\n" +

"			  inner join Products p on p.pid = od.pid\n" +
"               where od.odid= "+odid ;
            PreparedStatement pstm = connection.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            if (rs.next()) {
                 Product pro = Product.builder()
                        .pid(rs.getInt("pid"))
                        .pname(rs.getString("pname"))
                        .rate(rs.getFloat("rate"))
                        .price(rs.getInt("price"))
                        .img(rs.getString("img"))
                        .priceSale(rs.getInt("priceSale"))
                        .quantity(rs.getInt("quantity"))
                        .pubid(rs.getInt("pubid"))
                        .saled(rs.getInt("saled"))
                      
                        .isDiscount(rs.getBoolean("isDiscount"))
                        .isSoldout(rs.getBoolean("isSoldout"))
                        .build();
           return pro;
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return null;
    }
    
     public void cancelOrder(String oid ,String currentDate) {
        String sql = "Update orders set status = 3,enddate = '"+currentDate+"' where oid = "+oid;

        try {
            PreparedStatement st = connection.prepareStatement(sql);
         

            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);

        }
    }
       public void completeOrder(String oid ,String currentDate) {
        String sql = "Update orders set status = 2,enddate = '"+currentDate+"' where oid = "+oid;

        try {
            PreparedStatement st = connection.prepareStatement(sql);
         

            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);

        }
    }
       public void confirmOrder(String oid ) {
        String sql = "Update orders set status = 1 where oid = "+oid;

        try {
            PreparedStatement st = connection.prepareStatement(sql);
         

            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);

        }
    }
   public List<Order> getListOrder(String name,String dateFrom,String dateTo,String status,String page_raw) {
         int id=0;
        List<Order> list = new ArrayList<>();
        String sql = "select * from [dbo].[Orders] where oid>0 \n";   
        try{
             id = Integer.parseInt(name);
        }catch(Exception e){
            
        }
        if(status!=null){
            sql+=" and [status] = "+status;
        }
        if(name!=null && id==0){
            sql+= " and ( address like '%"+name+"%' or receiver like '%"+name+"%')";
        }else if(name!=null && id!=0){
              sql+= " and (oid="+id+" or address like '%"+name+"%' or receiver like '%"+name+"%')";
        }
        if(dateFrom !=null && dateTo!=null){
           sql+=" and ordered_at > '"+dateFrom+"' and ordered_at <'"+dateTo+"'" ;
        }
        
        sql+=" Order by [status] ASC,oid DESC ";
          try {
            int page = 1;
            if (page_raw != null) {
                page = Integer.parseInt(page_raw);
            }            
            
            sql += "   "
                    + "OFFSET " + (page - 1) * 7 + " ROWS\n"
                    + "FETCH FIRST 7 ROWS Only";
            
        } catch (Exception e) {
            
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Order o = Order.builder()
                        .oid(rs.getInt(1))
                        .acid(rs.getInt(2))
                        .ordered_at(rs.getString(3))
                        .totalAmount(rs.getInt(4))
                       .created_by(rs.getString(5))
                        .address(rs.getString(6))
                        .phonenumber(rs.getString(7))
                        .status(rs.getInt(8))
                        .note(rs.getString(9))
                        .receiver(rs.getString("receiver"))
                        .discount(rs.getInt("discount"))
                         .enddate("enddate")
                        .build();
                list.add(o);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

public int getTotalOrder(String name,String dateFrom,String dateTo,String status){
     String sql = "select * from [dbo].[Orders] where oid>0 \n";          
        if(status!=null){
            sql+=" and [status] = "+status;
        }
        if(name!=null){
            sql+= " and ( address like '%"+name+"%' or receiver like '%"+name+"%')";
        }
        if(dateFrom !=null && dateTo!=null){
           sql+=" and ordered_at > '"+dateFrom+"' and ordered_at <'"+dateTo+"'" ;
        }
          int a = 0;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = a + 1;
                
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return a;
}
 public int getNumberPage(int total, int numentries) {
        
        try {
            
            int countPage = 0;
            countPage = total / numentries;
            if (total % numentries != 0) {
                countPage++;
            }
            return countPage;
            
        } catch (Exception e) {
        }
        
        return 0;
    }
    public int gettotalAmount() {
        int toltalAmount = 0;
        try {
            String sql = "SELECT SUM(TotalAmount) AS total FROM Orders";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                toltalAmount = rs.getInt("total");
            }
        } catch (Exception e) {
            System.out.println("total Amount " + e.getMessage());
        }
        return toltalAmount;
    }

    public Map<Integer, Integer> getListTotalAmountPerWeek() {

         Map<Integer, Integer> Amount = new HashMap<>();
        String sql = "SELECT \n"
                + "DATEPART(WEEK, o.ordered_at) AS WeekNumber,\n"
                + "SUM(o.TotalAmount) AS TotalAmountPerWeek\n"
                + "FROM [dbo].[Orders] o \n"
                + "INNER JOIN Accounts a ON a.acid = o.acid \n"
                + "WHERE DATEDIFF(WEEK, o.ordered_at, GETDATE()) >= 0\n"
                + "GROUP BY DATEPART(WEEK, o.ordered_at)";

        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int weekNumb = rs.getInt("WeekNumber");
                int totalPerWeek = rs.getInt("TotalAmountPerWeek");
                Amount.put(weekNumb, totalPerWeek);
                
            }
        } catch (SQLException e) {
            System.out.println("total" + e.getMessage());
        }
        return Amount;
    }
 
    public static void main(String[] args) {
        OrderDao od = new OrderDao();
        String oid = "10";
        String txt =null;
   
        System.out.println(od.getOrderById("1026"));
    }
}
