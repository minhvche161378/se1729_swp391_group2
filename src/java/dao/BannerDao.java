/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

/**
 *
 * @author Thanh
 */
import context.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import model.Banner;

public class BannerDao extends DBContext {
    
    public List<Banner> getListTop3BannerbyStatus() {
        List<Banner> list = new ArrayList<>();
        try {
          String sql = "SELECT Top(3) * FROM Banner Where [status] = 1 Order by banid DESC";
            
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Banner ban = Banner.builder()
                        .banid(rs.getInt("banid"))
                        .img(rs.getString("img"))
                        .status(rs.getInt("status"))
                        .create_at(rs.getString("create_at"))
                        .build();
                list.add(ban);
            }
        } catch (Exception e) {
            System.out.println("list Banner " + e.getMessage());
        }
        return list;
    }

    public List<Banner> getListAllBanner(String sort) {
        List<Banner> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM Banner ";
            if(sort!=null){
             sql+= "Order by "+sort+" DESC";
          }else {
                sql += "Order by banid DESC";
            }
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Banner ban = Banner.builder()
                        .banid(rs.getInt("banid"))
                        .img(rs.getString("img"))
                        .status(rs.getInt("status"))
                        .create_at(rs.getString("create_at"))
                        .build();
                list.add(ban);
            }
        } catch (Exception e) {
            System.out.println("list Banner " + e.getMessage());
        }
        return list;
    }

    public Banner getBannerDetailbyId(String banid) {
        try {
            String sql = "select * "
                    + "from Banner\n"
                    + "where banid = " + banid;
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Banner ban = Banner.builder()
                        .banid(rs.getInt("banid"))
                        .img(rs.getString("img"))
                        .status(rs.getInt("status"))
                        .create_at(rs.getString("carete_at"))
                        .build();
                return ban;
            }
        } catch (Exception e) {
            System.out.println("getbanner " + e.getMessage());
        }
        return null;
    }

    public void addNewBanner(Banner ban) {
        try {
            String sql = "INSERT INTO Banner ([img], [status], [create_at]) VALUES (?, ?, ?)";

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, ban.getImg());
            st.setInt(2, ban.getStatus());
            st.setString(3, ban.getCreate_at());
            int addbanner = st.executeUpdate();
            if (addbanner > 0) {

                System.out.println("Insert successfull");
            } else {
                // Rollback the transaction if any of the updates failed

                System.out.println("Insert failed. Transaction rolled back.");
            }

        } catch (Exception e) {
            System.out.println("Insert Banner " + e.getMessage());
        }
    }

    public void deletebannerbyID(String banid) {
        try {
            String sql = "delete "
                    + "from Banner\n"
                    + "where banid=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, banid);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println("delete banner " + e.getMessage());
        }
    }

    public void updateBanner(Banner ban) {
        try {
            String sqlNews = "UPDATE Banner\n"
                    + "SET "
                    + "[img] = ?,"
                    + "[status]=?, "
                    + "[create]_at= ?\n "
                    + "WHERE banid = ?";

            // Update the "News" table
            PreparedStatement stn = connection.prepareStatement(sqlNews);
            stn.setString(1, ban.getImg());
            stn.setInt(2, ban.getStatus());
            stn.setString(3, ban.getCreate_at());
            stn.setInt(4, ban.getBanid());
            stn.executeUpdate();

        } catch (Exception e) {
            System.out.println("update Banner " + e.getMessage());
        }
    }
    
    public void updateBannerSatus(Banner ban) {
        try {
            String sqlNews = "UPDATE Banner\n"
                    + "SET "
                    + "[status]=?\n "
                    + "WHERE banid = ?";

            // Update the "News" table
            PreparedStatement stn = connection.prepareStatement(sqlNews);
            stn.setInt(1, ban.getStatus());
            stn.setInt(2, ban.getBanid());
            stn.executeUpdate();

        } catch (Exception e) {
            System.out.println("update Banner " + e.getMessage());
        }
    }
    
    public List<Banner> getListByPage(List<Banner> list, int begin, int end) {
        List<Banner> myList = new ArrayList<>();
        int myEnd = Math.min(end, list.size());
        for (int i = begin; i < myEnd; i++) {
            myList.add(list.get(i));
        }
        return myList;
    }
    
    public int getTotalBannerCount() {
    int totalBannerCount = 0;

    try {
        String sql = "SELECT COUNT(*) AS TotalCount FROM Banner";
        PreparedStatement st = connection.prepareStatement(sql);
        ResultSet resultSet = st.executeQuery();

        if (resultSet.next()) {
            totalBannerCount = resultSet.getInt("TotalCount");
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }

    return totalBannerCount;
}
    public Banner getBannerbyID (String banid){
        try{
            String sql = "SELECT * FROM Banner Where banid = " +banid;
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                Banner ban = Banner.builder()
                        .banid(rs.getInt("banid"))
                        .create_at(rs.getString("create_at"))
                        .img(rs.getString("img"))
                        .status(rs.getInt("status"))
                        .build();
                return ban;
            }
        }catch (Exception e){
            System.out.println("get banner " + e.getMessage());
        }
        return null;
    }
    
    public static void main(String[] args) {
        BannerDao d = new BannerDao();
        List<Banner> list = d.getListAllBanner("create_at");
        System.out.println(list);
    }

}
