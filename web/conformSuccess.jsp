<%-- 
    Document   : conformSuccess
    Created on : Nov 16, 2023, 11:16:01 AM
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <%@include file="Header.jsp" %>
<body>




<div id="main" class="container-content">
<div class="profile">
<div class="title_profile">
<i></i>
Đổi mật khẩu thành công
</div>
<div class="content_profile">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="sub-content_success">
<p>Mật khẩu mới của bạn đã được chúng tôi gửi vào mail. Bạn vui lòng kiểm tra mail để lấy lại mật khẩu mới.</p>
<p>Nếu có thắc mắc bạn hãy liên hệ với chúng tôi theo số hotline: <span style="color:red;">
Hotline 0817587256 </span>
</p>
<p>Xin chân thành cảm ơn!</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="clear"></div>



    </body>
</html>
