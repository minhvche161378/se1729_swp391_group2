<%-- 
    Document   : ManagerProduct
    Created on : Dec 28, 2020, 5:19:02 PM
    Author     : trinh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap CRUD Data Table for Database with Modal Form</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="css/manager.css" rel="stylesheet" type="text/css"/>
        <style>
            img{
                width: 200px;
                height: 120px;
            }
        </style>
    <body>
        <div class="container">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>Edit <b>Product</b></h2>
                        </div>
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div>
            </div>
            <div id="editEmployeeModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="edit" method="post">
                            <div class="modal-header">						
                                <h4 class="modal-title">Edit Product</h4>
                            </div>
                            <div class="modal-body">
                                <span style="color: red">${error}</span>

                                <div class="form-group">
                                    <label>ID</label>
                                    <input value="${!empty param.id ? param.id : listp.pid}" name="id" type="text" class="form-control" readonly required>
                                </div>
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="${!empty param.pname ? param.pname : listp.pname}" name="pname" type="text" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Price</label>
                                    <input value="${!empty param.price ? param.price : listp.price}" name="price" type="number" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Quantity</label>
                                    <input value="${!empty param.quantity ? param.quantity : listp.quantity}" name="quantity" type="number" class="form-control" required>
                                    <span style="color: red">${quantityerror}</span>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea name="description" class="form-control" required>${!empty param.description ? param.description : listpd.description}</textarea>
                                    <span style="color: red">${descriptionerror}</span>
                                </div>
                                <div class="form-group">
                                    <label>Rules</label>
                                    <textarea name="rules" class="form-control" required>${!empty param.rules ? param.rules : listpd.rules}</textarea>
                                    <span style="color: red">${ruleserror}</span>
                                </div>
                                <div class="form-group">
                                    <label>Required Age</label>
                                    <input value="${!empty param.requiredAge ? param.requiredAge : listpd.requiredAge}" name="requiredAge" type="number" class="form-control" required>
                                    <span style="color: red">${ageerror}</span>
                                </div>
                                <div class="form-group">
                                    <label>Number Player</label>
                                    <input value="${!empty param.numplayer ? param.numplayer : listpd.numPlayer}" name="numplayer" type="number" class="form-control" required>
                                    <span style="color: red">${numPayererror}</span>
                                </div>
                                <div class="form-group">
                                    <label>Time Play</label>
                                    <input value="${!empty param.timeplay ? param.timeplay : listpd.timeplay}" name="timeplay" type="number" class="form-control" required>
                                    <span style="color: red">${timeplayerror}</span>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="status" class="form-select" aria-label="Default select example">
                                        <option value="1" >Enable</option>
                                        <option value="0">Disable</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Danh mục</label>
                                    <select name="category" class="form-select" aria-label="Chọn danh mục">
                                        <c:forEach items="${calist}" var="ca">
                                            <c:choose>
                                                <c:when test="${!empty param.category && param.category eq ca.caid}">
                                                    <option value="${ca.caid}" selected>${ca.caname}</option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="${ca.caid}">${ca.caname}</option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </select>
                                </div>




                                <div class="form-group">
                                    <label>Image</label>
                                    <div><img src="images/${listp.img}"></div>
                                    <input type="file" id="avatar" name="image" accept="image/*"><br>
                                    <c:choose>
                                        <c:when test="${not empty param.image}">
                                            <input type="hidden" name="image" value="${param.image}" />
                                        </c:when>
                                        <c:otherwise>
                                            <!-- Nếu không có giá trị mới được tải lên, giữ nguyên giá trị hiện tại -->
                                        </c:otherwise>
                                    </c:choose>
                                    <input type="submit" class="btn btn-success" value="Edit">
                                </div>
                            </div>
                        </form>


                    </div>
                </div>
            </div>

        </div>


        <script src="js/manager.js" type="text/javascript"></script>
    </body>
</html>