<%-- 
    Document   : test
    Created on : 15-10-2023, 14:31:02
    Author     : hihih
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title>crud dashboard</title>
	    <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
	    <!----css3---->
        <link rel="stylesheet" href="css/custom.css">
		
		
		<!--google fonts -->
	    <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">
	
	
	   <!--google material icon-->
      <link href="https://fonts.googleapis.com/css2?family=Material+Icons"rel="stylesheet">

  </head>
  <body>
  


<div class="wrapper">
     
	  <div class="body-overlay"></div>
	 
	 <!-------sidebar--design------------>
	 
         <%@include file="Admin/LeftAdmin.jsp"%>
	 
   <!-------sidebar--design- close----------->
   
   
   
      <!-------page-content start----------->
   
      <div id="content">
	     
		  <!------top-navbar-start-----------> 


                  
		  <div class="top-navbar">
		     <div class="xd-topbar">
			     <div class="row">
				     <div class="col-2 col-md-1 col-lg-1 order-2 order-md-1 align-self-center">
					    <div class="xp-menubar">
						    <span class="material-icons text-white">signal_cellular_alt</span>
						</div>
					 </div>
					 
					 <div class="col-md-5 col-lg-3 order-3 order-md-2">
                                             
					     <div class="xp-searchbar">
						     <form action="FeedbackListController" method="post">
							    <div class="input-group">
								  <input type="search" class="form-control"
								  placeholder="Search" name="searchKeyword"  >
								  <div class="input-group-append">
                                                                      
								     <button class="btn" name="action" value="search" type="submit" id="button-addon2">Go
									 </button>
								  </div>
								</div>
							 </form>
						 </div>
					 </div>
					 
					 
					 <div class="col-10 col-md-6 col-lg-8 order-1 order-md-3">
					     <div class="xp-profilebar text-right">
						    <nav class="navbar p-0">
							   <ul class="nav navbar-nav flex-row ml-auto">
							   <li class="dropdown nav-item active">
							     <a class="nav-link" href="#" data-toggle="dropdown">
								  <span class="material-icons">notifications</span>
								  <span class="notification">4</span>
								 </a>
								  <ul class="dropdown-menu">
								     <li><a href="#">You Have 4 New Messages</a></li>
									 <li><a href="#">You Have 4 New Messages</a></li>
									 <li><a href="#">You Have 4 New Messages</a></li>
									 <li><a href="#">You Have 4 New Messages</a></li>
								  </ul>
							   </li>
							   
							   <li class="nav-item">
							     <a class="nav-link" href="#">
								   <span class="material-icons">question_answer</span>
								 </a>
							   </li>
							   
							   <li class="dropdown nav-item">
							     <a class="nav-link" href="#" data-toggle="dropdown">
								  <img src="img/user.jpg" style="width:40px; border-radius:50%;"/>
								  <span class="xp-user-live"></span>
								 </a>
								  <ul class="dropdown-menu small-menu">
								     <li><a href="#">
									 <span class="material-icons">person_outline</span>
									 Profile
									 </a></li>
									 <li><a href="#">
									 <span class="material-icons">settings</span>
									 Settings
									 </a></li>
									 <li><a href="#">
									 <span class="material-icons">logout</span>
									 Logout
									 </a></li>
									 
								  </ul>
							   </li>
							   
							   
							   </ul>
							</nav>
						 </div>
					 </div>
					 
				 </div>
				 
				 <div class="xp-breadcrumbbar text-center">
				    <h4 class="page-title">Feedback List</h4>
					<ol class="breadcrumb">
					  <li class="breadcrumb-item"><a href="#">Comment</a></li>
					  <li class="breadcrumb-item active" aria-curent="page">Feedback</li>
					</ol>
				 </div>
				 
				 
			 </div>
		  </div>
		  <!------top-navbar-end-----------> 
		  
		  
		   <!------main-content-start-----------> 
		     
		      <div class="main-content">
			     <div class="row">
				    <div class="col-md-12">
					   <div class="table-wrapper">
					     
					   <div class="table-title">
					     <div class="row">
						     <div class="col-sm-6 p-0 flex justify-content-lg-start justify-content-center">
							    <h2 class="ml-lg-2">Manage  Employees</h2>
							 </div>
							 <div class="col-sm-6 p-0 flex justify-content-lg-end justify-content-center">
							   <a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal">
							   <i class="material-icons">&#xE147;</i>
							   <span>Có ${comments.size()} Kết quả tìm được</span>
							   </a>
							   <a href="#deleteEmployeeModal" class="btn btn-danger" data-toggle="modal">
							   <i class="material-icons">&#xE15C;</i>
							   <span></span>
							   </a>
							 </div>
					     </div>
                                              
					   </div>
		
    
					   <table class="table table-striped table-hover">
					      <thead>
						     <tr>
							 <th><span class="custom-checkbox">
							 <input type="checkbox" id="selectAll">
							 <label for="selectAll"></label></th>
                                                         <th><a href="?sort=CommentId">Comment ID</a>
                                                             <a href="?reverse=CommentId" style="color: red"> ↗</a></th>
            <th><a href="?sort=CommentContent">Comment Content</a>
                <a href="?reverse=CommentContent" style="color: red">↗</a></th>
            <th><a href="?sort=Create_at" >Created At</a>
                <a href="?reverse=Create_at" style="color: red">↗</a></th>
            <th><a href="?sort=Status" >Status</a>
                <a href="?reverse=Status" style="color: red">↗</a></th>
            <th><a href="?sort=ProductId" >Product ID</a>
                <a href="?reverse=ProductId" style="color: red">↗</a></th>
            <th><a href="?sort=AccountId">Account ID</a>
                <a href="?reverse=AccountId" style="color: red">↗</a></th>
            <th><a href="?sort=Rate">Rate</a>
                <a href="?reverse=Rate"style="color: red">↗</a></th>
            <th><a href="?sort=CustomerName">Customer Name</a>
                <a href="?reverse=CustomerName"style="color: red">↗</a></th>
            <th><a href="?sort=CustomerEmail">Customer Email</a>
                <a href="?reverse=CustomerEmail" style="color: red">↗</a></th>
							 
							 </tr>
						  </thead>
						  
						  <tbody>
                                                      <c:forEach var="comment" items="${comments}">
            <tr>
                <th><span class="custom-checkbox">
							 <input type="checkbox" id="checkbox1" name="option[]" value="1">
							 <label for="checkbox1"></label></th>
                <td>
                <a href="FeedbackDetailServlet?commentId=${comment.commentId}">
                        ${comment.commentId}
                    </a>
                </td>
                <td>${comment.commentContent}</td>
                <td>${comment.create_at}</td>
                <td>${comment.status}</td>
                <td>${comment.productId}</td>
                <td>${comment.accountId}</td>
                <td>${comment.rate}</td>
                <td>${comment.customerName}</td>
                <td>${comment.customerEmail}</td>
                 <th>
			<form action="FeedbackListController" method="post">
    <!-- Thêm form chỉnh sửa -->
     <input type="hidden" name="action" value="edit">

                        <input type="hidden" name="commentId" value="${comment.commentId}">
                        <input type="text" name="newContent" value="${comment.commentContent}">
                        <select name="newStatus">
                            <option value="0" ${comment.status == 0 ? 'selected' : ''}>Không hiển thị</option>
                            <option value="1" ${comment.status == 1 ? 'selected' : ''}>Hiển thị</option>
                        </select>
                        <input type="submit" value="Lưu chỉnh sửa">
</form>

<form action="FeedbackListController" method="post">
    <!-- Thêm form phản hồi -->
                            <input type="hidden" name="commentId" value="${comment.commentId}">

    <input type="hidden" name="action" value="reply">
    <input type="text" name="feedbackBySellerName" placeholder="feedbackByName">
    <input type="text" name="feedbackContent" placeholder="feedbackContent">
    <input type="submit" value="Phản hồi">
</form>
							 </th>
            </tr>
        </c:forEach>
						      
							 
						  </tbody>
						  
					      
					   </table>
					   <ul class="pagination">
    <li class="page-item ${currentPage == 1 ? 'disabled' : ''}">
        <a class="page-link" href="?page=${previousPage}">Previous</a>
    </li>
    <c:forEach var="page" begin="1" end="${totalPages}">
        <li class="page-item ${page == currentPage ? 'active' : ''}">
            <a class="page-link" href="?page=${page}">${page}</a>
        </li>
    </c:forEach>
    <li class="page-item ${currentPage == totalPages ? 'disabled' : ''}">
        <a class="page-link" href="?page=${nextPage}">Next</a>
    </li>
</ul>
					 
					   
					   
					   
					   
	
					   
					   
					   
					   
					   </div>
					</div>
					
					
									   <!----add-modal start--------->
		<div class="modal fade" tabindex="-1" id="addEmployeeModal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Employees</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
		    <label>Name</label>
			<input type="text" class="form-control" required>
		</div>
		<div class="form-group">
		    <label>Email</label>
			<input type="emil" class="form-control" required>
		</div>
		<div class="form-group">
		    <label>Address</label>
			<textarea class="form-control" required></textarea>
		</div>
		<div class="form-group">
		    <label>Phone</label>
			<input type="text" class="form-control" required>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-success">Add</button>
      </div>
    </div>
  </div>
</div>

					   <!----edit-modal end--------->
					   
					   
					   
					   
					   
				   <!----edit-modal start--------->
		<div class="modal fade" tabindex="-1" id="editEmployeeModal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Employees</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
		    <label>Name</label>
			<input type="text" class="form-control" required>
		</div>
		<div class="form-group">
		    <label>Email</label>
			<input type="emil" class="form-control" required>
		</div>
		<div class="form-group">
		    <label>Address</label>
			<textarea class="form-control" required></textarea>
		</div>
		<div class="form-group">
		    <label>Phone</label>
			<input type="text" class="form-control" required>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-success">Save</button>
      </div>
    </div>
  </div>
</div>

					   <!----edit-modal end--------->	   
					   
					   
					 <!----delete-modal start--------->
		<div class="modal fade" tabindex="-1" id="deleteEmployeeModal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Delete Employees</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this Records</p>
		<p class="text-warning"><small>this action Cannot be Undone,</small></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-success">Delete</button>
      </div>
    </div>
  </div>
</div>

					   <!----edit-modal end--------->   
					   
					
					
				 
			     </div>
			  </div>
		  
		    <!------main-content-end-----------> 
		  
		 
		 
		 <!----footer-design------------->
		 
		 <footer class="footer">
		    <div class="container-fluid">
			   <div class="footer-in">
			      <p class="mb-0">&copy 2021 Vishweb Design . All Rights Reserved.</p>
			   </div>
			</div>
		 </footer>
		 
		 
		 
		 
	  </div>
   
</div>



<!-------complete html----------->





  
     <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src="js/jquery-3.3.1.slim.min.js"></script>
   <script src="js/popper.min.js"></script>
   <script src="js/bootstrap.min.js"></script>
   <script src="js/jquery-3.3.1.min.js"></script>
  
  
  <script type="text/javascript">
       $(document).ready(function(){
	      $(".xp-menubar").on('click',function(){
		    $("#sidebar").toggleClass('active');
			$("#content").toggleClass('active');
		  });
		  
		  $('.xp-menubar,.body-overlay').on('click',function(){
		     $("#sidebar,.body-overlay").toggleClass('show-nav');
		  });
		  
	   });
  </script>
  
  



  </body>
  
  </html>


