<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi" lang="vi"><head>
        <title>Board Game VN - Bring People Closer</title>
        <head>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
                    <%@include file="Header.jsp" %>
    
        </head>

        <body>
            <div id="main" class>

                <div class="image_home">
                    <div class="container">
                        <div id="image_home">
                            <div class="owl-carousel">
                            <c:forEach items="${banlist}" var="b">
                                            
                                                <div class="item">
                                                    <a href="" title="Trung Thu banner 1">
                                                        <img src="${b.img}" alt="Trung Thu banner 1">
                                                    </a>
                                                </div>
                                                
                                        </c:forEach>
                                            </div>
                        </div>
                    </div>
                </div>
                    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
                            <script>
                                $(document).ready(function () {
                                    $('.owl-carousel').owlCarousel({
                                        items: 1,
                                        loop: true,
                                        autoplay: true,
                                        autoplayTimeout: 3000, // Change this value to adjust the autoplay speed (in milliseconds)
                                    });
                                });
                            </script>

                <div class="clear"></div>
                <div class="about-info">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="ship_policy">
                                    <h4>Giao hàng toàn quốc</h4>
                                    <span>Hỗ trợ 63 tỉnh thành trên toàn quốc
                                    </span>
                                    <a target="_blank" rel="nofollow" href="https://boardgame.vn/ns79/chinh-sach-van-chuyen">Xem thêm ></a>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="payment_policy">
                                    <h4>Thanh toán dễ dàng</h4>
                                    <span>Chuyển khoản ngân hàng hoặc<br> thanh toán khi nhận hàng</span></div>
                            </div>
                            <div class="col-lg-4">
                                <div class="group_policy">
                                    <h4>Ưu đãi mua nhóm</h4>
                                    <span>Dành cho đối tác Nhà phân phối, cafe board game, trường học Liên hệ Mr Tùng (0934433995)
                                    </span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <!--   sản phẩm mới-->
                            <div class="box-product-home blue">
                                <!--Game hot - giá sốc - recomment - sản phẩm mới-->

                                <div class="title">


                                    <h3><a href="criteria?id=3" title="">Board Game mới</a></h3>
                                    <ul class="box_child">
                                    </ul>
                                    <div class="view_all">
                                        <a href="criteria?id=3" title="Xem tất cả">Xem tất cả »</a>
                                    </div>
                                </div>

                                <ul id="slide_product_home_100" class="slide_product_home">
                                    <c:forEach items="${pnewList}" var="p" begin="0" end="3">



                                        <li class="col-lg-3">
                                            <c:choose>
                                                <c:when test="${p.getQuantity() == 0}">
                                                    <div class="product out_of_stock">
                                                    </c:when>
                                                    <c:otherwise>
                                                        <div class="product">

                                                        </c:otherwise>
                                                    </c:choose>

                                                    <div class="thumb-img">
                                                        <a href="productdetail?Pid=${p.pid}" title="${p.pname}">
                                                            <img src="images/${p.img}" alt="" width="295" height="295">
                                                        </a>

                                                        <div class="action" onclick="actionclick('')">
                                                            <div class="content">
                                                                <a class="btn_view" href="productdetail?Pid=${p.pid}" title="Xem chi tiết">
                                                                    Xem chi tiết
                                                                </a>
                                                                <a class="btn_addcart" onclick="" href="addcart?pid=${p.pid}&url=home" title="Cho vào giỏ">
                                                                    Cho vào giỏ
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product-info">
                                                        <div class="info">
                                                            <a class="name-p" href="productdetail?Pid=${p.pid}"
                                                               title="${p.pname}">
                                                                ${p.pname}
                                                                 <c:if test="${p.isIsDiscount()}"><div class="discount-label">
                                                                    <c:set var="price" value="${p.price}" />
                                                                    <c:set var="priceSale" value="${p.priceSale}" />

                                                                    <%-- Kiểm tra nếu có giảm giá và giá gốc lớn hơn 0 --%>
                                                                   
                                                                        <%-- Tính phần trăm giảm giá --%>
                                                                        <c:set var="discountPercentage" value="${((price - priceSale) / price) * 100}" />

                                                                        <%-- Hiển thị phần trăm giảm giá --%>
                                                                        <p>${discountPercentage}%</p>
                                                                  

                                                                </div>  </c:if>
                                                            </a>
                                                        </div>
                                                        <div class="summary">

                                                        </div>
                                                        <div class="price"> 

                                                          <c:choose>
        <c:when test="${p.isIsDiscount() && p.priceSale != 0}">
            <fmt:formatNumber value="${p.priceSale}" pattern="#,##0" var="formattedPriceSale" />
            <fmt:formatNumber value="${p.price}" pattern="#,##0" var="formattedPrice" />
            <font>${formattedPriceSale} VNĐ</font>
            <span class="old_price">${formattedPrice} VNĐ</span>
            <c:if test="${p.getQuantity() == 0}">
                <span class="outofstock">Cháy hàng</span>
            </c:if>
        </c:when>
        <c:otherwise>
            <fmt:formatNumber value="${p.price}" pattern="#,##0" var="formattedPrice" />
            <font>${formattedPrice} VNĐ</font>
        </c:otherwise>
    </c:choose>

                                                        </div>
                                                    </div>
                                                </div>
                                        </li>
                                    </c:forEach>
                                </ul>

                            </div>
                            <!--   sản phẩm mới-->

                            <!--    Giá sốc-->
                            <div class="box-product-home blue">
                               
                                <!--    Product list-->
                                <div class="title">


                                    <h3><a href="criteria?id=1" title="">Board Game giá sốc</a></h3>
                                    <ul class="box_child">
                                    </ul>
                                    <div class="view_all">
                                        <a href="criteria?id=1" title="Xem tất cả">Xem tất cả »</a>
                                    </div>
                                </div>

                                <ul id="slide_product_home_100" class="slide_product_home">
                                    <c:forEach items="${shockpriceList}" var="p" begin="0" end="3">



                                        <li class="col-lg-3">
                                            <c:choose>
                                                <c:when test="${p.getQuantity() == 0}">
                                                    <div class="product out_of_stock">
                                                    </c:when>
                                                    <c:otherwise>
                                                        <div class="product">

                                                        </c:otherwise>
                                                    </c:choose>
                                                    <div class="thumb-img">
                                                        <a href="productdetail?Pid=${p.pid}" title="${p.pname}">
                                                            <img src="images/${p.img}" alt="" width="295" height="295">
                                                        </a>

                                                        <div class="action" onclick="actionclick('')">
                                                            <div class="content">
                                                                <a class="btn_view" href="productdetail?Pid=${p.pid}" title="Xem chi tiết">
                                                                    Xem chi tiết
                                                                </a>
                                                                <a class="btn_addcart" onclick="" href="addcart?pid=${p.pid}&url=home" title="Cho vào giỏ">
                                                                    Cho vào giỏ
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product-info">
                                                        <div class="info">
                                                            <a class="name-p" href="productdetail?Pid=${p.pid}"
                                                               title="${p.pname}">
                                                                ${p.pname}
                                                                  <c:if test="${p.isIsDiscount()}">
                                                                <div class="discount-label">
                                                                    <c:set var="price" value="${p.price}" />
                                                                    <c:set var="priceSale" value="${p.priceSale}" />

                                                                    <%-- Kiểm tra nếu có giảm giá và giá gốc lớn hơn 0 --%>
                                                                  
                                                                        <%-- Tính phần trăm giảm giá --%>
                                                                        <c:set var="discountPercentage" value="${((price - priceSale) / price) * 100}" />

                                                                        <%-- Hiển thị phần trăm giảm giá --%>
                                                                        <p>${discountPercentage}%</p>
                                                                  

                                                                </div>  </c:if>
                                                                    <div>
                                                                       
                                                                

                  
                                                            </a>
                                                        </div>
                                                        <div class="summary">

                                                        </div>
                                                        <div class="price"> 

                                                           <c:choose>
        <c:when test="${p.isIsDiscount() && p.priceSale != 0}">
            <fmt:formatNumber value="${p.priceSale}" pattern="#,##0" var="formattedPriceSale" />
            <fmt:formatNumber value="${p.price}" pattern="#,##0" var="formattedPrice" />
            <font>${formattedPriceSale} VNĐ</font>
            <span class="old_price">${formattedPrice} VNĐ</span>
            <c:if test="${p.getQuantity() == 0}">
                <span class="outofstock">Cháy hàng</span>
            </c:if>
        </c:when>
        <c:otherwise>
            <fmt:formatNumber value="${p.price}" pattern="#,##0" var="formattedPrice" />
            <font>${formattedPrice} VNĐ</font>
        </c:otherwise>
    </c:choose>

                                                        </div>
                                                    </div>
                                                </div>
                                        </li>
                                    </c:forEach>
                                </ul>

                            </div>
                            <!--                           Giá sốc-->
                            
                            
                             <!--    recomment-->
                            <div class="box-product-home blue">
                                <!--Game hot - giá sốc - recomment - sản phẩm mới-->
                                <!--    Product list-->
                                <div class="title">


                                    <h3><a href="criteria?id=4" title="">Gợi ý Board Game</a></h3>
                                    <ul class="box_child">
                                    </ul>
                                    <div class="view_all">
                                        <a href="criteria?id=4" title="Xem tất cả">Xem tất cả »</a>
                                    </div>
                                </div>

                                <ul id="slide_product_home_100" class="slide_product_home">
                                    <c:forEach items="${pRecommentList}" var="p" begin="0" end="3">



                                        <li class="col-lg-3">
                                            <c:choose>
                                                <c:when test="${p.getQuantity() == 0}">
                                                    <div class="product out_of_stock">
                                                    </c:when>
                                                    <c:otherwise>
                                                        <div class="product">

                                                        </c:otherwise>
                                                    </c:choose>
                                                    <div class="thumb-img">
                                                        <a href="productdetail?Pid=${p.pid}" title="${p.pname}">
                                                            <img src="images/${p.img}" alt="" width="295" height="295">
                                                        </a>

                                                        <div class="action" onclick="actionclick('')">
                                                            <div class="content">
                                                                <a class="btn_view" href="productdetail?Pid=${p.pid}" title="Xem chi tiết">
                                                                    Xem chi tiết
                                                                </a>
                                                                <a class="btn_addcart" onclick="" href="addcart?pid=${p.pid}&url=home" title="Cho vào giỏ">
                                                                    Cho vào giỏ
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product-info">
                                                        <div class="info">
                                                            <a class="name-p" href="productdetail?Pid=${p.pid}"
                                                               title="${p.pname}">
                                                                ${p.pname}
                                                                   <c:if test="${p.isIsDiscount()}">
                                                                <div class="discount-label">
                                                                    <c:set var="price" value="${p.price}" />
                                                                    <c:set var="priceSale" value="${p.priceSale}" />

                                                                    <%-- Kiểm tra nếu có giảm giá và giá gốc lớn hơn 0 --%>
                                                                 
                                                                        <%-- Tính phần trăm giảm giá --%>
                                                                        <c:set var="discountPercentage" value="${((price - priceSale) / price) * 100}" />

                                                                        <%-- Hiển thị phần trăm giảm giá --%>
                                                                        <p>${discountPercentage}%</p>
                                                                   

                                                                </div> </c:if>
                                                                    <div>
                                                                       
                                                                

                  
                                                            </a>
                                                        </div>
                                                        <div class="summary">

                                                        </div>
                                                        <div class="price"> 

                                                           <c:choose>
        <c:when test="${p.isIsDiscount() && p.priceSale != 0}">
            <fmt:formatNumber value="${p.priceSale}" pattern="#,##0" var="formattedPriceSale" />
            <fmt:formatNumber value="${p.price}" pattern="#,##0" var="formattedPrice" />
            <font>${formattedPriceSale} VNĐ</font>
            <span class="old_price">${formattedPrice} VNĐ</span>
            <c:if test="${p.getQuantity() == 0}">
                <span class="outofstock">Cháy hàng</span>
            </c:if>
        </c:when>
        <c:otherwise>
            <fmt:formatNumber value="${p.price}" pattern="#,##0" var="formattedPrice" />
            <font>${formattedPrice} VNĐ</font>
        </c:otherwise>
    </c:choose>

                                                        </div>
                                                    </div>
                                                </div>
                                        </li>
                                    </c:forEach>
                                </ul>

                            </div>
                            <!--                       Recomment-->
  <!--    Game hot-->
                            <div class="box-product-home blue">
                              
                                <!--    Product list-->
                                <div class="title">


                                    <h3><a href="criteria?id=2" title="Lớp Học Mật Ngữ">Game hot</a></h3>
                                    <ul class="box_child">
                                    </ul>
                                    <div class="view_all">
                                        <a href="criteria?id=2" title="Xem tất cả">Xem tất cả »</a>
                                    </div>
                                </div>

                                <ul id="slide_product_home_100" class="slide_product_home">
                                    <c:forEach items="${photList}" var="p" begin="0" end="3">



                                        <li class="col-lg-3">
                                            <c:choose>
                                                <c:when test="${p.getQuantity() == 0}">
                                                    <div class="product out_of_stock">
                                                    </c:when>
                                                    <c:otherwise>
                                                        <div class="product">

                                                        </c:otherwise>
                                                    </c:choose>
                                                    <div class="thumb-img">
                                                        <a href="productdetail?Pid=${p.pid}" title="${p.pname}">
                                                            <img src="images/${p.img}" alt="" width="295" height="295">
                                                        </a>

                                                        <div class="action" onclick="actionclick('')">
                                                            <div class="content">
                                                                <a class="btn_view" href="productdetail?Pid=${p.pid}" title="Xem chi tiết">
                                                                    Xem chi tiết
                                                                </a>
                                                                <a class="btn_addcart" onclick="" href="addcart?pid=${p.pid}&url=home" title="Cho vào giỏ">
                                                                    Cho vào giỏ
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product-info">
                                                        <div class="info">
                                                            <a class="name-p" href="productdetail?Pid=${p.pid}"
                                                               title="${p.pname}">
                                                                ${p.pname}
                                                                  <c:if test="${p.isIsDiscount()}">
                                                                <div class="discount-label">
                                                                    <c:set var="price" value="${p.price}" />
                                                                    <c:set var="priceSale" value="${p.priceSale}" />

                                                                    <%-- Kiểm tra nếu có giảm giá và giá gốc lớn hơn 0 --%>
                                                                  
                                                                        <%-- Tính phần trăm giảm giá --%>
                                                                        <c:set var="discountPercentage" value="${((price - priceSale) / price) * 100}" />

                                                                        <%-- Hiển thị phần trăm giảm giá --%>
                                                                        <p>${discountPercentage}%</p>
                                                                   

                                                                </div> </c:if>
                                                                    <div>
                                                                       
                                                                

                  
                                                            </a>
                                                        </div>
                                                        <div class="summary">

                                                        </div>
                                                        <div class="price"> 

                                                           <c:choose>
        <c:when test="${p.isIsDiscount() && p.priceSale != 0}">
            <fmt:formatNumber value="${p.priceSale}" pattern="#,##0" var="formattedPriceSale" />
            <fmt:formatNumber value="${p.price}" pattern="#,##0" var="formattedPrice" />
            <font>${formattedPriceSale} VNĐ</font>
            <span class="old_price">${formattedPrice} VNĐ</span>
            <c:if test="${p.getQuantity() == 0}">
                <span class="outofstock">Cháy hàng</span>
            </c:if>
        </c:when>
        <c:otherwise>
            <fmt:formatNumber value="${p.price}" pattern="#,##0" var="formattedPrice" />
            <font>${formattedPrice} VNĐ</font>
        </c:otherwise>
    </c:choose>

                                                        </div>
                                                    </div>
                                                </div>
                                        </li>
                                    </c:forEach>
                                </ul>

                            </div>
                            <!--                          Game hot-->
                            

                            
                            
                            <!--tin tức-->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="box_newshome">
                                        <h3 class="box-title">Tin tức - sự kiện
                                            <a class="view_all" href="/cn1/tin-tuc" title>Xem tất cả »</a>
                                        </h3>
                                        <div class="content">
                                            <c:forEach items="${newsList}" var="news" begin="0" end="5" varStatus="loopStatus">
                                                  <div class="list_item${loopStatus.index}">
                                                <div class="item_image">
                                                    <a href="newsdetails?nid=${news.nid}" >
                                                        <img src="${news.img}" width="610" height="360">
                                                    </a>
                                                </div>
                                                <div class="bg_info"></div>
                                                <div class="info_news">
                                                    <div class="item_name">
                                                        <a href="newsdetails?nid=${news.nid}" style="color: white" >${news.title}<span class="bg_catenews" style="background-color:#fb5332">Tin tức</span> </a>
                                                    </div>
                                                    <div class="item_summary">
                                                    </div>
                                                </div>
                                            </div>
                                            </c:forEach> 
                                       
                                           
                                             
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clear"></div>


                            <%@include file="Footer.jsp"  %>

                            </body></html>