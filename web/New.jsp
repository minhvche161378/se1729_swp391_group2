<%-- 
    Document   : New
    Created on : 14-09-2023, 18:10:45
    Author     : hihih
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi" lang="vi"><head></head>
   <body>
        <%@include file="Header.jsp"  %>
        <div id="main" class="container-content">
            <div class="box_slide_news">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="title">
                                Tin nổi bật
                            </div>
                            <c:forEach items="${highlightNewsList}" var="listAllNews" begin="0" end="3">

                                <ul id="slide_news" class="slide_news">
                                    <li>
                                        <div class="list_item">
                                            <div class="item_image">
                                                <a href="newsdetails?nid=${listAllNews.nid}" title="${listAllNews.title}">
                                                    <img src="${listAllNews.img}" width="570" height="325">
                                                </a>
                                            </div>
                                            <div class="bg_info"></div>
                                            <div class="info_news">
                                                <div class="item_name">
                                                    <a href="newsdetails?nid=${listAllNews.nid}" title="">${listAllNews.title} <span class="bg_catenews" style="background-color:#000">Tin tức</span> </a>
                                                </div>
                                                <div class="item_summary">
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </c:forEach>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $("#slide_news").flexisel({
                                        visibleItems: 2.1,
                                        animationSpeed: 600,
                                        autoPlay: true,
                                        autoPlaySpeed: 5000,
                                        pauseOnHover: true,
                                        enableResponsiveBreakpoints: true
                                    });
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
            <div class="news_content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="title_page">
                                <h1 class="title_cate">Tin mới</h1>
                                <ul class="box_child" itemscope="" itemtype="http://www.schema.org/SiteNavigationElement">
                                    <c:forEach items="${canewList}" var="canew">
                                        <li class="" itemprop="name">
                                            <a href="categoriesnews?canewId=${canew.canewId}" itemprop="url" title="">${canew.name}</a>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </div>
                            <div class="content">
    <c:choose>
        <c:when test="${not empty newsListbyCa}">
            <!-- Hiển thị tin tức theo danh mục -->
            <c:forEach items="${newsListbyCa}" var="news">
                <div id="list-contentNews" class="list-view">
                    <div class="summary"></div>
                    <div class="list-news">
                        <div class="list_item">
                            <div class="item_image">
                                <a href="newsdetails?nid=${news.nid}" title="">
                                    <img src="${news.img}">
                                </a>
                            </div>
                            <div class="info_news">
                                <div class="time">
                                    ${news.createat} <span class="bg_catenews" style="background-color:#000">Tin tức</span>
                                </div>
                                <div class="item_name">
                                    <li>
                                        <a href="newsdetails?nid=${news.nid}">
                                            <c:choose>
                                                <c:when test="${fn:length(news.title) > 40}">
                                                    ${fn:substring(news.title, 0, 40)}....
                                                </c:when>
                                                <c:otherwise>
                                                    ${news.title}
                                                </c:otherwise>
                                            </c:choose>
                                        </a>
                                    </li>
                                </div>
                                <div class="item_summary">
                                    <!-- Thêm nội dung tóm tắt tin tức nếu cần -->
                                </div>
                                <div class="item_more">
                                    <a href="newsdetails?nid=${news.nid}">Đọc tiếp &gt;</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <!-- Hiển thị tất cả tin tức -->
            <c:forEach items="${newsList}" var="news">
                <div id="list-contentNews" class="list-view">
                    <div class="summary"></div>
                    <div class="list-news">
                        <div class="list_item">
                            <div class="item_image">
                                <a href="newsdetails?nid=${news.nid}" title="">
                                    <img src="${news.img}">
                                </a>
                            </div>
                            <div class="info_news">
                                <div class="time">
                                    ${news.createat} <span class="bg_catenews" style="background-color:#000">Tin tức</span>
                                </div>
                                <div class="item_name">
                                    <li>
                                        <a href="newsdetails?nid=${news.nid}">
                                            <c:choose>
                                                <c:when test="${fn:length(news.title) > 40}">
                                                    ${fn:substring(news.title, 0, 40)}....
                                                </c:when>
                                                <c:otherwise>
                                                    ${news.title}
                                                </c:otherwise>
                                            </c:choose>
                                        </a>
                                    </li>
                                </div>
                                <div class="item_summary">
                                    <!-- Thêm nội dung tóm tắt tin tức nếu cần -->
                                </div>
                                <div class="item_more">
                                    <a href="newsdetails?nid=${news.nid}">Đọc tiếp &gt;</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </c:otherwise>
    </c:choose>
</div>


                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="clear"></div>
        <%@include file="Footer.jsp"  %>

    </body></html>