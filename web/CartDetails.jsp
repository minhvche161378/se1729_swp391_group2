


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi" lang="vi"><head>
        <title>Board Game VN - Bring People Closer</title>
        <head>

            <%@include file="Header.jsp"  %>
            <style>
                input[type=number]::-webkit-inner-spin-button,
                input[type=number]::-webkit-outer-spin-button {
                    -webkit-appearance: none;
                    margin: 0;
                }

                input[type=number] {
                    -moz-appearance: textfield;
                }
                .quantity {
                    font-family: arial;
                    font-size: 13px;
                    height: 25px;
                    text-align: center;
                    width: 20%;
                }
                .custom-checkbox {
                    position: relative;
                }
                .custom-checkbox input[type="checkbox"] {
                    opacity: 0;
                    position: absolute;
                    margin: 5px 0 0 3px;
                    z-index: 9;
                }
                .custom-checkbox label:before{
                    width: 18px;
                    height: 18px;
                }
                .custom-checkbox label:before {
                    content: '';
                    margin-right: 10px;
                    display: inline-block;
                    vertical-align: text-top;
                    background: white;
                    border: 1px solid #bbb;
                    border-radius: 2px;
                    box-sizing: border-box;
                    z-index: 2;
                }
                .custom-checkbox input[type="checkbox"]:checked + label:after {
                    content: '';
                    position: absolute;
                    left: 6px;
                    top: 3px;
                    width: 6px;
                    height: 11px;
                    border: solid #000;
                    border-width: 0 3px 3px 0;
                    transform: inherit;
                    z-index: 3;
                    transform: rotateZ(45deg);
                }
                .custom-checkbox input[type="checkbox"]:checked + label:before {
                    border-color: #03A9F4;
                    background: #03A9F4;
                }
                .custom-checkbox input[type="checkbox"]:checked + label:after {
                    border-color: #fff;
                }
                .custom-checkbox input[type="checkbox"]:disabled + label:before {
                    color: #b8b8b8;
                    cursor: auto;
                    box-shadow: none;
                    background: #ddd;
                }

            </style>



        </head>

        <body>
            <div id="main" class="container-content">
                <div id="cart-wrapper" class="checkout-div-wrapper">
                    <div class="container">
                        <div class="cart list_order_item">
                            <div id="cart-content">
                                <div class="page-title title-buttons">
                                    <h1>Giỏ hàng</h1>
                                </div>
                                <div id="cart-form" >
                                    <fieldset>
                                        <c:choose>
                                            <c:when test="${pList.size() ==0}">
                                                <div class="tfoot-center" style="text-align: center">
                                                    <h4>Không có sản phẩm nào trong cửa hàng của bạn</h4>
                                                    <a href="home" title="Chọn thêm sản phẩm khác" class="btn-continue" style="color: #87CEEB">Chọn thêm sản phẩm vào giỏ hàng</a>
                                                </div>
                                            </c:when>
                                            <c:otherwise>


                                                <table id="shopping-cart-table" class="data-table cart-table" >
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 3%;">

                                                                <span class="custom-checkbox">
                                                                    <input type="checkbox" id="selectAll" onclick="selectAllCheckboxes()">
                                                                        <label for="selectAll"></label>
                                                                </span>

                                                            </th>
                                                            <th style="width: 35%; text-indent: 20px;"><span class="nobr">Chọn tất cả (${pList.size()} sản phẩm)</span></th>
                                                            <th class="a-center" style="width: 25%;"><span class="nobr">Đơn giá</span></th>
                                                            <th class="a-center" style="width: 15%;">Số lượng</th>
                                                            <th class="a-center" style="width: 25%;">Thành tiền</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <!--                                                  List Product cart                             -->
                                                        <c:set var="totalPrice" value="0" />
                                                        <c:set var="count" value="1" />
                                                        <c:forEach items="${pList}" var="p" varStatus="loop">
                                                            <tr>
                                                                <td>

                                                                    <span class="custom-checkbox">
                                                                        <input type="checkbox" id="checkbox${count}" name="options[]" value="${p.key.pid}">
                                                                            <label for="checkbox${count}"></label>

                                                                    </span>


                                                                </td>
                                                                <td>

                                                                    <div class="product-image">
                                                                        <a href="productdetail?Pid=${p.key.pid}" class="product-image-link">
                                                                            <img src="images/${p.key.img}" width="65" height="65"></a>
                                                                    </div>


                                                                    <div class="product-info">
                                                                        <a href="productdetail?Pid=${p.key.pid}">${p.key.pname}</a>
                                                                    </div>
                                                                </td>
                                                                <td class="a-center">
                                                                    <c:choose>
                                                                        <c:when test="${p.key.isIsDiscount() && p.key.priceSale != 0}">
                                                                            <fmt:formatNumber value="${p.key.priceSale}" pattern="#,##0" var="Price" />
                                                                            <fmt:formatNumber value="${p.key.priceSale * p.value}" pattern="#,##0" var="money" />
                                                                            <c:set var="totalPrice" value="${totalPrice + (p.key.priceSale * p.value)}" />
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <fmt:formatNumber value="${p.key.price}" pattern="#,##0" var="Price" />
                                                                            <fmt:formatNumber value="${p.key.price * p.value}" pattern="#,##0" var="money" />
                                                                            <c:set var="totalPrice" value="${totalPrice + (p.key.price * p.value)}" />


                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                    <span class="price">${Price} vnđ</span>
                                                                </td>
                                                                <td class="a-center">
                                                                    <p class="contain-qty contain-qty-hidden ">
                                                                        <!--  Đổi số lượng-->
                                                                        <form action="cartDetails" method="post">



                                                                            <!--                                                                                Dấu trừ-->
                                                                            <a class="btn-subtract-qty" onclick="subtractQuantity(${loop.index})">
                                                                                <img style="width: 12px; height: auto;vertical-align: middle;" src="https://cdn0.fahasa.com/skin//frontend/ma_vanese/fahasa/images/ico_minus2x.png"></a>


                                                                            <input type="number" name="quantity" value="${p.value}" lang="2" size="2" title="Số lượng" 
                                                                                   class="input-text qty update-qty-index quantity" id="quantityInput_${loop.index}">
                                                                                <!--                                                                                Dấu cộng-->
                                                                                <a class="btn-add-qty"  onclick="addQuantity(${loop.index})"">
                                                                                    <img style="width: 12px; height: auto;vertical-align: middle;" src="https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/ico_plus2x.png"></a></div>

                                                                                <input type="hidden" name="pid" value="${p.key.pid}">
                                                                                    </form>
                                                                                    <!--  Đổi số lượng-->


                                                                                    </p>
                                                                                    </td>
                                                                                    <td class="a-center">

                                                                                        <span class="price" id="total-item-551050">${money} </span><span class="currentcy">vnđ</span>


                                                                                        <!--  Xóa product -->
                                                                                        <a class="action-delete-item" href="javascript:void(0);" onclick="submitDeleteForm(${p.key.pid})" title="Loại bỏ mặt hàng">
                                                                                            <span aria-hidden="true" class="glyphicon glyphicon-trash"></span>
                                                                                        </a>


                                                                                        <form id="delete-form" action="cartDetails" method="post" style="display: none;">
                                                                                            <input type="hidden" name="pid" id="delete-pid" value="">
                                                                                                <input type="hidden" name="type" value="delete">
                                                                                                    </form>

                                                                                                    <!-- Xóa product -->
                                                                                                    </td>
                                                                                                    </tr>

                                                                                                </c:forEach>




                                                                                                </tbody>
                                                                                                <tfoot>
                                                                                                    <tr>
                                                                                                        <td colspan="4" class="">
                                                                                                            <div class="tfoot-center" style="width: 820px;margin-left: 0;">
                                                                                                                <a href="home" title="Chọn thêm sản phẩm khác" class="btn-continue">Chọn thêm sản phẩm khác</a>
                                                                                                            </div>
                                                                                                            <div id="cart_footer" class="tfoot-right" style="margin-right: 0;">
                                                                                                                <div class="totals">
                                                                                                                    <table id="shopping-cart-totals-table">
                                                                                                                        <tfoot>
                                                                                                                            <tr>
                                                                                                                                <td style="" class="a-right" colspan="1">
                                                                                                                                    Tiền hàng:</td>
                                                                                                                                <td id="cart-subtotal" class="a-right">
                                                                                                                                    <span class="price"><fmt:formatNumber value="${totalPrice}" pattern="#,##0" /></span><span class="currentcy">vnđ</span> </td>
                                                                                                                            </tr>

                                                                                                                            <tr>
                                                                                                                                <td class="a-right" colspan="">
                                                                                                                                    <strong>Tổng cộng:</strong>
                                                                                                                                </td>
                                                                                                                                <td id="total_amount" style="" class="a-right">
                                                                                                                                    <strong><span class="price"><fmt:formatNumber value="${totalPrice}" pattern="#,##0" /></span><span class="currentcy">vnđ</span></strong>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tfoot>
                                                                                                                    </table>
                                                                                                                </div>
                                                                                                                <div id="checkoutButton">
                                                                                                                    <a rel="nofollow"  href="#" onclick="sendData()"  title="Tiến hành đặt hàng" class="btn-proceed-checkout">
                                                                                                                        Tiến hành đặt hàng
                                                                                                                    </a>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>

                                                                                                </tfoot>
                                                                                                </table>
                                                                                            </c:otherwise>
                                                                                        </c:choose>
                                                                                        </fieldset>

                                                                                        </div>
                                                                                        </div>
                                                                                        </div>



                                                                                        <div class="product-detail">
                                                                                            <div class="box-product-new">
                                                                                                <h3 class="box-title">Sản phẩm bán chạy</h3>
                                                                                                <ul id="slide_product_related">

                                                                                                    <li class=""> 


                                                                                                        <!--    list Product-->
                                                                                                        <c:forEach items="${photsale}" var="p" begin="0" end="3">

                                                                                                            <div class="product ">
                                                                                                                <div class="thumb-img">
                                                                                                                    <a href="productdetail?Pid=${p.pid}" >
                                                                                                                        <img src="images/${p.img}" width="260" height="260">
                                                                                                                    </a>
                                                                                                                    <c:if test="${p.isIsDiscount()}">
                                                                                                                        <div class="icon-sale"></div></c:if>
                                                                                                                    </div>
                                                                                                                    <div class="product-info">
                                                                                                                        <div class="info">
                                                                                                                            <a class="name-p" href="productdetail?Pid=${p.pid}" title="${p.pname}">${p.pname}</a>
                                                                                                                    </div>

                                                                                                                    <div class="price">
                                                                                                                        <c:choose>
                                                                                                                            <c:when test="${p.isIsDiscount() && p.priceSale != 0}">
                                                                                                                                <fmt:formatNumber value="${p.priceSale}" pattern="#,##0" var="formattedPriceSale" />
                                                                                                                                <fmt:formatNumber value="${p.price}" pattern="#,##0" var="formattedPrice" />
                                                                                                                                <font>${formattedPriceSale} VNĐ</font>
                                                                                                                                <span class="old_price">${formattedPrice} VNĐ</span>
                                                                                                                                <c:if test="${p.getQuantity() == 0}">
                                                                                                                                    <span class="outofstock">Cháy hàng</span>
                                                                                                                                </c:if>
                                                                                                                            </c:when>
                                                                                                                            <c:otherwise>
                                                                                                                                <fmt:formatNumber value="${p.price}" pattern="#,##0" var="formattedPrice" />
                                                                                                                                <font>${formattedPrice} VNĐ</font>
                                                                                                                                </c:otherwise>
                                                                                                                            </c:choose>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="border"></div>
                                                                                                            </div>
                                                                                                        </c:forEach>

                                                                                                        <!--    list Product-->
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                        </div>

                                                                                        </div>
                                                                                        </div>
                                                                                        <div class="clear"></div>
                                                                                        <%@include file="Footer.jsp"  %>
                                                                                        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

                                                                                        <script src="js/cart.js"></script>                                                    
                                                                                        <script type="text/javascript">
                                                                                                                                                                                                        function sendData() {
                                                                                                                                                                                                        var checkboxes = document.querySelectorAll('input[name="options[]"]:checked');
                                                                                                                                                                                                        var values = Array.from(checkboxes).map(function (checkbox) {
                                                                                                                                                                                                        return checkbox.value;
                                                                                                                                                                                                        });




                                                                                                                                                                                                        var form = document.createElement('form');
                                                                                                                                                                                                        form.method = 'GET';
                                                                                                                                                                                                        form.action = 'checkout';

                                                                                                                                                                                                        values.forEach(function (value) {
                                                                                                                                                                                                        var input = document.createElement('input');
                                                                                                                                                                                                        input.type = 'hidden';
                                                                                                                                                                                                        input.name = 'selectedItems';
                                                                                                                                                                                                        input.value = value;
                                                                                                                                                                                                        form.appendChild(input);
                                                                                                                                                                                                        });



                                                                                                                                                                                                        document.body.appendChild(form);
                                                                                                                                                                                                        form.submit();


                                                                                                                                                                                                        }

                                                                                        </script>                                                                                      

                                                                                        </body></html>