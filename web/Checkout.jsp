



<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi" lang="vi"><head>
        <title>Board Game VN - Bring People Closer</title>
        <head>
            <link rel="stylesheet" href="css/checkout.css" >
                <%@include file="Header.jsp"  %>
                <style>
                    /* Sử dụng flexbox để căn chỉnh phần tử */
                    .input-container {
                        display: flex;
                        align-items: center;
                        margin-bottom: 10px; /* Để tạo khoảng cách giữa các dòng */
                    }

                    /* Định dạng label */
                    .input-container label {
                        flex: 1; /* Phần trăm của không gian cho label */
                        margin-right: 10px; /* Để tạo khoảng cách giữa label và input */
                    }


                    .input-container input, .input-container textarea {
                        flex: 2; /* Phần trăm của không gian cho input và textarea */
                        margin-left: 0; /* Đặt lề trái thành 0 */
                    }
                </style>




                <script>
                    function toggleCode() {
                        var codeSelect = document.getElementById("codeSelect");
                        var selectedOption = codeSelect.options[codeSelect.selectedIndex].value;

                        var codeSnippet = document.getElementById("codeSnippet");
                        var noneSubmit = document.getElementById("noneSubmit");

                        var addNewSnippet = document.getElementById("addNewSnippet");

                        if (selectedOption === "changeInfo") {
                            codeSnippet.style.display = "block";
                            noneSubmit.style.display = "block";

                            addNewSnippet.style.display = "none";
                        } else if (selectedOption === "cancel") {
                            codeSnippet.style.display = "none";
                            noneSubmit.style.display = "none";

                            addNewSnippet.style.display = "none";

                        } else if (selectedOption === "addNew") {
                            codeSnippet.style.display = "none";
                            noneSubmit.style.display = "block";

                            addNewSnippet.style.display = "block";
                        }
                    }
                </script>
                <script>
                    function submitFormA() {
                        document.getElementById('formA').submit();
                    }
                </script>
        </head>

        <body>
            <div id="main" class="container-content">
                <div id="cart-wrapper" class="checkout-div-wrapper">
                    <div class="container">
                        <div class="cart list_order_item">
                            <div id="cart-content">
                                <div class="page-title title-buttons">
                                    <h1>Đặt hàng</h1>
                                </div>
                                <div id="cart-form">
                                    <table id="shopping-cart-table" class="data-table cart-table">
                                        <thead>
                                            <tr>
                                                <th style="width: 40%; text-indent: 20px;"><span class="nobr">Sản phẩm</span></th>
                                                <th class="a-center"><span class="nobr">Đơn giá</span></th>
                                                <th class="a-center" style="width: 20%;">Số lượng</th>
                                                <th class="a-center">Thành tiền</th>
                                            </tr>
                                        </thead>
                                        <tbody> 
                                            <c:set var="totalPrice" value="0" />
                                            <c:forEach items="${pList}" var="p">
                                                <tr>

                                                    <td>
                                                        <div class="product-image">
                                                            <a href="productdetail?Pid=${p.key.pid}" title="Lớp Học Mật Ngữ - Siêu Thú Ngân Hà" class="product-image-link">
                                                                <img src="images/${p.key.img}" alt="Lớp Học Mật Ngữ - Siêu Thú Ngân Hà" width="65" height="65"></a>
                                                        </div>
                                                        <div class="product-info">
                                                            <a href="productdetail?Pid=${p.key.pid}">${p.key.pname}</a>
                                                        </div>
                                                    </td>
                                                    <td class="a-center">
                                                        <c:choose>
                                                            <c:when test="${p.key.isIsDiscount() && p.key.priceSale != 0}">
                                                                <fmt:formatNumber value="${p.key.priceSale}" pattern="#,##0" var="Price" />
                                                                <fmt:formatNumber value="${p.key.priceSale * p.value}" pattern="#,##0" var="money" />
                                                                <c:set var="totalPrice" value="${totalPrice + (p.key.priceSale * p.value)}" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:formatNumber value="${p.key.price}" pattern="#,##0" var="Price" />
                                                                <fmt:formatNumber value="${p.key.price * p.value}" pattern="#,##0" var="money" />
                                                                <c:set var="totalPrice" value="${totalPrice + (p.key.price * p.value)}" />


                                                            </c:otherwise>
                                                        </c:choose>



                                                        <span class="price">${Price} vnđ</span>
                                                    </td>
                                                    <td class="a-center">
                                                        <p class="contain-qty contain-qty-hidden ">
                                                            <input readonly="true" type="text"  value="${p.value}" lang="1" size="3" title="Số lượng" class="input-text qty update-qty-index" maxlength="4">
                                                        </p>
                                                    </td>
                                                    <td class="a-center">

                                                        <span class="price" id="total-item-551078">${money}</span><span class="currentcy">vnđ</span>
                                                    </td>

                                                </tr>


                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    <div class="payment_cusbilling">
                                        <div class="paymen_itemtitle">
                                            Địa chỉ giao hàng
                                        </div>
                                        <form  id="formA" action="checkout" method="post">
                                            <div class="paymen_itemcontent">
                                                <div class="row">
                                                    <div class="col-lg-6">

                                                        <div class="input-container">
                                                            <label>Email </label> 
                                                            <input  name="email" id="Order_email" type="text"
                                                                    maxlength="255" value="${sessionScope.account.email}" disabled>
                                                        </div>


                                                        <div class="input-container">
                                                            <label for="Order_note">Ghi chú</label>
                                                            <textarea name="note" style="width: 283px; height: 80px;" maxlength="250"></textarea>
                                                        </div>
                                                        <div class="input-container">
                                                            <label class="required">Mã giảm giá</label>
                                                            <div class="input-group-giftcode">
                                                                <input class="input_text" id="coupon_input" name="coupon_code" placeholder="Nhập mã giảm giá" maxlength="255" value="${codeSale.codeSale}">
                                                                    <a id="btn_gifcode" href="#" onclick="applyCoupon()">Áp dụng</a>

                                                            </div>
                                                        </div>
                                                        <c:if test="${codeSale.discountConditions>totalPrice}">
                                                            <fmt:formatNumber value="${codeSale.discountConditions/1000}" pattern="#" var="discountConditions" />
                                                            <p style="color: red">MÃ Giảm giá chỉ áp dụng cho đơn hàng trên ${discountConditions}K</p>
                                                        </c:if>
                                                        <p style="color: red">${err}</p>

                                                        <script>
                                                            function applyCoupon() {
                                                                var inputField = document.getElementById('coupon_input');
                                                                var couponValue = inputField.value;
                                                                var totalPrice = "${totalPrice}";

                                                                // Chuyển hướng đến trang khác và truyền giá trị của trường input qua tham số URL
                                                                window.location.href = "useCodeSale?codeSale=" + encodeURIComponent(couponValue);
                                                                ;
                                                            }
                                                        </script>







                                                        <input type="hidden" name="totalPrice" value="${totalPrice}">


                                                    </div>

                                                    <c:choose>
                                                        <c:when test="${listsh.size()==0}">
                                                            <div class="col-lg-6">
                                                                <div class="input-container">
                                                                    <label>Tỉnh/Thành Phố    <span class="required">*</span></label>
                                                                    <select name="city" class="form-select form-select-sm mb-3" id="city" aria-label=".form-select-sm">
                                                                        <option value="" selected>Chọn tỉnh thành</option>     


                                                                    </select> </div>
                                                                <div class="input-container">
                                                                    <label>Quận Huyện    <span class="required">*</span></label>

                                                                    <select name="district" class="form-select form-select-sm mb-3" id="district" aria-label=".form-select-sm">
                                                                        <option value="" selected>Chọn quận huyện</option>

                                                                    </select>  </div>
                                                                <div class="input-container">
                                                                    <label>Phường xã   <span class="required">*</span></label>

                                                                    <select name="ward" class="form-select form-select-sm" id="ward" aria-label=".form-select-sm">
                                                                        <option value="" selected>Chọn phường xã</option>

                                                                    </select>
                                                                </div>
                                                                <div class="input-container">
                                                                    <label>Người nhận   <span class="required">*</span></label>
                                                                    <input name="receiver" id="Order_name" type="text" maxlength="255" value="${receiver}">
                                                                </div>
                                                                <div class="input-container">
                                                                    <label>Địa chỉ    <span class="required">*</span></label>
                                                                    <input name="address" id="Order_name" type="text" maxlength="255" value="${address}">
                                                                </div>


                                                                <div class="input-container">
                                                                    <label for="Order_phone" class="required">Điện thoại
                                                                        <span class="required">* </span></label> 
                                                                    <input name="phonenumber" id="Order_phone" type="text" maxlength="255" value="${phonenumber}"> 


                                                                </div>  

                                                            </div>
                                                            <div id="cart_footer" class="tfoot-right" style="margin-right: 0;">
                                                                <div class="totals">
                                                                    <table id="shopping-cart-totals-table">
                                                                        <tfoot>
                                                                            <tr>
                                                                                <td style="" class="a-right" colspan="1">
                                                                                    Tiền hàng:</td>
                                                                                <td id="cart-subtotal" class="a-right">
                                                                                    </div>
                                                                                    <span class="price" id="subtotal"><fmt:formatNumber value="${totalPrice}" pattern="#,##0" /></span>
                                                                                    <span class="currentcy">vnđ</span> </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="" class="a-right" colspan="1">
                                                                                    Giảm giá:</td>
                                                                                <td class="a-right">
                                                                                    -<span class="price" id="discount_money"><c:choose>
                                                                                            <c:when test="${codeSale!=null && codeSale.discountConditions<totalPrice}">
                                                                                                -<fmt:formatNumber value="${codeSale.discount}" pattern="#,##0" />
                                                                                                <input type="hidden" name="codeSale" value="${codeSale.codeSale}">


                                                                                                </c:when>

                                                                                                <c:otherwise>
                                                                                                    0
                                                                                                </c:otherwise>
                                                                                            </c:choose> </span><span class="currentcy">vnđ</span></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="a-right" colspan="">
                                                                                    <strong>Tổng tiền:</strong>
                                                                                </td>
                                                                                <td id="total_amount" style="" class="a-right">
                                                                                    <strong><span class="price" id="total_money">
                                                                                            <c:choose>
                                                                                                <c:when test="${codeSale!=null && codeSale.discountConditions<totalPrice}">
                                                                                                    <fmt:formatNumber value="${totalPrice-codeSale.discount}" pattern="#,##0" />

                                                                                                </c:when>

                                                                                                <c:otherwise>
                                                                                                    <fmt:formatNumber value="${totalPrice}" pattern="#,##0" />
                                                                                                </c:otherwise>
                                                                                            </c:choose> </span><span class="currentcy">vnđ</span></strong>
                                                                                </td>
                                                                            </tr>
                                                                        </tfoot>
                                                                    </table>
                                                                </div>
                                                                <div id="checkoutButton">
                                                                    <a href="" onclick="submitFormA(); return false;" rel="nofollow" title="Xác nhận" class="btn-proceed-checkout" >
                                                                        Đặt hàng
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            </form></div>

                                                    </c:when>
                                                    <c:otherwise>

                                                        <div class="col-lg-6">
                                                            <div class="input-container">
                                                                <label>Người nhận   <span class="required">*</span></label>
                                                                <input name="receiver" id="Order_name" type="text" maxlength="255" value="${listsh.get(0).receiver}" readonly>
                                                            </div>
                                                            <div class="input-container">
                                                                <label>Địa chỉ    <span class="required">*</span></label>
                                                                <input name="address" id="Order_name" type="text" maxlength="255" value="${listsh.get(0).address}" readonly>
                                                            </div>
                                                            <div class="input-container">
                                                                <label for="Order_phone" class="required">Điện thoại
                                                                    <span class="required">* </span></label> 
                                                                <input name="phonenumber" id="Order_phone" type="text" maxlength="255" value="${listsh.get(0).phonenumber}" readonly> 


                                                            </div>  
                                                            </form>
                                                            <div>

                                                                <div class="col-lg-4" ></div>
                                                                <form action="editShipment" method="post">
                                                                    <c:if test="${codeSale!=null && codeSale.discountConditions<totalPrice}">
                                                                        <input type="hidden" name="codeSale" value="${codeSale.codeSale}">
                                                                        </c:if>

                                                                        <div class="col-lg-8" >
                                                                            <select name="type" style="width: 50%" id="codeSelect" onchange="toggleCode()">
                                                                                <option >Chọn thao tác</option>
                                                                                <option value="changeInfo">Đổi địa chỉ giao hàng</option>
                                                                                <c:choose>
                                                                                    <c:when test="${displayForm != null}">
                                                                                        <option value="addNew" selected>Thêm địa chỉ giao hàng</option>
                                                                                    </c:when>
                                                                                    <c:otherwise>
                                                                                        <option value="addNew" >Thêm địa chỉ giao hàng</option>
                                                                                    </c:otherwise>
                                                                                </c:choose>




                                                                                <option value="cancel">Hủy</option> 
                                                                            </select></div>   </div>   

                                                                        </div>


                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-lg-6"></div>

                                                                            <div class="col-lg-6">  

                                                                                <!--                                                            
                                                                 Add new Address-->
                                                                                <div id="addNewSnippet" style="display: none;">
                                                                                    <p>Thêm địa chỉ giao hàng</p><br>
                                                                                        <div class="input-container">
                                                                                            <label>Tỉnh/Thành Phố    <span class="required">*</span></label>
                                                                                            <select  name="city" class="form-select form-select-sm mb-3" id="city" aria-label=".form-select-sm">
                                                                                                <option value=""  selected>Chọn tỉnh thành</option>           
                                                                                            </select> </div>
                                                                                        <div class="input-container">
                                                                                            <label>Quận Huyện    <span class="required">*</span></label>

                                                                                            <select name="district"  class="form-select form-select-sm mb-3" id="district" aria-label=".form-select-sm">
                                                                                                <option value="" selected>Chọn quận huyện</option>
                                                                                            </select>  </div>
                                                                                        <div class="input-container">
                                                                                            <label>Phường xã  <span class="required">*</span></label>

                                                                                            <select name="ward" class="form-select form-select-sm" id="ward" aria-label=".form-select-sm">
                                                                                                <option value="" selected>Chọn phường xã</option>
                                                                                            </select>
                                                                                        </div>
                                                                                        <div class="input-container">
                                                                                            <label>Người nhận    </label>
                                                                                            <input name="newReceiver" id="Order_name" type="text" maxlength="255" value="${receiver}"> </div>
                                                                                        <div class="input-container">
                                                                                            <label>Địa chỉ    </label>
                                                                                            <input name="newAddress" id="Order_name" type="text" maxlength="255" value="${address}"> </div>

                                                                                        <div class="input-container">   <label>Số Điện thoại </label> 
                                                                                            <input name="newPhonenumber" id="Order_phone" type="text" maxlength="255" value="${phonenumber}"> <br>

                                                                                                    </div>  </div>


                                                                                                    <!--  Change Address-->
                                                                                                    <div id="codeSnippet" style="display: none;">
                                                                                                        <p>Đổi địa chỉ giao hàng</p><br>
                                                                                                            <c:forEach items="${listsh}" var="sh">
                                                                                                                <input type="radio" name="changeMarkShip" value="${sh.aaid}">  
                                                                                                                    <p style="color: orange">Người nhận:  ${sh.receiver}</p><br>
                                                                                                                        <p>Địa chỉ:  ${sh.address}</p><br>

                                                                                                                            <p>Điện thoại: ${sh.phonenumber}</p>
                                                                                                                        </c:forEach>
                                                                                                                        </div>



                                                                                                                        </div>
                                                                                                                        <div class="row">
                                                                                                                            <div class="col-lg-8"></div>
                                                                                                                            <div class="col-lg-4" id="noneSubmit" style="display: none;"> 
                                                                                                                                <input type="submit" value="Submit">
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        </div>
                                                                                                                        </form>


                                                                                                                        <div id="cart_footer" class="tfoot-right" style="margin-right: 0;">
                                                                                                                            <div class="totals">
                                                                                                                                <table id="shopping-cart-totals-table">
                                                                                                                                    <tfoot>
                                                                                                                                        <tr>
                                                                                                                                            <td style="" class="a-right" colspan="1">
                                                                                                                                                Tiền hàng:</td>
                                                                                                                                            <td id="cart-subtotal" class="a-right">
                                                                                                                                                <span class="price" id="subtotal"><fmt:formatNumber value="${totalPrice}" pattern="#,##0" /></span>
                                                                                                                                                <span class="currentcy">vnđ</span> </td>
                                                                                                                                        </tr>
                                                                                                                                        <tr>
                                                                                                                                            <td style="" class="a-right" colspan="1">
                                                                                                                                                Giảm giá:</td>
                                                                                                                                            <td class="a-right">
                                                                                                                                                <span class="price" id="discount_money">
                                                                                                                                                    <c:choose>
                                                                                                                                                        <c:when test="${codeSale!=null && codeSale.discountConditions < totalPrice}">
                                                                                                                                                            <fmt:formatNumber value="${codeSale.discount}" pattern="#,##0" />
                                                                                                                                                            <input type="hidden" name="codeSale" value="${codeSale.codeSale}">
                                                                                                                                                            </c:when>

                                                                                                                                                            <c:otherwise>
                                                                                                                                                                0
                                                                                                                                                            </c:otherwise>
                                                                                                                                                        </c:choose> </span><span class="currentcy">vnđ</span></td>
                                                                                                                                        </tr>
                                                                                                                                        <tr>
                                                                                                                                            <td class="a-right" colspan="">
                                                                                                                                                <strong>Tổng tiền:</strong>
                                                                                                                                            </td>
                                                                                                                                            <td id="total_amount" style="" class="a-right">
                                                                                                                                                <strong><span class="price" id="total_money">  <c:choose> <c:when test="${codeSale!=null && codeSale.discountConditions<totalPrice}">
                                                                                                                                                                <fmt:formatNumber value="${totalPrice-codeSale.discount}" pattern="#,##0" />

                                                                                                                                                            </c:when>

                                                                                                                                                            <c:otherwise>
                                                                                                                                                                <fmt:formatNumber value="${totalPrice}" pattern="#,##0" />
                                                                                                                                                            </c:otherwise>
                                                                                                                                                        </c:choose></span><span class="currentcy">vnđ</span></strong>
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </tfoot>
                                                                                                                                </table>
                                                                                                                            </div>
                                                                                                                            <div id="checkoutButton">
                                                                                                                                <a href="" onclick="submitFormA(); return false;" rel="nofollow" title="Xác nhận" class="btn-proceed-checkout" >
                                                                                                                                    Đặt hàng
                                                                                                                                </a>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        </div>
                                                                                                                    </c:otherwise>
                                                                                                                </c:choose>

                                                                                                                </div>
                                                                                                                </div>
                                                                                                                </div>
                                                                                                                </div>
                                                                                                                </div>

                                                                                                                </div>
                                                                                                                <div class="clear"></div>
                                                                                                                <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
                                                                                                                <script>
                                                                                                                                    var cities = document.getElementById("city");
                                                                                                                                    var districts = document.getElementById("district");
                                                                                                                                    var wards = document.getElementById("ward");
                                                                                                                                    var selectedCityValue = '${city}'; // Replace with the actual value of the selected city

                                                                                                                                    var Parameter = {
                                                                                                                                        url: "https://raw.githubusercontent.com/kenzouno1/DiaGioiHanhChinhVN/master/data.json",
                                                                                                                                        method: "GET",
                                                                                                                                        responseType: "application/json",
                                                                                                                                    };

                                                                                                                                    var promise = axios(Parameter);

                                                                                                                                    promise.then(function (result) {
                                                                                                                                        renderCity(result.data);
                                                                                                                                        selectCityOption(result.data);
                                                                                                                                        selectDistrictOption(result.data);
                                                                                                                                        selectWardOption(result.data);
                                                                                                                                    });

                                                                                                                                    function selectCityOption(data) {
                                                                                                                                        for (let i = 0; i < cities.options.length; i++) {
                                                                                                                                            if (cities.options[i].value === selectedCityValue) {
                                                                                                                                                cities.options[i].selected = true;
                                                                                                                                                // Trigger change event to populate districts and wards
                                                                                                                                                simulateEvent(cities, 'change');
                                                                                                                                                break;
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                    function selectDistrictOption(data) {
                                                                                                                                        const selectedCity = data.find((city) => city.Name === selectedCityValue);
                                                                                                                                        for (let i = 0; i < selectedCity.Districts.length; i++) {
                                                                                                                                            if (selectedCity.Districts[i].Name === '${district}') {
                                                                                                                                                districts.options[i + 1].selected = true; // Plus 1 to account for the initial "Chọn quận huyện" option
                                                                                                                                                simulateEvent(districts, 'change');
                                                                                                                                                break;
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }

                                                                                                                                    function selectWardOption(data) {
                                                                                                                                        const selectedCity = data.find((city) => city.Name === selectedCityValue);
                                                                                                                                        const selectedDistrict = selectedCity.Districts.find((district) => district.Name === '${district}');
                                                                                                                                        for (let i = 0; i < selectedDistrict.Wards.length; i++) {
                                                                                                                                            if (selectedDistrict.Wards[i].Name === '${ward}') {
                                                                                                                                                wards.options[i + 1].selected = true; // Plus 1 to account for the initial "Chọn phường xã" option
                                                                                                                                                simulateEvent(wards, 'change');
                                                                                                                                                break;
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                    function renderCity(data) {
                                                                                                                                        for (const city of data) {
                                                                                                                                            cities.options[cities.options.length] = new Option(city.Name, city.Name); // Use "Name" as both value and text.
                                                                                                                                        }

                                                                                                                                        cities.onchange = function () {
                                                                                                                                            districts.length = 1;
                                                                                                                                            wards.length = 1;

                                                                                                                                            if (this.value !== "") {
                                                                                                                                                const selectedCity = data.find((city) => city.Name === this.value);

                                                                                                                                                for (const district of selectedCity.Districts) {
                                                                                                                                                    districts.options[districts.options.length] = new Option(district.Name, district.Name); // Use "Name" as both value and text.
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        };

                                                                                                                                        districts.onchange = function () {
                                                                                                                                            wards.length = 1;
                                                                                                                                            const selectedCity = data.find((city) => city.Name === cities.value);
                                                                                                                                            const selectedDistrict = selectedCity.Districts.find((district) => district.Name === this.value);

                                                                                                                                            if (this.value !== "") {
                                                                                                                                                for (const ward of selectedDistrict.Wards) {
                                                                                                                                                    wards.options[wards.options.length] = new Option(ward.Name, ward.Name); // Use "Name" as both value and text.
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        };
                                                                                                                                    }

                                                                                                                                    // Function to simulate change event
                                                                                                                                    function simulateEvent(element, eventName) {
                                                                                                                                        var event = new Event(eventName);
                                                                                                                                        element.dispatchEvent(event);
                                                                                                                                    }

                                                                                                                </script>

                                                                                                                <script>
    document.addEventListener("DOMContentLoaded", function () {
        var addNewSnippet = document.getElementById("addNewSnippet");
        var noneSubmit = document.getElementById("noneSubmit");
        var displayFormValue = '${displayForm}';

        if (displayFormValue && displayFormValue !== 'null') {
            addNewSnippet.style.display = "block";
            noneSubmit.style.display = "block";
        }
    });
</script>

                                                                                                                </body>
                                                                                                                </html>