<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi" lang="vi"><head>
    
            <title> Order Information</title>



            <link rel="stylesheet" type="text/css" href="https://cdn0.fahasa.com/media/css_secure/5cc046b695588987268a3c8c9699f3ac.css?q=102551" media="all">
                <link rel="stylesheet" type="text/css" href="https://cdn0.fahasa.com/media/css_secure/5722a920567ae30e71034727845e9254.css?q=102551" media="print">
              <link href="https://www.fahasa.com/blog/rss/index/store_id/1/" title="Blog" rel="alternate" type="application/rss+xml">
                        <link rel="stylesheet" type="text/css" href="https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/css/default.css?q=102551" media="all">

                            <link rel="stylesheet" type="text/css" href="https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/css/header.css?q=102551" media="all">
                                <link rel="stylesheet" type="text/css" href="https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/css/select2.min.css?q=102551" media="all">
                                   
                                                    <link rel="stylesheet" href="css/orderInfo.css" >


                                                        <%@include file="Header.jsp"  %>
  <style>
              #footer{background: #132539;
    }
#footer .footer_center .info-foot {
    float: left;
    min-height: 370px;
    padding-top: 35px;
    width: 100%;
    background: #132539;
}

                                </style>
                                                        </head>
                                                        <body id="offcanvas-container" class=" sales-order-view">                               
                                                            <div class="main-container col2-left-layout no-margin-top">
                                                                <div class="main">
                                                                    <div>

                                                                    </div>
                                                                    <!--                                <div></div>-->
                                                                    <div class="container">
                                                                        <div class="container-inner">
                                                                            <div class="d-flex justify-content-center">
                                                                                <div class="col-left sidebar col-lg-3 col-md-3 col-sm-12 col-xs-12" style="padding-left: 0px;">

                                                                                    <div class="block block-account">
                                                                                        <div class="block-title">
                                                                                            <strong><span>Tài khoản</span></strong>
                                                                                        </div>
                                                                                        <div class="block-content">
                                                                                            <ul>
                                                                                               
                                                                                                <li><a href="profile">Thông tin tài khoản</a></li>
                                                                                                <li><a href="shipmentDetails">Sổ địa chỉ</a></li>
                                                                                                <li class="orderHistory"><strong><a href="orderHistory">Đơn hàng của tôi</a></strong></li>    
                                                                                                 <li><a href="home">Về trang chủ</a></li>
                                                                                               

                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                            <div class="col-main col-lg-9 col-md-9 col-sm-12 col-xs-12 col-fhs-main-body" style="margin-top: 8px;">
                                                                                <div class="my-account"><!--<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">-->

                                                                                    <div class="order-view-content-info">
                                                                                        <div class="order-view-title">Chi tiết đơn hàng</div><div class="order-view-id-mobile"><span>Mã đơn hàng: </span><span>${order.oid}</span></div>
                                                                                        <div>
                                                                                            <c:if test="${order.status==0}">
                                                                                            <div class="order-view-status" style="background:#FCDAB0;color:#F7941E;border-color:#FCDAB0;">Đơn hàng Chờ xác nhận</div></c:if>
                                                                                               <c:if test="${order.status==1}">
                                                                                            <div class="order-view-status" style="background:#FCDAB0;color:#F7941E;border-color:#FCDAB0;">Đang xử lí</div></c:if>
                                                                        <c:if test="${order.status==2}">
                                                                                            <div class="order-view-status" style="background:#FCDAB0;color:#F7941E;border-color:#FCDAB0;">Hoàn tất</div></c:if>
                                                                       
                                                                                            
                                                                                            <c:if test="${order.status==3}">
                                                                                          <div class="subOder-progress-bar" style="background:#F3B4AF;color:#A90000;border-color:#F3B4AF;">Đơn hàng Bị hủy</div></c:if>
                                                                                            
                                                                                            <div class="order-view-id"><span>Mã đơn hàng: </span><span>#${order.oid}</span></div>
                                                                                            <div class="order-view-date"><span>Ngày mua: </span><span>${order.ordered_at}</span></div>
                                                                                            <fmt:formatNumber value="${order.totalAmount}" pattern="#,##0" var="totalAmount" />
                                                                                            <fmt:formatNumber value="${order.discount}" pattern="#,##0" var="discount" />
                                                                                            <fmt:formatNumber value="${order.totalAmount-order.discount}" pattern="#,##0" var="total" />
                                                                                            <div class="order-view-total"><span>Tổng Tiền: </span><span><span class="price">${total}</span><span class="sym-totals">đ</span></span></div>

                                                                                            <div class="order-view-note"><span style="flex:1">Ghi chú: </span>
                                                                                                <c:if test="${order.note==null}">
                                                                                                     <span class="dont-have-info" style="flex:16">(Không có)</span>
                                                                                                </c:if>
                                                                                                      <c:if test="${order.note!=null}">
                                                                                                     <span class="dont-have-info" style="flex:16">${order.note}</span>
                                                                                                </c:if>
                                                                                                     
                                                                                               
                                                                                            </div>
                                                                                        </div>


                                                                                        <div style="text-align:center;" class="order-view-buttons-color">

                                                                                            <div class="order-view-buttons-color-child">
                                                                                                <c:if test="${sessionScope.account.role ==4}">

                                                                                             
                                                                                                <c:if test="${order.status==0}">
                                                                                                    <a href="#" id="cancel-order" class="order-view-review-btn">Hủy đơn hàng</a>
                                                                                                    <form id="cancel-order-form" action="orderInfo" method="get">
                                                                                                        <input type="hidden" name="oid" value="${order.oid}">
                                                                                                    </form>
                                                                                                </c:if>
                                                                                                     <c:if test="${order.status==2}">
                                                                                                    <a href="#" id="return-order" class="order-view-review-btn">Trả lại hàng</a>
                                                                                                    <form id="return-order-form" action="orderInfo" method="get">
                                                                                                        <input type="hidden" name="oid" value="${order.oid}">
                                                                                                    </form>
                                                                                                </c:if>
                                                                                                <script>
                                                                                                    document.getElementById("cancel-order").addEventListener("click", function (event) {
                                                                                                        event.preventDefault(); // Ngăn chặn chuyển đến liên kết mặc định

                                                                                                        // Hiển thị hộp thoại cảnh báo
                                                                                                        var confirmation = confirm("Bạn có chắc chắn muốn hủy đơn hàng không?");

                                                                                                        // Nếu người dùng xác nhận thông báo, gửi biểu mẫu
                                                                                                        if (confirmation) {
                                                                                                            document.getElementById("cancel-order-form").submit();
                                                                                                        }
                                                                                                    });
                                                                                                     document.getElementById("return-order").addEventListener("click", function (event) {
                                                                                                        event.preventDefault(); // Ngăn chặn chuyển đến liên kết mặc định

                                                                                                        // Hiển thị hộp thoại cảnh báo
                                                                                                        var confirmation = confirm("Bạn có chắc chắn muốn trả đơn hàng không?");

                                                                                                        // Nếu người dùng xác nhận thông báo, gửi biểu mẫu
                                                                                                        if (confirmation) {
                                                                                                            document.getElementById("return-order-form").submit();
                                                                                                        }
                                                                                                    });
                                                                                                </script>
                                                                                                    </c:if>

                                                                                            </div>
                                                                                        </div>   


                                                                                    </div>


                                                                                    <div class="order-info-border-block"></div>
                                                                                    <div class="order-view-content-details">
                                                                                        <div class="order-view-content-box1">
                                                                                            <div class="order-view-box">
                                                                                                <div class="order-box-title">
                                                                                                    <div class="order-view-title">Thông tin người nhận</div>
                                                                                                </div>
                                                                                                <div class="order-box-info">
                                                                                                    <address>
                                                                                                     ${order.receiver}
                                                                                                   
                                                                                                        <br>

                                                                                                           <br>



                                                                                                                ${order.address}<br>
                                                                                                                    Tel: ${order.phonenumber}

                                                                                                                    </address>
                                                                                                                    </div>
                                                                                                                    </div>
                                                                                                                    <div class="order-view-box">
                                                                                                                        <div class="order-info-shipping-description">
                                                                                                                            <div class="order-box-title">
                                                                                                                                <div class="order-view-title">Phương thức vận chuyển</div>
                                                                                                                            </div>
                                                                                                                            <div class="order-box-info">
                                                                                                                                Giao hàng tiêu chuẩn                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="order-view-box">
                                                                                                                        <div class="order-box-title">
                                                                                                                            <div class="order-view-title">Phương thức thanh toán</div>
                                                                                                                        </div>

                                                                                                                        <div class="order-box-info">
                                                                                                                            <div class="order-box-info-historypayment">
                                                                                                                                <div><p><strong>Thanh toán bằng tiền mặt khi nhận hàng</strong></p>


                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    </div>
                                                                                                                      <div class="order-view-status-container">
         <c:if test="${order.status==0}" >                  
         <div class="order-view-status-new-order">
            <div class="order-view-icon-container">
                <div style="height: 60px;width: 60px;align-self: center;"><div class="order-view-icon-img" style="background: url(https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/order/ico_donhangmoi_orange.svg) no-repeat center;border-color:#f7941f;"></div></div>
                <div class="order-view-icon-content"><p>Đơn hàng mới</p><p>${order.ordered_at}</p></div>
            </div>
           
        </div>
         </c:if>    
                                                                                                                    
              <c:if test="${order.status==1}" >   
       <div class="order-view-status-new-order">
            <div class="order-view-icon-container">
                <div style="height: 60px;width: 60px;align-self: center;"><div class="order-view-icon-img" style="background: url(https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/order/ico_donhangmoi_blue.svg) no-repeat center;border-color:#005FCC;"></div></div>
                <div class="order-view-icon-content"><p>Đơn hàng mới</p><p>${order.ordered_at}</p></div>
            </div>
             <div class="order-view-progress-bar" style="background:#005FCC;"></div>
        </div>
        <div class="order-view-status-new-order">
            <div class="order-view-icon-container">
                <div style="height: 60px;width: 60px;align-self: center;"><div class="order-view-icon-img" style="background: url(https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/order/ico_dangxuly_blue.svg) no-repeat center;border-color:#005FCC;"></div></div>
                <div class="order-view-icon-content"><p>Đang xử lý</p><p></p></div>
            </div>
         
        </div> 
       </c:if>    
        <c:if test="${order.status==2}" > <div class="order-view-status-new-order">
            <div class="order-view-icon-container">
                <div style="height: 60px;width: 60px;align-self: center;"><div class="order-view-icon-img" style="background: url(https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/order/ico_donhangmoi_green.svg) no-repeat center;border-color:#228B22;;"></div></div>
                <div class="order-view-icon-content"><p>Đơn hàng mới</p><p>${order.ordered_at}</p></div>
            </div>
            <div class="order-view-progress-bar" style="border-color:#228B22;"></div>
        </div>
        <div class="order-view-status-new-order">
            <div class="order-view-icon-container">
                <div style="height: 60px;width: 60px;align-self: center;"><div class="order-view-icon-img" style="background: url(https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/order/ico_dangxuly_green.svg) no-repeat center;border-color:#228B22;"></div></div>
                <div class="order-view-icon-content"><p>Đang xử lý</p><p></p></div>
            </div>
            <div class="order-view-progress-bar" style="background:#228B22;"></div>
        </div>
        <div class="order-view-status-new-order">
             <div class="order-view-icon-container">
                <div style="height: 60px;width: 60px;align-self: center;"><div class="order-view-icon-img" style="background: url(https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/order/ico_hoantat_green.svg) no-repeat center;border-color:#228B22;"></div></div>
                <div class="order-view-icon-content"><p>Hoàn tất</p><p>${order.enddate}</p></div>
            </div>
            
        </div>  </c:if>       
        <c:if test="${order.status==3}" >     <div class="order-view-status-new-order">
            <div class="order-view-icon-container">
                <div style="height: 60px;width: 60px;align-self: center;"><div class="order-view-icon-img" style="background: url(https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/order/ico_donhangmoi_red.svg) no-repeat center;border-color:#fa0001;"></div></div>
                <div class="order-view-icon-content"><p>Đơn hàng mới</p><p>${order.ordered_at}</p></div>
            </div>
             <div class="order-view-progress-bar" style="background:#fa0001;"></div>
        </div>
        <div class="order-view-status-new-order">
            <div class="order-view-icon-container">
                <div style="height: 60px;width: 60px;align-self: center;"><div class="order-view-icon-img" style="background: url(https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/order/ico_dangxuly_red.svg) no-repeat center;border-color:#fa0001;"></div></div>
                <div class="order-view-icon-content"><p>Đang xử lý</p><p></p></div>
            </div>
            <div class="order-view-progress-bar" style="background:#fa0001;"></div>
        </div>
        <div class="order-view-status-new-order">
            <div class="order-view-icon-container">
                <div style="height: 60px;width: 60px;align-self: center;"><div class="order-view-icon-img" style="background: url(https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/order/ico_huy_red.svg) no-repeat center;border-color:#fa0001;"></div></div>
                <div class="order-view-icon-content"><p>Bị hủy</p><p>${order.enddate}</p></div>
            </div>
            
        </div>   </c:if>      
 

                                                                                                                    </div>
                                                                                                                   
                                                                                                                

                                                                                                                    </div>
                                                                                                                    <div class="order-details-items">

                                                                                                                        <div class="border-block-mobile-desktop"></div>
                                                                                                                        <div class="order-subOrder-container">
                                                                                                                            <div class="order-subOrder-items">
                                                                                                                                <div class="order-subOrder-info-status">
                                                                                                                                    <div class="order-subOrder-info" >
                                                                                                                                        <div><span>Đơn hàng:</span><span>#${order.oid}</span></div>
                                                                                                                                        
                                                                                                                                        <div><span>Tổng tiền:</span><span><span class="price">${total}</span><span style="sym-totals">đ</span></span></div>
                                                                                                                                        <div class="order-subOrder-quantity"><span>Số lượng:</span><span>${list.size()}</span></div>
                                                                                                                                        <div class="order-subOrder-arrow"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="order-subOrder-products id-list-products-103313930">
                                                                                                                                <div class="table-subOrder-container">
                                                                                                                                    <div class="table-subOrder-header-and-img">
                                                                                                                                        <div class="table-subOrder-title-product order-view-title">Sản phẩm</div>
                                                                                                                                        <div class="table-subOrder-row table-subOrder-header">
                                                                                                                                            <div class="table-subOrder-cell" style="width:100px">Hình ảnh</div>
                                                                                                                                            <div class="table-subOrder-cell">Tên sản phẩm</div>

                                                                                                                                            <div class="table-subOrder-cell">Giá bán</div>
                                                                                                                                            <div class="table-subOrder-cell">SL</div>
                                                                                                                                            <div class="table-subOrder-cell">Thành tiền</div>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <c:forEach items="${list}" var="entry">


                                                                                                                                        <div class="table-subOrder-parent-img-and-cell">

                                                                                                                                            <div class="table-subOrder-row">
                                                                                                                                                <div class="table-subOrder-cell table-subOrder-img-web"><img src="images/${entry.getValue().img}"></div>
                                                                                                                                                <div class="table-subOrder-cell table-subOrder-name-product">
                                                                                                                                                    <div class="table-subOrder-name-tag-a">
                                                                                                                                                        <a href="productdetail?Pid=${entry.value.pid}" style="height: auto;">
                                                                                                                                                            ${entry.value.pname}    </a>
                                                                                                                                                    </div>
                                                                                                                                                </div>

                                                                                                                                                <div class="table-subOrder-cell">
                                                                                                                                                    <span class="table-subOrder-hidden-desktop">Giá bán:&nbsp;</span>
                                                                                                                                                    <span>
                                                                                                                                                        <!--<span class="price-incl-tax">-->
                                                                                                                                                        <span class="cart-price">

                                                                                                                                                            <div class="cart-orderHs-price">
                                                                                                                                                                <fmt:formatNumber value="${entry.key.price}" pattern="#,##0" var="Price" />
                                                                                                                                                                <div><span class="price">${Price}</span> đ</div>
                                                                                                                                                               
                                                                                                                                                            </div>
                                                                                                                                                            <!--</span>-->
                                                                                                                                                        </span>
                                                                                                                                                    </span> 
                                                                                                                                                </div>
                                                                                                                                                <div class="table-subOrder-cell">
                                                                                                                                                    <span class="table-subOrder-hidden-desktop">Số lượng:&nbsp;</span>
                                                                                                                                                    <span>
                                                                                                                                                        <strong>${entry.key.quantity}</strong><br>
                                                                                                                                                    </span> 
                                                                                                                                                </div>
                                                                                                                                                <div class="table-subOrder-cell">
                                                                                                                                                    <span class="table-subOrder-hidden-desktop">Thành tiền:&nbsp;</span>
                                                                                                                                                    <span>
                                                                                                                                                        <!--<span class="price-incl-tax">-->
                                                                                                                                                        <span class="cart-price">
                                                                                                                                                            <fmt:formatNumber value="${entry.key.price*entry.key.quantity}" pattern="#,##0" var="totalPrice" />
                                                                                                                                                            <span class="price">${totalPrice}</span> đ					    <!--</span>-->
                                                                                                                                                        </span>

                                                                                                                                                    </span> 
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                    </c:forEach>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="order-subOrder-total id-total-103313930">
                                                                                                                             
                                                                                                                                <div class="order-subOrder-total-desktop">
                                                                                                                                    <div>

                                                                                                                                        <p><span>Thành tiền: </span></p>
                                                                                                                                        <p><span>Giảm giá: </span></p>



<!--                                                                                                                                        <p><span>Phí vận chuyển: </span></p>-->

                                                                                                                                        <p><span>Tổng Số Tiền: </span></p>

                                                                                                                                    </div>
                                                                                                                                    <div>

                                                                                                                                        <p class="order-totals-price"><span class="price">${totalAmount}</span>&nbsp;<span class="sym-totals">đ</span></p>
                                                                                                                                        
                                                                                                                                        
                                                                                                                                        <p class="order-totals-price"><span class="price">${discount}</span>&nbsp;<span class="sym-totals">đ</span></p>






                                                                                                                                        <p class="order-totals-price"><span class="price">${total}</span>&nbsp;<span class="sym-totals">đ</span></p>

                                                                                                                                    </div>
                                                                                                                                </div> </div>

                                                                                                                        </div>


                                                                                                                    </div></div>                                            </div>
                                                                                                                    </div>
                                                                                                                    </div>
                                                                                                                    </div>
                                                                                                                    </div>	
                                                                                                                    </div>



                                                                                                                    <%@include file="Footer.jsp"  %>
                                                                                                                           
                                                                                                                            



                                                                                                                                          </body><div style="position: absolute; top: 0px; z-index: 2147483647; display: block !important;"></div></html>