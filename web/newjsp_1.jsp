<%-- 
    Document   : newjsp
    Created on : 26-10-2023, 22:20:38
    Author     : hihih
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>MKT</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!----css3---->

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

        <link rel="stylesheet" href="css/post/custom.css">
        <link rel="stylesheet" href="./css/post/adminStyle.css">
        

        <!--google fonts -->

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">


        <!--google material icon-->
        <link href="https://fonts.googleapis.com/css2?family=Material+Icons"rel="stylesheet">
        <!-- Include the necessary Chart.js library -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="chartt/lib/chart/chart.min.js"></script>
        <script src="chartt/lib/easing/easing.min.js"></script>
        <script src="chartt/lib/waypoints/waypoints.min.js"></script>
        <script src="chartt/lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="chartt/lib/tempusdominus/js/moment.min.js"></script>
        <script src="chartt/lib/tempusdominus/js/moment-timezone.min.js"></script>
        <script src="chartt/lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js"></script>


    </head>

    <body>
        <%@include file="Admin/LeftAdmin.jsp" %>

        <!-- main -->
        <div class="main">
            <%@include file="Admin/headerAdmin.jsp" %>

            <!-- Cards -->
            <div class="cardBox">
                <div class="card">
                    <div class="">
                        <div class="numbers">1,504</div>
                        <div class="cardName">Total News Views</div>
                    </div>
                    <div class="iconBx">
                        <ion-icon name="eye-outline"></ion-icon>
                    </div>
                </div>
                <div class="card">
                    <div class="">
                        <div class="numbers">80</div>
                        <div class="cardName">Sales</div>
                    </div>
                    <div class="iconBx">
                        <ion-icon name="cart-outline"></ion-icon>
                    </div>
                </div>
                <div class="card">
                    <div class="">
                        <div class="numbers">284</div>
                        <div class="cardName">Comments</div>
                    </div>
                    <div class="iconBx">
                        <ion-icon name="chatbubbles-outline"></ion-icon>
                    </div>
                </div>
                <div class="card">
                    <div class="">
                        <div class="numbers">$7,842</div>
                        <div class="cardName">Earning</div>
                    </div>
                    <div class="iconBx">
                        <ion-icon name="cash-outline"></ion-icon>
                    </div>
                </div>
            </div>

            <!-- chart -->
            <%@include file="Admin/AdminSettingList.jsp" %>
            <!-- end chart -->

        
                <!-- New customers -->
               
            </div>

        </div>

        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
        <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>

        <script>
                // menu toggle
                let toggle = document.querySelector('.toggle')
                let navigation = document.querySelector('.navigation')
                let main = document.querySelector('.main')

                toggle.addEventListener('click', function () {
                    navigation.classList.toggle('active')
                    main.classList.toggle('active')
                })

                // add hovered class in selected list item
                // let list = document.querySelectorAll('.navigation li')
                // function activeLink() {
                //     list.forEach((item) => item.classList.remove('hovered'));
                //     this.classList.add('hovered');            
                // }
                // list.forEach((item) => item.addEventListener('mouseover', activeLink));
        </script>

        <!--  flot-chart js -->
        <!--    <script src="js/MKT/common.min.js"></script>
        -->                
    </body>

</html>

