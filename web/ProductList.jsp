<%-- 
    Document   : ProductList
    Created on : 17-10-2023, 14:23:17
    Author     : tuanm
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<!DOCTYPE html>
<html lang="en" class="light-style layout-navbar-fixed layout-compact layout-menu-fixed" dir="ltr" data-theme="theme-default" data-assets-path="../../assets/" data-template="vertical-menu-template">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

        <title>Product List - eCommerce | Vuexy - Bootstrap Admin Template</title>

        <link href="css/css2" rel="stylesheet">
        <link rel="stylesheet" href="css/fontawesome.css">
        <link rel="stylesheet" href="css/tabler-icons.css">
        <link rel="stylesheet" href="css/flag-icons.css">
        <link rel="stylesheet" href="css/demo.css">
        <link rel="stylesheet" href="css/node-waves.css">
        <link rel="stylesheet" href="css/perfect-scrollbar.css">
        <link rel="stylesheet" href="css/typeahead.css"> 
        <link rel="stylesheet" href="css/select2.css">

        <style type="text/css">
            .layout-menu-fixed .layout-navbar-full .layout-menu,
            .layout-menu-fixed-offcanvas .layout-navbar-full .layout-menu {
                top: 62px !important;
            }
            .layout-page {
                padding-top: 62px !important;
            }
            .content-wrapper {
                padding-bottom: 56px !important;
            }</style>
        <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
        <!--? Template customizer: To hide customizer set displayCustomizer value false in config.js.  -->

        <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
        <link rel="stylesheet" type="text/css" href="css/core.css" class="template-customizer-core-css">
        <link rel="stylesheet" type="text/css" href="css/theme-default.css" class="template-customizer-theme-css">

    </head>

    <body>
        <div class="layout-wrapper layout-content-navbar  ">
            <div class="layout-container">
                <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme" data-bg-class="bg-menu-theme" style="touch-action: none; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                    <div class="app-brand demo ">
                        <a href="home" class="app-brand-link">
                            <span class="app-brand-logo demo">
                                <svg width="32" height="22" viewBox="0 0 32 22" fill="none" >
                                </svg>
                            </span>
                            <span class="app-brand-text demo menu-text fw-bold">Board Game</span>
                        </a>
                    </div>

                    <ul class="menu-inner py-1 ps ps--active-y">
                        <li class="menu-item active open">

                            <ul class="menu-sub">

                                <li class="menu-item active open">

                                    <ul class="menu-sub">
                                        <li >
                                            <a href="manageproduct" class="menu-link">
                                                <div data-i18n="Product list">Product list</div>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="addproduct" class="menu-link">
                                                <div>Add Product</div>
                                            </a>
                                        </li>

                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </aside>
                <div class="layout-page">
                    <!-- Content wrapper -->
                    <div class="content-wrapper">
                        <div class="container-xxl flex-grow-1 container-p-y">
                            <h4 class="py-3 mb-4">
                                <span class="text-muted fw-light"></span> Product List
                            </h4>
                            <!-- Product List Widget -->
                            <div class="card mb-4">
                                <div class="card-widget-separator-wrapper">
                                    <div class="card-body card-widget-separator">
                                        <div class="row gy-4 gy-sm-1">
                                            <div class="col-sm-6 col-lg-3">
                                                <div class="d-flex justify-content-between align-items-start card-widget-1 border-end pb-3 pb-sm-0">
                                                    <div>
                                                        <h6 class="mb-2">In-store Sales</h6>
                                                        <h4 class="mb-2">$5,345.43</h4>
                                                        <p class="mb-0"><span class="text-muted me-2">5k orders</span><span class="badge bg-label-success">+5.7%</span></p>
                                                    </div>
                                                    <span class="avatar me-sm-4">
                                                        <span class="avatar-initial bg-label-secondary rounded"><i class="ti-md ti ti-smart-home text-body"></i></span>
                                                    </span>
                                                </div>
                                                <hr class="d-none d-sm-block d-lg-none me-4">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Product List Table -->
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-title mb-0">Filter</h5>
                                    <div class="d-flex justify-content-between align-items-center row py-3 gap-3 gap-md-0">
                                        <div class="col-md-6 product_status"><select id="ProductStatus" class="form-select text-capitalize">
                                                <option value="">Status</option>
                                                <option value="Publish">Publish</option>
                                                <option value="Inactive">Inactive</option></select>
                                        </div>
                                       
                                            <div class="col-md-6 product_category">
                                                <select name="Caid" id="ProductCategory" class="form-select text-capitalize" onchange="document.getElementById('cateForm').submit();">
                                                    <c:forEach items="${listc}" var="c">
                                                        <option value="${c.caid}">${c.caname}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                    </div>
                                </div>

                                <form action="manageproduct" method="post">
                                    <c:if test="${notfound != null}">
                                        <div class="alert alert-warning" role="alert">
                                            <a style="color: red">${notfound}</a> 
                                        </div>
                                    </c:if>

                                    <div class="card-datatable table-responsive">

                                        <div class="dataTables_wrapper dt-bootstrap5 no-footer">

                                            <div class="card-header d-flex border-top rounded-0 flex-wrap py-2">

                                                <div class="me-5 ms-n2 pe-5">
                                                    <div>
                                                        <label>
                                                            <input type="text" name="pname" class="form-control" placeholder="Search Product">
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>

                                            <table id="productTable"  class="datatables-products table dataTable no-footer dtr-column"style="width: 1396px;">

                                                <thead>

                                                    <tr>
                                                        <th onclick="sortTable(0)">Product</th>
                                                        <th onclick="sortTable(1)">Price</th>
                                                        <th onclick="sortTable(2)">Quantity</th>
                                                        <th onclick="sortTable(3)">Status</th>
                                                        <th>Actions</th>
                                                    </tr>

                                                </thead>
                                                <c:forEach items="${pageProduct}" var="pp">


                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <div class="d-flex justify-content-start align-items-center product-name">
                                                                    <div class="avatar-wrapper">
                                                                        <div class="avatar avatar me-2 rounded-2 bg-label-secondary"><img src="images/${pp.img}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="d-flex flex-column">
                                                                        <h6 class="text-body text-nowrap mb-0"><span>${pp.pname}</span></h6>
                                                                    </div>
                                                                </div>
                                                            </td>


                                                            <td>${pp.price}</td>
                                                            <td>${pp.quantity}</td>
                                                            <td>
                                                                <c:if test="${pp.status  ==  1}">
                                                                    <span class="badge bg-label-success" text-capitalized="">Publish</span>
                                                                </c:if>
                                                                <c:if test="${pp.status  ==  0}">
                                                                    <span class="badge bg-label-danger" text-capitalized="">Inactive</span>
                                                                </c:if>
                                                            </td>
                                                            <td>
                                                            <div>

                                                                    <a href="edit?Pid=${pp.pid}" class="btn btn-sm btn-icon">
                                                                        <i class="ti ti-edit"></i>
                                                                    </a>

                                                                    <a href="delete?Pid=${pp.pid}" class="btn btn-sm btn-icon delete-record"><i class="ti ti-trash"></i></a>
                                                                    <a href="productdetail?Pid=${pp.pid}" class="btn btn-sm btn-icon dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="ti ti-dots-vertical me-2"></i></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </c:forEach>
                                            </table>




                                            <div class="pagination-container">
                                                <c:if test="${indexPage > 1}">
                                                    <a class="pagination-link" href="manageproduct?index=${indexPage - 1}">Previous</a>
                                                </c:if>

                                                <c:forEach begin="1" end="${endPage}" var="i" varStatus="loop">
                                                    <c:set var="isActive" value="${i == currentPage ? 'current' : ''}" />
                                                    <a class="pagination-link ${isActive}" href="manageproduct?index=${i}">${i}</a>
                                                </c:forEach>

                                                <c:if test="${indexPage < endPage}">
                                                    <a class="pagination-link" href="manageproduct?index=${indexPage + 1}">Next</a>
                                                </c:if>
                                            </div>

                                            <style>
                                                .pagination-container {
                                                    text-align: right; /* Căn giữa dòng phân trang */
                                                    padding-right: 100px
                                                }
                                                .pagination-link {
                                                    display: inline-block;
                                                    margin: 10px;
                                                    text-align: left;
                                                }

                                                .pagination-link a {
                                                    color: #007BFF; /* Màu chữ cho các liên kết */
                                                    padding: 8px 16px;
                                                    text-decoration: none;
                                                    background-color: #fff; /* Màu nền cho các liên kết */
                                                    border: 1px solid #007BFF; /* Viền cho các liên kết */
                                                    margin-right: 5px; /* Khoảng cách giữa các liên kết */
                                                    border-radius: 5px; /* Bo góc cho các liên kết */
                                                    transition: background-color 0.3s, color 0.3s;
                                                }

                                                .pagination-link a:hover {
                                                    background-color: #007BFF; /* Màu nền khi di chuột qua */
                                                    color: #fff; /* Màu chữ khi di chuột qua */
                                                }

                                                .pagination-link .current {
                                                    background-color: #007BFF; /* Màu nền cho trang hiện tại */
                                                    color: #fff; /* Màu chữ cho trang hiện tại */
                                                    padding: 8px 16px;
                                                }
                                            </style>


                                        </div>
                                    </div>



                                </form>
                                <script>
                                    let sortPriceAsc = true;
                                    let sortQtyAsc = true;
                                    let sortNameAsc = true;
                                    let sortStatusAsc = true;

                                    function sortTable(col) {
                                        if (col == 1) {
                                            sortPriceAsc = !sortPriceAsc;
                                        }
                                        if (col == 2) {
                                            sortQtyAsc = !sortQtyAsc;
                                        }
                                        if (col == 0) {
                                            sortNameAsc = !sortNameAsc;
                                        }
                                        if (col == 3) {
                                            sortStatusAsc = !sortStatusAsc;
                                        }

                                        var table, rows, switching, i, x, y, shouldSwitch;
                                        table = document.getElementById("productTable");
                                        switching = true;
                                        while (switching) {
                                            switching = false;
                                            rows = table.rows;
                                            for (i = 1; i < (rows.length - 1); i++) {
                                                shouldSwitch = false;
                                                x = rows[i].getElementsByTagName("TD")[col];
                                                y = rows[i + 1].getElementsByTagName("TD")[col];

                                                if (col == 1) {
                                                    xPrice = Number(x.innerHTML);
                                                    yPrice = Number(y.innerHTML);
                                                    if (sortPriceAsc) {
                                                        if (xPrice > yPrice) {
                                                            shouldSwitch = true;
                                                            break;
                                                        }
                                                    } else {
                                                        if (xPrice < yPrice) {
                                                            shouldSwitch = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                if (col == 0) {
                                                    let xName = x.getElementsByTagName("SPAN")[0].innerText.toLowerCase();
                                                    let yName = y.getElementsByTagName("SPAN")[0].innerText.toLowerCase();
                                                    if (sortNameAsc) {
                                                        if (xName > yName) {
                                                            shouldSwitch = true;
                                                            break;
                                                        }
                                                    } else {
                                                        if (xName < yName) {
                                                            shouldSwitch = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                if (col == 2) {
                                                    xQty = Number(x.innerHTML);
                                                    yQty = Number(y.innerHTML);
                                                    if (sortQtyAsc) {
                                                        if (xQty > yQty) {
                                                            shouldSwitch = true;
                                                            break;
                                                        }
                                                    } else {
                                                        if (xQty < yQty) {
                                                            shouldSwitch = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                if (col == 3) {
                                                    let xStatus = x.getElementsByTagName("SPAN")[0].innerText.toLowerCase();
                                                    let yStatus = y.getElementsByTagName("SPAN")[0].innerText.toLowerCase();
                                                    if (sortStatusAsc) {
                                                        if (xStatus > yStatus) {
                                                            shouldSwitch = true;
                                                            break;
                                                        }
                                                    } else {
                                                        if (xStatus < yStatus) {
                                                            shouldSwitch = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }

                                            if (shouldSwitch) {
                                                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                                                switching = true;
                                            }
                                        }
                                    }
                                </script>

                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

</html>
