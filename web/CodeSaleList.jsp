<%-- 
    Document   : HomeAdmin
    Created on : Sep 27, 2023, 9:09:14 PM
    Author     : FPT
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title>Board Game</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!----css3---->
        <link rel="stylesheet" href="css/custom.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

        <!--google fonts -->

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">


        <!--google material icon-->
        <link href="https://fonts.googleapis.com/css2?family=Material+Icons"rel="stylesheet">

    </head>
    <body>


        <div class="wrapper">


            <div class="body-overlay"></div>

            <!-------------------------sidebar------------>
            <!-- Sidebar  -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <h3><img src="images/favicon.png" class="img-fluid"/><span>Board Game</span></h3>
                </div>
                <ul class="list-unstyled components">
                    <li  class="">
                        <a href="mktdashboard" class="dashboard"><i class="material-icons">dashboard</i>
                            <span>MKT Dashboard</span></a>
                    </li>


                    <li class="">
                        <a href="customerList" >
                            <i class="fas fa-user"></i>Quản Lý Khách Hàng</a>
                    </li>

                    <li class="">
                        <a href="postlist" >
                            <i class="fas fa-newspaper"></i><span>Quản Lý Tin Tức</span></a>
                    </li>

                    

                    <li class="">
                        <a href="bannerlist">
                            <i class="fas fa-list"></i><span>Quản Lý Banner</span></a>
                    </li>
                    <li class="active">
                        <a href="codeSaleList">
                            <i class="fas fa-list"></i><span>Quản Lý Mã Giảm giá</span></a>
                    </li>


                    <li  class="">
                        <a href="home"><i class="fas fa-home"></i><span>Trở lại trang chính
                            </span></a>
                    </li>

                </ul>


            </nav>




            <!--------page-content---------------->

            <div id="content">

                <!--top--navbar----design--------->

                <div class="top-navbar">
                    <div class="xp-topbar">

                        <!-- Start XP Row -->
                        <div class="row"> 
                            <!-- Start XP Col -->
                            <div class="col-2 col-md-1 col-lg-1 order-2 order-md-1 align-self-center">
                                <div class="xp-menubar">
                                    <span class="material-icons text-white">signal_cellular_alt
                                    </span>
                                </div>
                            </div> 
                            <!-- End XP Col -->

                            <!-- Start XP Col -->
                            <div class="col-md-5 col-lg-3 order-3 order-md-2">
                                <div class="xp-searchbar">
                                    <form>
                                        <div class="input-group">
                                            <input type="search" class="form-control" 
                                                   placeholder="Search">
                                            <div class="input-group-append">
                                                <button class="btn" type="submit" 
                                                        id="button-addon2">GO</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- End XP Col -->

                            <!-- Start XP Col -->
                            <div class="col-10 col-md-6 col-lg-8 order-1 order-md-3">
                                <div class="xp-profilebar text-right">
                                    <nav class="navbar p-0">
                                        <ul class="nav navbar-nav flex-row ml-auto">   
                                            <li class="dropdown nav-item active">
                                                <a href="#" class="nav-link" data-toggle="dropdown">
                                                    <span class="material-icons">notifications</span>
                                                    <span class="notification">4</span>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="#">You have 5 new messages</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">You're now friend with Mike</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Wish Mary on her birthday!</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">5 warnings in Server Console</a>
                                                    </li>

                                                </ul>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">
                                                    <span class="material-icons">question_answer</span>

                                                </a>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link" href="#" data-toggle="dropdown">
                                                    <img src="img/user.jpg" style="width:40px; border-radius:50%;"/>
                                                    <span class="xp-user-live"></span>
                                                </a>
                                                <ul class="dropdown-menu small-menu">
                                                    <li>
                                                        <a href="#">
                                                            <span class="material-icons">
                                                                person_outline
                                                            </span>Profile

                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><span class="material-icons">
                                                                settings
                                                            </span>Settings</a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><span class="material-icons">
                                                                logout</span>Logout</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>


                                    </nav>

                                </div>
                            </div>
                            <!-- End XP Col -->

                        </div> 
                        <!-- End XP Row -->

                    </div>
                    <div class="xp-breadcrumbbar text-center">
                        <h4 class="page-title">Dashboard</h4>  
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Booster</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                        </ol>                
                    </div>

                </div>



                <!--------main-content------------->

                <div class="main-content">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="table-wrapper">
                                <div class="table-title">
                                    <div class="row">
                                        <div class="col-sm-6 p-0 d-flex justify-content-lg-start justify-content-center">
                                            <h2 class="ml-lg-2">Mã Giảm Giá</h2>
                                        </div>
                                        <div class="col-sm-6 p-0 d-flex justify-content-lg-end justify-content-center">
                                            <a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal">
                                                <i class="material-icons">&#xE147;</i> <span>Thêm Mã Mới</span></a>

                                        </div>
                                    </div>
                                </div>
                                <table class="table table-striped table-hover">

                                    <tr>
                                        <!--          <th>
                                                    <span class="custom-checkbox">
                                                                <input type="checkbox" id="selectAll">
                                                                <label for="selectAll"></label>
                                                    </span>
                                                </th>-->
                                        <th>Id</th>
                                        <th>Mã Sale</th>
                                        <th>Giảm giá</th>
                                        <th>Trạng thái</th>
                                        <th>Ngày bắt đầu</th>
                                        <th>Ngày kết thúc</th>
                                        <th>Số lượng</th>
                                        <th>Điều kiện</th>
                                        <th>Tiêu đề</th>
                                        <th>Số lượng đã sử dụng</th>
                                        <th></th>
                                    </tr>
                                    <c:forEach items="${requestScope.acc}" var="c" begin="${param.page*8-8}" end="${param.page*8-1}"> 
                                        <tr>
                                            <td>${c.csid}</td>
                                            <td style="color: #ff6633; font-size:15px;font-weight:bolder;padding:10px;">${c.codeSale}</td>
                                            <td>${c.discount}</td>
                                            <td><c:if test="${c.csStatus == 1}">
                                                    <span style="color: #3333ff;"> Hoạt động</span>
                                                </c:if>
                                                <c:if test="${c.csStatus == 0}">
                                                    <span style="color: red;">Không hoạt động</span>
                                                </c:if></td>
                                            <td>${c.dateStart}</td>
                                            <td>${c.dateEnd}</td>
                                            <td>${c.limitedQuantity}</td>
                                            <td>${c.discountConditions}</td>
                                            <td>${c.titile}</td>
                                            <td>${c.quantityUsed}</td>

                                            <td>
                                                <a href="updateCodeSale?sid=${c.csid}" >
                                                    <i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                                <a href="javascript:void(0);" onclick="confirmDelete(${c.csid})">
                                                    <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                            </td>

                                        </tr>
                                    </c:forEach  >

                                    <script type="text/javascript">
                                        function confirmDelete(csid) {
                                            var confirmResult = confirm("Bạn có chắc chắn muốn xóa?");
                                            if (confirmResult) {
                                                // Nếu người dùng đồng ý xóa, chuyển hướng đến trang xóa
                                                window.location.href = "deleteCodeSale?sid=" + csid;
                                            }
                                        }
                                    </script>


                                </table>
                                <div class="clearfix">
                                    <div class="hint-text">Showing ${fn:length(requestScope.acc)} CodeSale</div>
                                    <% int i=1; %>

                                    <ul class="pagination">
                                        <c:if test="${param.page>1}">
                                            <li class="page-item "><a href="codeSaleList?page=${param.page-1}&sort=${param.sort}">Previous</a></li>
                                            </c:if>

                                        <c:forEach items="${requestScope.acc}" var="o" step="8" >
                                            <c:set var="i" value="<%=i%>"/>
                                            <li<c:if test="${param.page eq i}"> class="page-item active"</c:if>>
                                                <a href="codeSaleList?page=<%=i%>&sort=${param.sort}" class="page-link"><%=i%></a></li> 
                                                <% i++;%>
                                            </c:forEach>
                                            <c:if test="${fn:length(requestScope.acc)>=(param.page*8+1)}">
                                            <li class="page-item"><a href="codeSaleList?page=${param.page+1}&sort=${param.sort}" class="page-link">Next</a></li>
                                            </c:if>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Edit Modal HTML -->
                        <div id="addEmployeeModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form action="addCodeSale" method="post" id="codeSaleForm">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Thêm Mã Mới</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        </div>
                                        <div>
                                            <h6 id="errorMessage" style="color: red; font-size: 15px; font-weight: bold; text-align: center; padding: 10px;"></h6>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="codeSale">Mã Sale</label>
                                                <input id="codeSale" type="text" name="codeSale" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="discount">Giảm giá</label>
                                                <input id="discount" type="text" name="discount" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="dateStart">Ngày bắt đầu</label>
                                                <input id="dateStart" type="date" name="dateStart" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="dateEnd">Ngày kết thúc</label>
                                                <input id="dateEnd" type="date" name="dateEnd" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="limitedQuantity">Số lượng</label>
                                                <input id="limitedQuantity" type="text" name="limitedQuantity" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="discountConditions">Điều kiện</label>
                                                <input id="discountConditions" type="text" name="discountConditions" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="titile">Tiêu đề</label>
                                                <input id="titile" type="text" name="titile" class="form-control">
                                            </div>
                                        </div>
                                        <div class="modal-footer">

                                            <button type="button" class="btn btn-default" data-dismiss="modal">Quay Lại</button>
                                            <button type="submit" class="btn btn-success">Thêm</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            document.addEventListener("DOMContentLoaded", function () {
                                document.getElementById("codeSaleForm").addEventListener("submit", function (event) {
                                    var codeSale = document.getElementById("codeSale").value;
                                    var discount = document.getElementById("discount").value;
                                    var dateStart = document.getElementById("dateStart").value;
                                    var dateEnd = document.getElementById("dateEnd").value;
                                    var limitedQuantity = document.getElementById("limitedQuantity").value;
                                    var discountConditions = document.getElementById("discountConditions").value;
                                    var titile = document.getElementById("titile").value;

                                    var errorMessage = document.getElementById("errorMessage");

                                    // Kiểm tra điều kiện validation ở đây
                                    if (codeSale.trim() === "" || discount.trim() === "" || dateStart.trim() === "" || dateEnd.trim() === "" || limitedQuantity.trim() === "" || discountConditions.trim() === "" || titile.trim() === "") {
                                        errorMessage.textContent = "Vui lòng điền đầy đủ thông tin.";
                                        event.preventDefault();
                                        return;
                                    }

                                    if (isNaN(discount) || discount <= 0) {
                                        errorMessage.textContent = "Giảm Giá phải là một số dương.";
                                        event.preventDefault();
                                        return;
                                    }

                                    if (!isValidDate(dateStart) || !isValidDate(dateEnd)) {
                                        errorMessage.textContent = "Ngày phải có định dạng ngày tháng hợp lệ (VD: YYYY-MM-DD).";
                                        event.preventDefault();
                                        return;
                                    }

                                    if (!isPositiveInteger(limitedQuantity) || !isPositiveInteger(discountConditions) || limitedQuantity <= 0) {
                                        errorMessage.textContent = "Số lượng phải là số nguyên dương.";
                                        event.preventDefault();
                                        return;
                                    }

                                    if (!isPositiveInteger(limitedQuantity) || !isPositiveInteger(discountConditions)) {
                                        errorMessage.textContent = "Các trường phải là số nguyên dương.";
                                        event.preventDefault();
                                        return;
                                    }

                                    if (new Date(dateStart) > new Date(dateEnd)) {
                                        errorMessage.textContent = "Ngày bắt đầu phải nhỏ hoặc bằng hơn ngày kết thúc.";
                                        event.preventDefault();
                                        return;
                                    }

                                    if (codeSale.length < 3 ) {
                                        errorMessage.textContent = "Mã giảm giá phải có từ 3 ký tự trở lên.";
                                        event.preventDefault();
                                        return;
                                    }

                                    // Nếu không có lỗi, xóa thông báo lỗi
                                    errorMessage.textContent = "";
                                });
                            });

                            // Hàm kiểm tra định dạng ngày hợp lệ
                            function isValidDate(date) {
                                var pattern = /^\d{4}-\d{2}-\d{2}$/;
                                return pattern.test(date);
                            }

                            // Hàm kiểm tra số nguyên dương
                            function isPositiveInteger(value) {
                                var pattern = /^\d+$/;
                                return pattern.test(value);
                            }
                        </script>




                        <!-- Delete Modal HTML -->
                        <div id="deleteEmployeeModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form>
                                        <div class="modal-header">
                                            <h4 class="modal-title">Delete Employee</h4>
                                            <button type="button" class="close" data-dismiss="modal" 
                                                    aria-hidden="true">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <p>Are you sure you want to delete these Records?</p>
                                            <p class="text-warning"><small>This action cannot be undone.</small></p>
                                        </div>
                                        <div class="modal-footer">
                                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                                            <input type="submit" class="btn btn-danger" value="Delete">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>


                    <!---footer---->


                </div>

                <footer class="footer">
                    <div class="container-fluid">
                        <div class="footer-in">
                            <p class="mb-0">&copy 2020 Vishweb design - All Rights Reserved.</p>
                        </div>
                    </div>
                </footer>
            </div>
        </div>


        <!----------html code compleate----------->









        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery-3.3.1.slim.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery-3.3.1.min.js"></script>


        <script type="text/javascript">

                            $(document).ready(function () {
                                $(".xp-menubar").on('click', function () {
                                    $('#sidebar').toggleClass('active');
                                    $('#content').toggleClass('active');
                                });

                                $(".xp-menubar,.body-overlay").on('click', function () {
                                    $('#sidebar,.body-overlay').toggleClass('show-nav');
                                });

                            });

        </script>





    </body>

</html>


