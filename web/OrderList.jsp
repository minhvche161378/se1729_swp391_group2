<!doctype html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title>Order List</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!----css3---->
        <link rel="stylesheet" href="css/custom.css">


        <!--google fonts -->

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">


        <!--google material icon-->
        <link href="https://fonts.googleapis.com/css2?family=Material+Icons"rel="stylesheet">
        <style>
            td {

                word-wrap: break-word; /* nếu nội dung quá dài, nó sẽ tự động xuống dòng */
            }
            h6 {
                margin-bottom: 10px;
                ; /* Điều chỉnh khoảng cách dưới của thẻ h6 */
            }

            p {
                margin-top: 5px; /* Điều chỉnh khoảng cách trên của thẻ p */
                margin-bottom:  5px;
            }
            .search-form {
                display: flex;
                flex-direction: column;
                align-items: flex-start;
                margin-top: 10px;
                padding: 10px;

            }

            .search-status-container {
                display: flex;
                align-items: center;
                margin-bottom: 10px;
            }

            .search-container, .status-container, .date-container {
                display: flex;
                align-items: center;
                margin-bottom: 10px;
            }

            .search-label, .date-label, .status-label {
                margin-right: 10px;
                font-size: 14px; /* Chỉnh kích thước chữ ở đây */
            }

            input[type="text"], input[type="date"], select {
                width: 100%;
                box-sizing: border-box;
                padding: 8px;
                border: 1px solid #ccc;
                border-radius: 5px;
                font-size: 14px; /* Chỉnh kích thước chữ ở đây */
            }

            input[type="date"] {
                width: 150px; /* Điều chỉnh chiều rộng của input date ở đây */
                margin-right: 10px; /* Khoảng cách giữa hai ô input date */
            }

            .status-label {
                width: 150px; /* Điều chỉnh chiều rộng của label tình trạng đơn hàng */
            }

            #status {
                flex: 1; /* Để select chiếm phần còn lại của dòng */
            }

            .small-button {
                width: 8%;
                padding: 8px;
                border: none;
                background-color: #4CAF50;
                color: white;
                border-radius: 5px;
                cursor: pointer;
                font-size: 10px; /* Chỉnh kích thước chữ ở đây */
            }

            .small-button:hover {
                background-color: #45a049;
            }



        </style>
    </head>
    <body>


        <div class="wrapper">


            <div class="body-overlay"></div>

            <!-------------------------sidebar------------>
            <!-- Sidebar  -->
            <nav id="sidebar">
               <div class="sidebar-header">
                <a href="home"><h3><img src="images/favicon.png" class="img-fluid"/><span>Board Game</span></h3></a>
            </div>
            <ul class="list-unstyled components">

                <li  >
                    <a href="saledashboard" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Thống kê</span></a>
                </li>
                 <li class="active">
                    <a href="orderList" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Xử lí đơn hàng</span></a>
                </li>
                <li>
                    <a href="manageproduct" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Quản lí sản phẩm</span></a>
                </li>

                <li>
                    <a href="addproduct" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Thêm sản phẩm mới</span></a>
                </li>
                  <li>
                    <a href="home" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Về trang chủ</span></a>
                </li>





            </ul>


            </nav>




            <!--------page-content---------------->

            <div id="content">

                <!--top--navbar----design--------->

                <div class="top-navbar">
                    <div class="xp-topbar">

                        <!-- Start XP Row -->
                        <div class="row"> 
                            <!-- Start XP Col -->
                            <div class="col-2 col-md-1 col-lg-1 order-2 order-md-1 align-self-center">
                                <div class="xp-menubar">
                                    <span class="material-icons text-white">signal_cellular_alt
                                    </span>
                                </div>
                            </div> 
                            <!-- End XP Col -->

                            <!-- Start XP Col -->
                            <div class="col-md-5 col-lg-3 order-3 order-md-2">
                              
                            </div>
                            <!-- End XP Col -->

                            <!-- Start XP Col -->
                            <div class="col-10 col-md-6 col-lg-8 order-1 order-md-3">
                                
                            </div>
                            <!-- End XP Col -->

                        </div> 
                        <!-- End XP Row -->

                    </div>
                    <div class="xp-breadcrumbbar text-center">
                        <h4 class="page-title">Danh sách đơn hàng</h4>  
                        <ol class="breadcrumb">
                          
                        </ol>                
                    </div>

                </div>



                <!--------main-content------------->

                <div class="main-content">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="table-wrapper">
                                <div>
                                    <form action="orderList"  class="search-form">
                                        <div class="search-status-container">
                                            <div class="search-container">
                                                <label for="search" class="search-label">Tìm kiếm</label>
                                                <input type="text" name="name" id="search" placeholder="Nhập tên hoặc địa chỉ" value="${name}">
                                            </div>
                                            <div class="status-container" style="margin-left: 20px;">
                                                <label for="status" class="status-label">Tình trạng đơn hàng</label>
                                                <select name="status" id="status">
                                                    <option value="">Tất cả</option>
                                                    <option value="0" <c:if test="${status==0}">selected=""</c:if>>Chờ xác nhận</option>
                                                    <option value="1"<c:if test="${status==1}">selected=""</c:if>>Đang xử lí</option>
                                                    <option value="2"<c:if test="${status==2}">selected=""</c:if>>Hoàn tất</option>
                                                    <option value="3"<c:if test="${status==3}">selected=""</c:if>>Đơn hàng bị hủy</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="date-container">
                                                <label for="dateFrom" class="date-label">Ngày mua: Từ</label>
                                                <input type="date" name="dateFrom" id="dateFrom" value="${dateFrom}">
                                            <label for="dateTo" class="date-label">Đến</label>
                                            <input type="date" name="dateTo" id="dateTo" value="${dateTo}">
                                        </div>
                                        <input class="small-button" type="submit" value="Tìm kiếm">
                                    </form>


                                </div>

                                <div class="table-title">
                                    <div class="row">
                                        <div class="col-sm-6 p-0 d-flex justify-content-lg-start justify-content-center">
                                            <h2 class="ml-lg-2">Danh sách đơn hàng</h2>
                                        </div>
                                        <div class="col-sm-6 p-0 d-flex justify-content-lg-end justify-content-center">
                                            <c:if test="${status==0}">
                                                <a href="#" class="btn btn-success" onclick="confirmOrder('confirm', '${page}', '${name}', '${status}', '${dateFrom}', '${dateTo}')">
                                                    <i class="material-icons"></i> <span>Xác nhận đơn hàng</span>
                                                </a>
                                                <a href="#" class="btn btn-danger" onclick="confirmCancellation('cancel', '${page}', '${name}', '${status}', '${dateFrom}', '${dateTo}')">
                                                    <i class="material-icons"></i> <span>Hủy đơn hàng</span>
                                                </a>
                                            </c:if>
                                            <c:if test="${status==1}">
                                                <a href="#" class="btn btn-success" onclick="completeOrder('complete', '${page}', '${name}', '${status}', '${dateFrom}', '${dateTo}')">
                                                    <i class="material-icons"></i> <span>Hoàn tất đơn hàng</span></a>

                                            </c:if>

                                        </div>

                                    </div>
                                </div>
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th> <span class="custom-checkbox">
                                                    <input type="checkbox" id="selectAll" onclick="selectAllCheckboxes()">
                                                    <label for="selectAll"></label>
                                                </span>
                                            </th>
                                            <th>Mã đơn hàng</th>
                                            <th>Ngày mua</th>  
                                            <th>Email</th> 
                                            <th>Người nhận</th>
                                            <th>Trạng thái</th>
                                            <th>Tổng tiền</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                        <c:set var="count" value="1" />
                                        <c:forEach items="${orderList}" var="o" >

                                            <tr>

                                                <td>  <span class="custom-checkbox">
                                                        <input type="checkbox" id="checkbox${count}" name="options[]" value="${o.oid}">
                                                        <label for="checkbox${count}"></label>

                                                    </span></td>

                                                <td>  <a href="orderDetails?oid=${o.oid}" title="Chi tiết đơn hàng">  #${o.oid} </a></td> 
                                                <td>${o.ordered_at}</td>
                                                <td>
                                                    <form id="myForm${o.oid}" action="customerDetail_2" method="post">
                <input type="hidden" name="id" value="${ad.getAccountByoid(o.oid).acid}">
            </form>
            <a href="#" onclick="document.getElementById('myForm${o.oid}').submit(); return false;">
                ${ad.getAccountByoid(o.oid).email}
            </a>
                                                   
                                                <td >${o.receiver}</td>

                                                <td> 
                                                    <c:if test="${o.status==0}"><b style="color:#FFA500;"> Chờ xác nhận</b></c:if> 
                                                    <c:if test="${o.status==1}"> <span style="color:#2E8B57;"> Đang xử lí</span></c:if> 
                                                    <c:if test="${o.status==2}"> <span style="color:#2E8B57;"> Hoàn tất</span></c:if> 
                                                    <c:if test="${o.status==3}"><span style="color:red;"> Đơn hàng bị hủy</span></c:if> 

                                                    </td>
                                                    <td>
                                                    <fmt:formatNumber value="${o.totalAmount-o.discount}" pattern="#,##0" var="totalAmount" />
                                                    ${totalAmount}</td>
                                                <td>
                                                    <c:if test="${o.status==0}"> <a href="#Modal"  data-toggle="modal" onclick="processOrder('${o.oid}')">
                                                            <i class="material-icons" data-toggle="tooltip" title="Xử lí đơn hàng">&#xE254;</i></a>
                                                        </c:if> 
                                                     <c:if test="${o.status==1}"> <a href="#completeModal"  data-toggle="modal" onclick="processOrder2('${o.oid}')">
                                                            <i class="material-icons" data-toggle="tooltip" title="Xử lí đơn hàng">&#xE254;</i></a>
                                                        </c:if> 


                                                </td>
                                            </tr>
                                        </c:forEach>



                                    </tbody>
                                </table>
                                <div class="clearfix">

                                    <ul class="pagination">
                                        <c:if test="${page != null && page ne '1'}">
                                            <li class="page-item disabled">
                                                <a href="orderList?page=${page-1}&name=${name}&status=${status}&dateFrom=${dateFrom}&dateTo=${dateTo}">Previous</a>
                                            </li>
                                        </c:if>

                                       <c:choose>
    <c:when test="${page eq null}">
        <c:set var="page" value="1" />
    </c:when>
</c:choose>

<c:forEach begin="1" end="${numberPage}" var="i">
    <c:choose>
        <c:when test="${(i ge page-1 || page eq null) && i le page+1}">
            <c:choose>
                <c:when test="${page eq i}">
                    <li class="page-item active">
                        <a href="orderList?page=${i}&name=${name}&status=${status}&dateFrom=${dateFrom}&dateTo=${dateTo}" class="page-link">${i}</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li class="page-item">
                        <a href="orderList?page=${i}&name=${name}&status=${status}&dateFrom=${dateFrom}&dateTo=${dateTo}" class="page-link">${i}</a>
                    </li>
                </c:otherwise>
            </c:choose>
        </c:when>
    </c:choose>
</c:forEach>
                   <c:if test="${page != null && page lt numberPage}">
                                            <li class="page-item disabled">
                                                <a href="orderList?page=${page+1}&name=${name}&status=${status}&dateFrom=${dateFrom}&dateTo=${dateTo}">Next</a>
                                            </li>
                                        </c:if>





                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="Modal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form  action="orderprocess" method="post">
                                        <input type="hidden" name="page" value="${page}">
                                        <input type="hidden" name="name" value="${name}">
                                        <input type="hidden" name="status" value="${status}">
                                        <input type="hidden" name="dateFrom" value="${dateFrom}">
                                        <input type="hidden" name="dateTo" value="${dateTo}">
                                        <input type="hidden" id="orderIdInput" name="oid">
                                       
                                        <input type="hidden" id="typeInput" name="type">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Xử lí đơn hàng</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        </div>

                                        <div class="modal-body">
                                            <div class="form-group">

                                                Bạn có muốn xác nhận hoặc hủy đơn hàng?



                                            </div>

                                        </div>
                                        <div class="modal-footer"> 
                                            <button  type="submit" class="btn btn-success"  onclick="setType('confirm')">Xác nhận đơn hàng</button>
                                            <button  type="submit" class="btn btn-danger"  onclick="setType('cancel')">Hủy đơn hàng</button>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                          <div id="completeModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form  action="orderprocess" method="post">
                                        <input type="hidden" name="page" value="${page}">
                                        <input type="hidden" name="name" value="${name}">
                                        <input type="hidden" name="status" value="${status}">
                                        <input type="hidden" name="dateFrom" value="${dateFrom}">
                                        <input type="hidden" name="dateTo" value="${dateTo}">
                                        <input type="hidden" id="orderIdInput2" name="oid">
                                       
                                        <input type="hidden" id="typeInput" name="type">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Xử lí đơn hàng</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        </div>

                                        <div class="modal-body">
                                            <div class="form-group">

                                                Bạn có muốn hoàn tất đơn hàng?



                                            </div>

                                        </div>
                                        <div class="modal-footer"> 
                                            <button  type="submit" class="btn btn-success"  onclick="setType('confirm')">Xác nhận hoàn tất</button>
                                            

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>






                    </div>


                    <!---footer---->


                </div>

                <footer class="footer">
                    <div class="container-fluid">
                        <div class="footer-in">
                            <p class="mb-0">&copy 2020 Vishweb design - All Rights Reserved.</p>
                        </div>
                    </div>
                </footer>
            </div>
        </div>


        <!----------html code compleate----------->









        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery-3.3.1.slim.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="js/cart.js"></script>  
        <script type="text/javascript">
                                                 function setType(type) {
                                                     document.getElementById('typeInput').value = type;
                                                 }
        </script>
        <script>
            function processOrder(orderId) {
                document.getElementById('orderIdInput').value = orderId;
               
            }
        </script>
         <script>
            function processOrder2(orderId) {
                document.getElementById('orderIdInput2').value = orderId;
        

            }
        </script>
        <script type="text/javascript">
            function confirmOrder(type, page, name, status, dateFrom, dateTo) {
                var confirmed = confirm("Bạn có muốn xác nhận đơn hàng không?");
                if (confirmed) {
                    sendData(type, page, name, status, dateFrom, dateTo);
                }

            }
            function completeOrder(type, page, name, status, dateFrom, dateTo) {
                var confirmed = confirm("Bạn có muốn hoàn tất đơn hàng không?");
                if (confirmed) {
                    sendData(type, page, name, status, dateFrom, dateTo);
                }
            }
            function confirmCancellation(type, page, name, status, dateFrom, dateTo) {
                var confirmed = confirm("Bạn có muốn hủy không?");
                if (confirmed) {
                    sendData(type, page, name, status, dateFrom, dateTo);
                }
            }
            function sendData(type, page, name, status, dateFrom, dateTo) {
                var checkboxes = document.querySelectorAll('input[name="options[]"]:checked');
                var values = Array.from(checkboxes).map(function (checkbox) {
                    return checkbox.value;
                });

                var form = document.createElement('form');
                form.method = 'GET';
                form.action = 'orderprocess';

                values.forEach(function (value) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = 'selectedItems';
                    input.value = value;
                    form.appendChild(input);
                });

                // Adding the type parameter
                if (type) {
                    var typeInput = document.createElement('input');
                    typeInput.type = 'hidden';
                    typeInput.name = 'type';
                    typeInput.value = type;
                    form.appendChild(typeInput);
                }

                // Additional parameters
                var pageInput = document.createElement('input');
                pageInput.type = 'hidden';
                pageInput.name = 'page';
                pageInput.value = page;
                form.appendChild(pageInput);

                var nameInput = document.createElement('input');
                nameInput.type = 'hidden';
                nameInput.name = 'name';
                nameInput.value = name;
                form.appendChild(nameInput);

                var statusInput = document.createElement('input');
                statusInput.type = 'hidden';
                statusInput.name = 'status';
                statusInput.value = status;
                form.appendChild(statusInput);

                var dateFromInput = document.createElement('input');
                dateFromInput.type = 'hidden';
                dateFromInput.name = 'dateFrom';
                dateFromInput.value = dateFrom;
                form.appendChild(dateFromInput);

                var dateToInput = document.createElement('input');
                dateToInput.type = 'hidden';
                dateToInput.name = 'dateTo';
                dateToInput.value = dateTo;
                form.appendChild(dateToInput);

                document.body.appendChild(form);
                form.submit();
            }
        </script>








    </body>

</html>


