<!DOCTYPE html>
<html lang="en">
    <style>
         #myMenu {
    display: none;
}

    </style> 
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Your Website</title>
</head>
<body>

    <!-- Your menu -->
    <nav id="myMenu">
        <!-- Your menu items go here -->
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Services</a></li>
            <li><a href="#">Contact</a></li>
        </ul>
    </nav>

    <!-- The rest of your content goes here -->

    <script >
        document.addEventListener("DOMContentLoaded", function() {
    // Check if the current page is the home page
    if (window.location.pathname === '/WebBoardGame/test.jsp' || window.location.pathname === '/index.html') {
        // If it is, show the menu
        document.getElementById('myMenu').style.display = 'block';
    }
});

    </script>
</body>
</html>
