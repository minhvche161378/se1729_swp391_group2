<%-- 
    Document   : feedbackDetail
    Created on : 19-10-2023, 04:54:24
    Author     : hihih
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Danh Sách Phản Hồi</title>
</head>
<body>
    <h1>Danh Sách Phản Hồi</h1>
    <table>
        <tr>
            <th>FeedbackID</th>
            <th>FeedbackBySellerName</th>
            <th>Create_atFeedback</th>
            <th>FeedbackContent</th>
            <th>AccountId</th>
            <th>CommentId</th>
            <th>StatusFeedback</th>
        </tr>
        <c:forEach items="${feedbackList}" var="feedback">
            <tr>
                <td>${feedback.feedbackID}</td>
                <td>${feedback.feedbackBySellerName}</td>
                <td>${feedback.create_atFeedback}</td>
                <td>${feedback.feedbackContent}</td>
                <td>${feedback.acountId}</td>
                <td>${feedback.commentId}</td>
                <td>${feedback.statusFeedback}</td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>
