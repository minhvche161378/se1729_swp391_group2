
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>MKT</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!----css3---->

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

        <link rel="stylesheet" href="css/post/custom.css">
        <link rel="stylesheet" href="./css/post/adminStyle.css">


        <!--google fonts -->

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">


        <!--google material icon-->
        <link href="https://fonts.googleapis.com/css2?family=Material+Icons"rel="stylesheet">
        <!-- Include the necessary Chart.js library -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="chartt/lib/chart/chart.min.js"></script>
        <script src="chartt/lib/easing/easing.min.js"></script>
        <script src="chartt/lib/waypoints/waypoints.min.js"></script>
        <script src="chartt/lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="chartt/lib/tempusdominus/js/moment.min.js"></script>
        <script src="chartt/lib/tempusdominus/js/moment-timezone.min.js"></script>
        <script src="chartt/lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js"></script>


    </head>

    <body>
        <nav id="sidebar">
            <div class="sidebar-header">
                <a href="home"><h3><img src="images/favicon.png" class="img-fluid"/><span>Board Game</span></h3></a>
            </div>
            <ul class="list-unstyled components">
                <li  class="active">
                    <a href="mktdashboard" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>MKT Dashboard</span></a>
                </li>


                <li class="">
                    <a href="customerList" >
                        <i class="fas fa-user"></i>Quản Lý Khách Hàng</a>


                </li>

                <li class="">
                    <a href="postlist" >
                        <i class="fas fa-newspaper"></i><span>Quản Lý Tin Tức</span></a>

                </li>

                <li class="">
                    <a href="bannerlist" >
                        <i class="fas fa-list"></i><span>Quản Lý Banner</span></a>
                </li>
<li class="">
                    <a href="codeSaleList" >
                        <i class="fas fa-list"></i><span>Quản Lý Mã Giảm Giá</span></a>
                </li>
                <li  class="">
                    <a href="home"><i class="fas fa-home"></i><span>Trở lại trang chính
                        </span></a>
                </li>


            </ul>


        </nav>

        <!-- main -->
        <div class="main">
            <div class="top-navbar">
                <div class="xp-topbar">

                    <!-- Start XP Row -->
                    <div class="row"> 
                        <!-- Start XP Col -->
                        <div class="col-2 col-md-1 col-lg-1 order-2 order-md-1 align-self-center">
                            <div class="xp-menubar">
                                <span class="material-icons text-white">signal_cellular_alt
                                </span>
                            </div>
                        </div> 
                        <!-- End XP Col -->

                        <!-- Start XP Col -->
                        <div class="col-md-5 col-lg-3 order-3 order-md-2">
                            <div class="xp-searchbar">
                                <form action="postlist">
                                    <div class="input-group">
                                        <input type="search" class="form-control"  name="search"
                                               placeholder="Search">
                                        <div class="input-group-append">
                                            <button class="btn" type="submit" 
                                                    id="button-addon2">GO</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End XP Col -->

                        <!-- Start XP Col -->
                        <div class="col-10 col-md-6 col-lg-8 order-1 order-md-3">
                            <div class="xp-profilebar text-right">
                                <nav class="navbar p-0">
                                    <ul class="nav navbar-nav flex-row ml-auto">   
                                        <li class="dropdown nav-item active">
                                            <a href="#" class="nav-link" data-toggle="dropdown">
                                                <span class="material-icons">notifications</span>
                                                <span class="notification">4</span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="#">You have 5 new messages</a>
                                                </li>
                                                <li>
                                                    <a href="#">You're now friend with Mike</a>
                                                </li>
                                                <li>
                                                    <a href="#">Wish Mary on her birthday!</a>
                                                </li>
                                                <li>
                                                    <a href="#">5 warnings in Server Console</a>
                                                </li>

                                            </ul>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">
                                                <span class="material-icons">question_answer</span>

                                            </a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link" href="#" data-toggle="dropdown">
                                                <img src="img/user.jpg" style="width:40px; border-radius:50%;"/>
                                                <span class="xp-user-live"></span>
                                            </a>
                                            <ul class="dropdown-menu small-menu">
                                                <li>
                                                    <a href="#">
                                                        <span class="material-icons">
                                                            person_outline
                                                        </span>Profile

                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#"><span class="material-icons">
                                                            settings
                                                        </span>Settings</a>
                                                </li>
                                                <li>
                                                    <a href="#"><span class="material-icons">
                                                            logout</span>Logout</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>


                                </nav>

                            </div>
                        </div>
                        <!-- End XP Col -->

                    </div> 
                    <!-- End XP Row -->

                </div>
                <div class="xp-breadcrumbbar text-center">
                    <h4 class="page-title">MKT DashBoard</h4>  
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="mktdashboard">MKT DashBoard</a></li>
                    </ol>                
                </div>

            </div>

            <!-- Cards -->
            <div class="cardBox">
                <div class="card">
                    <div class="">
                        <div class="numbers">${totalView}</div>
                        <div class="cardName">Tổng Lượt Xem Tin Tức</div>
                    </div>
                    <div class="iconBx">
                        <ion-icon name="eye-outline"></ion-icon>
                    </div>
                </div>
                <div class="card">
                    <div class="">
                        <div class="numbers">${totalNews}</div>
                        <div class="cardName">Tổng Số Tin Tức</div>
                    </div>
                    <div class="iconBx">
                        <ion-icon name="newspaper-outline"></ion-icon>
                    </div>
                </div>
                <div class="card">
                    <div class="">
                        <div class="numbers">${totalcom}</div>
                        <div class="cardName">Số lượt Bình luận</div>
                    </div>
                    <div class="iconBx">
                        <ion-icon name="chatbubbles-outline"></ion-icon>
                    </div>
                </div>
                <div class="card">
                    <div class="">
                        <div class="numbers">${totalCode}</div>
                        <div class="cardName">Tổng số Mã giảm giá</div>
                    </div>
                    <div class="iconBx">
                        <ion-icon name="pricetag-outline"></ion-icon>
                    </div>
                </div>
            </div>

            <!-- chart -->
            <div class="row">
                <!-- Your canvas element for the chart -->
                <div class="col-lg-6">
                    <h4 class="card-title">Lượt khách hàng Mới Trên mỗi Tuần</h4>
                    <canvas id="bar-chart-earn"></canvas>
                    
                    <div>
                        <h5>Tổng Số Khách hàng</h5>
                        <h3>${totalCus}</h3>
                    </div>
                </div>

                <!-- Script for the Chart.js chart -->
                <script>
                    // Single Bar Chart
                    var ctx4 = $("#bar-chart-earn").get(0).getContext("2d");
                    var data1 = []; // For X-axis values
                    var data2 = []; // For Y-axis values

                    <c:forEach items="${CustomerCount}" var="a">
                    data1.push("${a.key}"); // Assuming "a.key" represents X-axis values
                    data2.push(${a.value}); // Assuming "a.value" represents Y-axis values
                    </c:forEach>
                    var myChart4 = new Chart(ctx4, {
                        type: "bar",
                        data: {
                            labels: data1,
                            datasets: [{
                                    label: "Total New Customer",
                                    backgroundColor: [
                                        "rgba(0, 156, 255, .7)",
                                        "rgba(0, 156, 255, .7)",
                                        "rgba(0, 156, 255, .7)",
                                        "rgba(0, 156, 255, .7)",
                                        "rgba(0, 156, 255, .7)"
                                    ],
                                    data: data2
                                }]
                        },
                        options: {
                            responsive: true
                        }
                    });
                </script>

                <div class="col-lg-6">
                    <h4 class="card-title">Đánh giá của khách hàng</h4>
                    <canvas id="bar-chart-rate"></canvas>

                    <c:forEach items="${ratepercent}" var="entry">
                        <ul>
                            <li>Rate Tích cực: ${entry.key}%</li>
                            <li>Rate Tiêu cực: ${entry.value}%</li>
                        </ul>
                    </c:forEach>
                    <div>
                        <h5>Tổng Số Lượt Rate</h5>
                        <h3>${totalcom}</h3>
                    </div>
                </div>

                <!-- Script for the Chart.js chart -->
                <script>
                    var ratecount = [];
                    <c:forEach items="${ratelist}" var="r">
                    ratecount.push(${r.value});
                    </c:forEach>
                    // Single Bar Chart
                    var ctx4 = $("#bar-chart-rate").get(0).getContext("2d");
                    var myChart4 = new Chart(ctx4, {
                        type: "bar",
                        data: {
                            labels: ["\u2605", "\u2605\u2605", "\u2605\u2605\u2605", "\u2605\u2605\u2605\u2605", "\u2605\u2605\u2605\u2605\u2605"],
                            datasets: [{
                                    label: "Rate",
                                    backgroundColor: [
                                        "rgba(0, 156, 255, .5)",
                                        "rgba(0, 156, 255, .4)",
                                        "rgba(0, 156, 255, .4)",
                                        "rgba(0, 156, 255, .4)",
                                        "rgba(0, 156, 255, .7)"
                                    ],
                                    data: ratecount
                                }]
                        },
                        options: {
                            responsive: true
                        }
                    });
                </script>
                <!-- end chart -->

                <!-- order details list -->
                <div class="details">
                    <div class="recentOrders">
                        <div class="cardHeader">

                            <h2 id="tableHeading">5 Tin Tức Xem Nhiều nhất</h2>
                            <a href="#" class="btn" id="leastViewButton" style="display: none">Hiện xem ít nhất</a>
                            <a href="#" class="btn" id="mostViewButton">Hiện xem nhiều nhất</a>

                        </div>

                        <table id="mostViewTable">
                            <thead>
                                <tr>
                                    <td>Tiêu đề</td>
                                    <td>Loại Tin Tức</td>
                                    <td>Tác Giả</td>
                                    <td>Lượt Xem</td>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${mostView}" var="n">
                                    <tr>
                                        <td><a href="postdetail?nid=${n.getNid()}">${n.title}</a></td>
                                        <td>${requestScope.canew.get(n)}</td>
                                        <td>${requestScope.username.get(n)}</td>
                                        <td>${requestScope.views.get(n)}</td>
                                    </tr>
                                </c:forEach>

                            </tbody>
                        </table>

                        <table id="leastViewTable" style="display: none">
                            <thead>
                                <tr>
                                    <td>Tiêu đề</td>
                                    <td>Loại Tin Tức</td>
                                    <td>Tác Giả</td>
                                    <td>Lượt Xem</td>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${leastView}" var="n">
                                    <tr>
                                        <td><a href="postdetail?nid=${n.getNid()}">${n.title}</a></td>
                                        <td>${requestScope.canewleast.get(n)}</td>
                                        <td>${requestScope.usernameleast.get(n)}</td>
                                        <td>${requestScope.viewsleast.get(n)}</td>
                                    </tr>
                                </c:forEach>

                            </tbody>
                        </table>
                    </div>

                    <script>
                        var mostSoldTable = document.getElementById("mostViewTable");
                        var slowestSoldTable = document.getElementById("leastViewTable");
                        var mostSoldButton = document.getElementById("mostViewButton");
                        var slowestSoldButton = document.getElementById("leastViewButton");
                        var tableHeading = document.getElementById("tableHeading");

                        mostSoldButton.addEventListener("click", function () {
                            mostSoldTable.style.display = "table";
                            slowestSoldTable.style.display = "none";
                            mostSoldButton.style.display = "none";
                            slowestSoldButton.style.display = "inline";
                            tableHeading.innerText = "5 Tin Tức Xem Nhiều Nhất";
                        });

                        slowestSoldButton.addEventListener("click", function () {
                            mostSoldTable.style.display = "none";
                            slowestSoldTable.style.display = "table";
                            mostSoldButton.style.display = "inline";
                            slowestSoldButton.style.display = "none";
                            tableHeading.innerText = "5 Tin Tức Xem Ít Nhất"
                        });
                    </script>
                    <!-- New customers -->
                    <div class="recentCustomers">
                        <div class="cardHeader">
                            <h2>Mã giảm giá hiện có</h2>
                        </div>
                        <ul>
                            <c:forEach items="${codlist}" var="ac">
                                <li>
                                <ion-icon name="pricetag-outline"></ion-icon>
                                <span>${ac.codeSale}</span>

                                </li>                    
                            </c:forEach>                    
                        </ul>
                    </div>
                </div>

            </div>

            <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
            <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>

            <script>
                        // menu toggle
                        let toggle = document.querySelector('.toggle')
                        let navigation = document.querySelector('.navigation')
                        let main = document.querySelector('.main')

                        toggle.addEventListener('click', function () {
                            navigation.classList.toggle('active')
                            main.classList.toggle('active')
                        })

                        // add hovered class in selected list item
                        // let list = document.querySelectorAll('.navigation li')
                        // function activeLink() {
                        //     list.forEach((item) => item.classList.remove('hovered'));
                        //     this.classList.add('hovered');            
                        // }
                        // list.forEach((item) => item.addEventListener('mouseover', activeLink));
            </script>

            <!--  flot-chart js -->
            <!--    <script src="js/MKT/common.min.js"></script>
            -->                
    </body>

</html>
