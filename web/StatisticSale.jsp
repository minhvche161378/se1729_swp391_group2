<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sale</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!----css3---->

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

        <link rel="stylesheet" href="css/custom.css">
        <link rel="stylesheet" href="css/post/adminStyle.css">



        <!--google fonts -->

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">


        <!--google material icon-->
        <link href="https://fonts.googleapis.com/css2?family=Material+Icons"rel="stylesheet">
        <!-- Include the necessary Chart.js library -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="chartt/lib/chart/chart.min.js"></script>
        <script src="chartt/lib/easing/easing.min.js"></script>
        <script src="chartt/lib/waypoints/waypoints.min.js"></script>
        <script src="chartt/lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="chartt/lib/tempusdominus/js/moment.min.js"></script>
        <script src="chartt/lib/tempusdominus/js/moment-timezone.min.js"></script>
        <script src="chartt/lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/chart.js@3.5.1/dist/chart.min.js"></script>



    </head>
    <body>
        <nav id="sidebar">
            <div class="sidebar-header">
                <a href="home"><h3><img src="images/favicon.png" class="img-fluid"/><span>Board Game</span></h3></a>
            </div>
            <ul class="list-unstyled components">
               <li  class="active">
                    <a href="saledashboard" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Thống kê</span></a>
                </li>
                 <li>
                    <a href="orderList" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Xử lí đơn hàng</span></a>
                </li>
                <li>
                    <a href="manageproduct" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Quản lí sản phẩm</span></a>
                </li>

                <li>
                    <a href="addproduct" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Thêm sản phẩm mới</span></a>
                </li>
                  <li>
                    <a href="home" class="dashboard"><i class="material-icons">dashboard</i>
                        <span>Về trang chủ</span></a>
                </li>



               




            </ul>


        </nav>
        
        <!-- main -->
        <div class="main">
            <div class="top-navbar">
                <div class="xp-topbar">

                    <!-- Start XP Row -->
                    <div class="row"> 
                        <!-- Start XP Col -->
                        <div class="col-2 col-md-1 col-lg-1 order-2 order-md-1 align-self-center">
                            <div class="xp-menubar">
                                <span class="material-icons text-white">signal_cellular_alt
                                </span>
                            </div>
                        </div> 
                        <!-- End XP Col -->

                        <!-- Start XP Col -->
                        <div class="col-md-5 col-lg-3 order-3 order-md-2">
                            <div class="xp-searchbar">
                                <form action="postlist">
                                    <div class="input-group">
                                        <input type="search" class="form-control"  name="search"
                                               placeholder="Search">
                                        <div class="input-group-append">
                                            <button class="btn" type="submit" 
                                                    id="button-addon2">GO</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End XP Col -->

                        <!-- Start XP Col -->
                        <div class="col-10 col-md-6 col-lg-8 order-1 order-md-3">
                            <div class="xp-profilebar text-right">
                                <nav class="navbar p-0">
                                    <ul class="nav navbar-nav flex-row ml-auto">   
                                        <li class="dropdown nav-item active">
                                            <a href="#" class="nav-link" data-toggle="dropdown">
                                                <span class="material-icons">notifications</span>
                                                <span class="notification">4</span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="#">You have 5 new messages</a>
                                                </li>
                                                <li>
                                                    <a href="#">You're now friend with Mike</a>
                                                </li>
                                                <li>
                                                    <a href="#">Wish Mary on her birthday!</a>
                                                </li>
                                                <li>
                                                    <a href="#">5 warnings in Server Console</a>
                                                </li>

                                            </ul>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">
                                                <span class="material-icons">question_answer</span>

                                            </a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link" href="#" data-toggle="dropdown">
                                                <img src="img/user.jpg" style="width:40px; border-radius:50%;"/>
                                                <span class="xp-user-live"></span>
                                            </a>
                                            <ul class="dropdown-menu small-menu">
                                                <li>
                                                    <a href="#">
                                                        <span class="material-icons">
                                                            person_outline
                                                        </span>Profile

                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#"><span class="material-icons">
                                                            settings
                                                        </span>Settings</a>
                                                </li>
                                                <li>
                                                    <a href="#"><span class="material-icons">
                                                            logout</span>Logout</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>


                                </nav>

                            </div>
                        </div>
                        <!-- End XP Col -->

                    </div> 
                    <!-- End XP Row -->

                </div>
                <div class="xp-breadcrumbbar text-center">
                    <h4 class="page-title">Sale DashBoard</h4>  
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="saledashboard">Sale DashBoard</a></li>
                    </ol>                
                </div>

            </div>

            <!-- Cards -->
            <div class="cardBox">
                <div class="card">
                    <div class="">
                        <div class="numbers">${TotalCustomers}</div>
                        <div class="cardName">TotalCustomers</div>
                    </div>
                    <div class="iconBx">
                        <ion-icon name="people-outline"></ion-icon>
                    </div>
                </div>
                <div class="card">
                    <div class="">
                        <div class="numbers">${totalOrders}</div>
                        <div class="cardName">Orders</div>
                    </div>
                    <div class="iconBx">
                        <ion-icon name="cart-outline"></ion-icon>
                    </div>
                </div>
                <div class="card">
                    <div class="">
                        <div class="numbers">${TotalSoldProducts}</div>
                        <div class="cardName">Total Sold Products</div>
                    </div>
                    <div class="iconBx">
                        <ion-icon name="receipt-outline"></ion-icon>
                    </div>
                </div>

                <div class="card">
                    <div class="">
                        <c:if test="${not empty totalEarning}">
                            <div class="numbers"><%= String.format("%,.0f", (double)request.getAttribute("totalEarning")) %> VNĐ</div>
                        </c:if>
                        <div class="cardName">Earning</div>
                    </div>
                    <div class="iconBx">
                        <ion-icon name="cash-outline"></ion-icon>
                    </div>
                </div>







            </div>

            <div class="charts">
                <div class="chart">

                    <div class="row">

                        <div class="col-lg-8">
                            <h2>Earnings (months)</h2>

                            <canvas id="lineChart"></canvas>
                        </div>

                        <script>
                            var ctx = document.getElementById('lineChart').getContext('2d');

                            var years = [];
                            var months = [];
                            var totalAmounts = [];
                            var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

                            <c:forEach var="orderStat" items="${list}">
                            var year = ${orderStat.year};
                            var monthIndex = ${orderStat.month - 1}; // Chuyển từ số tháng sang chỉ số của mảng (giảm đi 1)
                            var totalAmount = ${orderStat.totalAmount};

                            years.push(year);
                            months.push(monthNames[monthIndex]); // Sử dụng tên tiếng Anh của tháng
                            totalAmounts.push(totalAmount);
                            </c:forEach>

                            var myChart = new Chart(ctx, {
                                type: 'line',
                                data: {
                                    labels: months,
                                    datasets: [{
                                            label: 'Earnings in VNĐ',
                                            data: totalAmounts,
                                            backgroundColor: [
                                                'rgba(255, 69, 0, 1)'
                                            ],
                                            borderColor: 'rgb(255, 69, 0, 1)',
                                            borderWidth: 1
                                        }]
                                },
                                options: {
                                    responsive: true,
                                    scales: {
                                        x: {
                                            position: 'bottom' // Đặt tên trục hoành ở trên cùng
                                        },
                                        y: {
                                            beginAtZero: true,
                                            title: {
                                                display: true,
                                                text: 'Doanh thu' // Hiển thị tên "Doanh thu" trên trục tung
                                            }
                                        }
                                    }
                                }
                            });

                            // Hàm cập nhật biểu đồ dựa trên lựa chọn của người dùng
                            function updateChart() {
                                var chartType = document.getElementById("chartType").value;
                                // Thực hiện cập nhật biểu đồ dựa trên lựa chọn (tháng, tuần, năm)
                            }
                        </script>
                        <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels"></script>
                        <div class="col-lg-4">

                            <h2>Orders Status</h2>
                            <canvas id="doughnut" ></canvas>
                        </div>
                        <script>
                            var ctx2 = document.getElementById('doughnut').getContext('2d');

                            var labels = [];
                            var data = [];
                            var backgroundColors = [];
                            var borderColors = [];

                            <c:forEach var="entry" items="${listcount}">
                            labels.push('<c:out value="${entry.key}" />');
                            data.push(<c:out value="${entry.value}" />);

                            // Sử dụng chỉ 3 màu nền
                            var colors = ['#FFFF99', '#00CC66', '#9B30FF',''];
                            backgroundColors.push(colors[backgroundColors.length % colors.length]);
                            borderColors.push(colors[borderColors.length % colors.length]);
                            </c:forEach>

                            var myChart2 = new Chart(ctx2, {
                                type: 'doughnut',
                                data: {
                                    labels: labels,
                                    datasets: [{
                                            label: 'Employees',
                                            data: data,
                                            backgroundColor: backgroundColors,
                                            borderColor: borderColors,
                                            borderWidth: 1
                                        }]
                                },
                                options: {
                                    responsive: true
                                }
                            });
                        </script>

                    </div>
                </div>

            </div>





            <!-- Script for the Chart.js chart -->

            <!-- end chart -->

            <!-- order details list -->
            <div class="details">
                <div class="recentOrders">
                    <div class="cardHeader">
                        <h2>Danh Sách sản phẩm </h2>
                        
                    </div>

                    <table>
                        <thead>
                            <tr>
                                <td>Tên sản phẩm
                                    <c:choose>
                                        <c:when test="${param.direction == 'asc'}">
                                            <a href="saledashboard?sort=pname&direction=asc&index=${indexPage}" style="display: none;">&nbsp⬇</a>
                                            <a href="saledashboard?sort=pname&direction=desc&index=${indexPage}">&nbsp⬆</a>
                                        </c:when>
                                        <c:when test="${param.direction == 'desc'}">
                                            <a href="saledashboard?sort=pname&direction=asc&index=${indexPage}">&nbsp⬇</a>
                                            <a href="saledashboard?sort=pname&direction=desc&index=${indexPage}" style="display: none;">&nbsp⬆</a>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="saledashboard?sort=price&direction=asc&index=${indexPage}">&nbsp⬆</a>
                                        </c:otherwise>
                                    </c:choose></td>
                                <td>Giá 
                                    <c:choose>
                                        <c:when test="${param.direction == 'asc'}">
                                            <a href="saledashboard?sort=price&direction=asc&index=${indexPage}" style="display: none;">&nbsp⬇</a>
                                            <a href="saledashboard?sort=price&direction=desc&index=${indexPage}">&nbsp⬆</a>
                                        </c:when>
                                        <c:when test="${param.direction == 'desc'}">
                                            <a href="saledashboard?sort=price&direction=asc&index=${indexPage}">&nbsp⬇</a>
                                            <a href="saledashboard?sort=price&direction=desc&index=${indexPage}" style="display: none;">&nbsp⬆</a>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="saledashboard?sort=price&direction=asc&index=${indexPage}">&nbsp⬆</a>
                                        </c:otherwise>
                                    </c:choose></td>
                                <td>
                                    Quantity
                                    <c:choose>
                                        <c:when test="${param.direction == 'asc'}">
                                            <a href="saledashboard?sort=quantity&direction=asc&index=${indexPage}" style="display: none;">&nbsp⬇</a>
                                            <a href="saledashboard?sort=quantity&direction=desc&index=${indexPage}">&nbsp⬆</a>
                                        </c:when>
                                        <c:when test="${param.direction == 'desc'}">
                                            <a href="saledashboard?sort=quantity&direction=asc&index=${indexPage}">&nbsp⬇</a>
                                            <a href="saledashboard?sort=quantity&direction=desc&index=${indexPage}" style="display: none;">&nbsp⬆</a>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="saledashboard?sort=quantity&direction=asc&index=${indexPage}">&nbsp⬆</a>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>Status 
                                    <c:choose>
                                        <c:when test="${param.direction == 'asc'}">
                                            <a href="saledashboard?sort=status&direction=asc&index=${indexPage}" style="display: none;">&nbsp⬇</a>
                                            <a href="saledashboard?sort=status&direction=desc&index=${indexPage}">&nbsp⬆</a>
                                        </c:when>
                                        <c:when test="${param.direction == 'desc'}">
                                            <a href="saledashboard?sort=status&direction=asc&index=${indexPage}">&nbsp⬇</a>
                                            <a href="saledashboard?sort=status&direction=desc&index=${indexPage}" style="display: none;">&nbsp⬆</a>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="saledashboard?sort=status&direction=asc&index=${indexPage}">&nbsp⬆</a>
                                        </c:otherwise>
                                    </c:choose></td>

                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${plist}" var="p">
                                <tr>
                                    <td>${p.pname}</td>
                                    <td>${p.price} VND</td>
                                    <td>${p.quantity}</td>
                                    <td>
                                        <c:if test="${p.status  ==  1}">
                                            <span class="status delivered">Publish</span>
                                        </c:if>
                                        <c:if test="${p.status  ==  0}">
                                            <span class="status  return">Block</span>
                                        </c:if>
                                    </td>

                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <div class="clearfix">
                        <div class="hint-text">Showing <b>10</b> out of <b>${count}</b> entries</div>
                        <ul class="pagination">

                            <c:if test="${indexPage > 1}">
                                <li class="page-item disabled"><a href="saledashboard?index=${indexPage - 1}">Previous</a></li>
                                </c:if>

                            <c:forEach begin="1" end="${endPage}" var="i">
                                <li class="page-item"><a href="saledashboard?index=${i}" class="page-link">${i}</a></li>
                                </c:forEach>
                                <c:if test="${indexPage < endPage}">
                                <li class="page-item"><a href="saledashboard?index=${indexPage + 1}" class="page-link">Next</a></li>
                                </c:if>
                        </ul>
                    </div>
                </div>
                <!-- New customers -->

                <div class="recentOrders">
                    <div class="cardHeader">
                        <h2>Những sản phẩm bán chạy </h2>
                    </div>

                     <form action="saledashboard" method="get">
                        <div class="monthSelect">
                            <label for="selectedMonth">Chọn tháng:</label>
                            <select name="selectedMonth">
                                <c:forEach items="${months}" var="month">
                                    <option value="${month}">Tháng ${month}</option>
                                </c:forEach>
                            </select>
                            <input type="submit" value="Xem">
                        </div>

                        <table>
                            <thead>
                                <tr>
                                    <td>
                                        Tên sản phẩm 

                                    </td>
                                    <td>  Số lượng đã bán 


                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${topSellingProducts}" var="product">
                                    <tr>
                                        <td>
                                            ${product.pname}
                                        </td>
                                        <td>
                                            ${product.quantity}
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </form>
                    <div class="clearfix"></div>
                </div>

            </div>

        </div>


        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
        <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>

        <script>
                            // menu toggle
                            let toggle = document.querySelector('.toggle')
                            let navigation = document.querySelector('.navigation')
                            let main = document.querySelector('.main')

                            toggle.addEventListener('click', function () {
                                navigation.classList.toggle('active')
                                main.classList.toggle('active')
                            })

                            // add hovered class in selected list item
                            // let list = document.querySelectorAll('.navigation li')
                            // function activeLink() {
                            //     list.forEach((item) => item.classList.remove('hovered'));
                            //     this.classList.add('hovered');            
                            // }
                            // list.forEach((item) => item.addEventListener('mouseover', activeLink));
        </script>

        <!--  flot-chart js -->
        <!--    <script src="js/MKT/common.min.js"></script>
        -->                
    </body>

</html>
