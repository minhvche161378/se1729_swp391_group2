<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi" lang="vi"><head>
        <title>Board Game VN - Bring People Closer</title>
        <head>
            <script>
                function submitForm() {
                    document.getElementById("sortForm").submit();
                }
            </script> 
        </head>
        <%@include file="Header.jsp"  %>
        <div id="main" class="container-content">

            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="box-left-product">
                            <ul class="menu_cates">

         <li><a href="criteria?id=1" title="Game hot giá sốc"> <span>Game hot giá sốc</span></a> </li>
        <li><a href="criteria?id=2" title="Game hot giá sốc"> <span>Sản phầm bán chạy</span></a> </li>  
        <li><a href="criteria?id=3" title="Game hot giá sốc"> <span>Sản phầm mới ra</span></a> </li>        
        <c:forEach items="${caList}" var="ca">
              <li><a href="category?id=${ca.caid}"> <span>${ca.caname}</span></a></li>
        </c:forEach>
                            </ul>
                            <div class="filter-left-contain">
                                <div class="att-filter-title">
                                    Filter
                                </div>
                                <div class="att-filter">
                                    <h4 class="att-title">Khoảng giá <a href="" class="showfilter" ></a></h4>
                                    <ul class="att-filter-ops-price">
                                        <li>
                                            <a class="op-ft " href="category?id=${caid}&price=1&sort=${sort}&page=1" rel="nofollow"><span><i></i></span>0 - 100.000?</a>
                                        </li>
                                        <li>
                                            <a class="op-ft " href="category?id=${caid}&price=2&sort=${sort}&page=1" rel="nofollow"><span><i></i></span>100.000 - 300.000?</a>
                                        </li>
                                        <li>
                                            <a class="op-ft " href="category?id=${caid}&price=3&sort=${sort}&page=1" rel="nofollow"><span><i></i></span>300.000 - 500.000?</a>
                                        </li>
                                        <li>
                                            <a class="op-ft " href="category?id=${caid}&price=4&sort=${sort}&page=1" rel="nofollow"><span><i></i></span>Trên 500.000?</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>

                        <div class="clear"></div>


                    </div>
                    <div class="col-lg-9">
                        
                        <div class="box-filter-product">
                            <div class="total_product">Tìm thấy <font>${pList.size()}</font> sản phẩm</div>
                            <div id="pager-top">
                                Trang:
                                <ul id="yw0" class="yiiPager"><li class="first hidden"><a href="/category/tre-em?q=%2Fcategory%2Ftre-em"></a></li>
                                    <li class="previous hidden"><a href=""><span class="glyphicon glyphicon-triangle-left"></span></a></li>
                                            <c:forEach begin="1" end="${numberPage}" var="i">
                                        <li class="page selected"><a href="category?id=${caid}&price=4&sort=${sort}&page=${i}">${i}</a></li>
                                        </c:forEach>
                                    <li class="next"><a href=""><span class="glyphicon glyphicon-triangle-right"></span></a></li>
                                    <li class="last"><a href=""></a></li></ul> </div>
                            <div class="sort_product">
                                <form id="sortForm" action="category" method="get">
                                    <input type="hidden" name="id" value="${caid}">
                                        <input type="hidden" name="price" value="${price}">
                                            <input type="hidden" name="page" value="1">
                                                Sắp xếp theo:
                                                <select class="select_sort" name="sort" onchange="submitForm()">
                                                  
                                                    <option value="new">Mới nhất</option>
                                                    <option value="DESC">Giá giảm dần</option>
                                                   <option value="ASC">Giá tăng dần</option>
                                                </select>
                                                </form>

                                                </div>
                                                </div>
                                                <div class="category-products list_product" style="display: block;">
                                                    <div id="yw1" class="list-view">
                                                        <div class="row">
                                                            <c:forEach items="${pList}" var="p">
                                                                <div class="col-lg-4">
                                                                    <c:choose>
                                                                        <c:when test="${p.getQuantity() == 0}">
                                                                            <div class="product out_of_stock">
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <div class="product">

                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                            <div class="thumb-img">
                                                                                <div class="image">
                                                                                    <a href="productdetail?Pid=${p.pid}" title="${p.pname}">
                                                                                        <img 
                                                                                            src="images/${p.img}" 
                                                                                            alt="${p.pname}" width="290" height="290">
                                                                                    </a>
                                                                                </div>
                                                                                <div class="action" onclick="actionclick('/tre-em/mooncake-master-1769')">
                                                                                    <div class="content">
                                                                                        <a class="btn_view" href="productdetail?Pid=${p.pid}" title="Xem chi tiết">
                                                                                            Xem chi tiết
                                                                                        </a>
                                                                                        <a class="btn_addcart" href="addcart?pid=${p.pid}&url=category?id=${caid}&price=${price}&page=1&sort=${sort}" title="Cho vào giỏ">
                                                                                            Cho vào giỏ
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="product-info">
                                                                                <div class="info">
                                                                                    <a class="name-p" href="productdetail?Pid=${p.pid}" title="${p.pname}">${p.pname}
                                                                                           <c:if test="${p.isIsDiscount()}">
                                                                                    <div class="discount-label">
                                                                                        <c:set var="price" value="${p.price}" />
                                                                                        <c:set var="priceSale" value="${p.priceSale}" />

                                                                                     
                                                                                     
                                                                                            <%-- Tính phần trăm giảm giá --%>
                                                                                            <c:set var="discountPercentage" value="${((price - priceSale) / price) * 100}" />

                                                                                            <%-- Hiển thị phần trăm giảm giá --%>
                                                                                            <p>${discountPercentage}%</p>
                                                                                       

                                                                                    </div> </c:if>
                                                                                        </a>
                                                                                </div>
                                                                                <div class="summary">
                                                                                </div>
                                                                                <div class="price">

                                                                                    <c:choose>
        <c:when test="${p.isIsDiscount() && p.priceSale != 0}">
            <fmt:formatNumber value="${p.priceSale}" pattern="#,##0" var="formattedPriceSale" />
            <fmt:formatNumber value="${p.price}" pattern="#,##0" var="formattedPrice" />
            <font>${formattedPriceSale} VNĐ</font>
            <span class="old_price">${formattedPrice} VNĐ</span>
            <c:if test="${p.getQuantity() == 0}">
                <span class="outofstock">Cháy hàng</span>
            </c:if>
        </c:when>
        <c:otherwise>
            <fmt:formatNumber value="${p.price}" pattern="#,##0" var="formattedPrice" />
            <font>${formattedPrice} VNĐ</font>
        </c:otherwise>
    </c:choose>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </c:forEach>
                                                            </div><div class="keys" style="display:none" title="/category/tre-em"><span>1536</span><span>1221</span><span>1693</span><span>1657</span><span>1737</span><span>1712</span><span>1740</span><span>1739</span><span>1711</span><span>1710</span><span>1738</span><span>1769</span></div>
                                                        </div> </div>
                                                   
                                                                            </div>
                                                                            </div>
                                                                            </div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                            <%@include file="Footer.jsp"  %>
                                                                            </body></html>